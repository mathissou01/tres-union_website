<?php

namespace App\Actions;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use App\Models\School;
use App\Models\CourseSession;
use App\Models\SessionStudent;
use App\Models\Subject;
use App\Models\Contributor;
use App\Models\Lesson;
use App\Models\Level;
use Illuminate\Support\Carbon;



class GetTotalContributionsAction
{
    public static function handle($lessons, $id = null)
    {
        return $id ? self::getTotal($lessons, $id) : self::getTotals($lessons);

    }

    private static function getTotals($lessons)
    {
        $contributions = [];

        $lessonsWithContributions = [];

        foreach ($lessons as $l) {
            $l->contibution = array_key_exists($l->contributor_id, $contributions)
                ? $contributions[$l->contributor_id]
                : self::getTotal($lessons, $l->contributor_id);

            $contributions[$l->contributor_id] = $l->contibution;

            $lessonsWithContributions[] = $l;
        }

        return $lessonsWithContributions;
    }

    private static function getTotal($lessons, $id)
    {
        return $lessons->where('contributor_id', $id)->reduce(
            function  ($carry, $current) {
                return $carry + self::getContribution($current);
            }
        );
    }

    private static function getContribution($current)
    {
        return date('H:i', strtotime($current->endding) - strtotime($current->starting));
    }
}