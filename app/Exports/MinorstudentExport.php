<?php

namespace App\Exports;

use App\Models\Minorstudent;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\School;

use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MinorstudentExport implements FromCollection,
WithHeadings, 
ShouldAutoSize,
WithMapping,
WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Minorstudent::all();
    }
    public function headings():array{
        return [
            'Id','Créée le Année/Mois/Jour/Heure','Nom','Prénom','Email','Téléphone','Adresse',
            'Ville','Code postale','École','Classe','Etat du paiement','Nom du parent','Prénom du parent',
            'Email du parent','Téléphone du parent','Adresse du parent','Ville du parent','Code postale du parent'
            
        ];
       
    }
     public function map($minorstudent): array{
        return [
                
                $minorstudent->id,
                $minorstudent->created_at,
                $minorstudent->last_name,
                $minorstudent->first_name,
                $minorstudent->email_address,
                $minorstudent->phone_number,
                $minorstudent->street_address,
                $minorstudent->city,
                $minorstudent->postal_code,
                $minorstudent->school->name .".". $minorstudent->school->city, //Concatener pour afficher deux variable dans un champ
                $minorstudent->level,
                $minorstudent->choices,
                $minorstudent->familly->first_name,
                $minorstudent->familly->last_name,
                $minorstudent->familly->email_address,
                $minorstudent->familly->phone_number,
                $minorstudent->familly->street_address,
                $minorstudent->familly->city,
                $minorstudent->familly->postal_code,
               

        ];
    }
public function registerEvents(): array
    {
        return [
            // Style du Excel.Reference sur PhpSpreadsheet / Styles.
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1:L1')->applyFromArray([
                    'font' => ['bold' => true],
                    'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => 'cf6550'],
                            ],
                        ],
                        'fill' => [
        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation' => 90,
        'startColor' => [
            'argb' => 'ff4350',
        ],
        'endColor' => [
            'argb' => 'FFFFFFFF',
        ],
                        ]       
                ]);
                $event->sheet->getStyle('M1:S1')->applyFromArray([
                    'font' => ['bold' => true],
                    'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => 'bf80ff'],
                            ],
                        ],
                        'fill' => [
        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation' => 90,
        'startColor' => [
            'argb' => 'ff99ff',
        ],
        'endColor' => [
            'argb' => 'FFFFFFFF',
        ],
                        ]       
                ]);
}
];
}
}


