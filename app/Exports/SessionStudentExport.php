<?php

namespace App\Exports;

use App\Models\SessionStudent;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithDrawings;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class SessionStudentExport implements FromCollection,
WithHeadings, 
ShouldAutoSize,
WithMapping,
WithEvents, 
WithDrawings
{

    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
         return SessionStudent::all();
    }
    public function headings():array{
        return [
            'Id','Créée le Année/Mois/Jour/Heure','Nom de l\'étudiant','Prénom de l\'étudiant','Email de l\'étudiant','Téléphone de l\'étudiant','Classe','École',
            'Nom du parent','Prénom du parent','Email du parent','Téléphone du parent','Adresse','Code postale','Ville'
        ];
       
    }
    public function drawings()
    {
        $drawing = new Drawing();
        $drawing->setName('Logo_Tres_Union');
        $drawing->setDescription('Logo de Très-Union');
        $drawing->setPath(public_path('/storage/images/logo.png'));
        $drawing->setHeight(80);
        $drawing->setCoordinates('Q2');

        return $drawing;
    }
    public function map($student): array{
        return [
                $student->id,
                $student->created_at,
                $student->student_lastname,
                $student->student_firstname,
                $student->student_email,
                $student->student_phone,
                $student->level->name,
                $student->school,
                $student->legal_lastname,
                $student->legal_firstname,
                $student->legal_email,
                $student->legal_phone,
                $student->street,
                $student->postal_code,
                $student->city

        ];
    }
       public function registerEvents(): array
    {
        return [
            // Style du Excel.Reference sur PhpSpreadsheet / Styles.
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1:O1')->applyFromArray([
                    'font' => ['bold' => true],
                    'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => 'fcba03'],
                            ],
                        ],
                        'fill' => [
        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation' => 90,
        'startColor' => [
            'argb' => 'c9c199',
        ],
        'endColor' => [
            'argb' => 'FFFFFFFF',
        ],
                        ]       
                ]);
}
];
}
}
