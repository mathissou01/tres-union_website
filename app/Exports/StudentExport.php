<?php

namespace App\Exports;

use App\Models\Student;
use App\Models\Minorstudent;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class StudentExport implements FromCollection,
WithHeadings, 
ShouldAutoSize,
WithMapping,
WithEvents

{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return (Student::all());
        
    }
 public function headings():array{
        return [
            'Id','Créée le Année/Mois/Jour/Heure','Nom','Prénom','Email','Téléphone','Adresse',
            'Ville','Code postale','École','Classe','Etat du paiement'
            
        ];
       
    }
    public function map($student): array{
        return [
                $student->id,
                $student->created_at,
                $student->last_name,
                $student->first_name,
                $student->email_address,
                $student->phone_number,
                $student->street_address,
                $student->city,
                $student->postal_code,
                $student->school,
                $student->level,
                $student->choices,
               
               

        ];
    }
     
       public function registerEvents(): array
    {
        return [
            // Style du Excel.Reference sur PhpSpreadsheet / Styles.
            AfterSheet::class => function(AfterSheet $event) {
                $event->sheet->getStyle('A1:L1')->applyFromArray([
                    'font' => ['bold' => true],
                    'borders' => [
                            'outline' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => 'cf6550'],
                            ],
                        ],
                        'fill' => [
        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation' => 90,
        'startColor' => [
            'argb' => 'ff4350',
        ],
        'endColor' => [
            'argb' => 'FFFFFFFF',
        ],
                        ]       
                ]);
}
];
}
}

