<?php

namespace App\Http\Controllers;

use PDF;
use App\Models\Appel;
use App\Models\Student;
use App\Models\Contributor;
use App\Models\AppelStudent;
use App\Models\Minorstudent;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use Illuminate\Validation\Rule;
use Illuminate\Http\JsonResponse;
use App\Exports\AppelStudentExport;
use Illuminate\Support\Facades\Validator;

class AppelController extends Controller
{
  
    protected $appelstudent;
    // Afficher tout les appels
    public function show()
    {
        return view('appel', ["appels" => Appel::all()]);
    }

    // Envoyer la vu pour créer un appel
    public function new()
    {
 
        return view('newappel', ['appels'=>Appel::all(),"minorstudents"=> Minorstudent::all(),"contributors" => Contributor::all()]);

    }

    // Ajouter l'appel
    public function add_appel(Request $request)
    {
        $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'createur' => 'Createur de lappel',
          'motif' => 'Motif élève',
        ];

        $validateData = Validator::make($request->all(), [
          'createur' => 'required | string',
          'present' => '',
          'student_id' => 'nullable',
          'minorstudent_id' => 'nullable',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
                connectify('error', 'Erreur', $message);
            }
            return redirect("/appel/new");
        } 
        else {
            // foreach ($request->minorstudents as $index) {
            // $appelstudent = new AppelStudent;
            // $appelstudent->minorstudent_id=$index;
            // $appelstudent->present= true;
            // $appelstudent->motif= $request->input("motif");
            // $appel->minorstudents()->attach($appelstudent);
            // $appelstudent = AppelStudent::create([
            // "minorstudent_id" => $index,
            // "present" => true,
            // "motif" => $request->input("motif-".$index)
            // ]);
            // $appel->minorstudents()->associate($appelstudent);
            // $appelstudent->save();
            // $appel->minorstudents()->attach($request->minorstudents, [
            // "present" => true,
            //  "motif" => $request->input("motif")[$index]
            //  ]);
            //  }
            
          
            //  foreach (Minorstudent::whereNotIn("id", $request->minorstudents)->get() as $index) {
            //     $appelstudent = new AppelStudent;
            // $appelstudent->minorstudent_id=$index;
            // $appelstudent->present= false;
            // $appelstudent->motif= $request->input("motif");
            //  $appel->minorstudents()->attach($appelstudent);
            //  $appelstudent->save();
            //  }
            // $appelstudent = AppelStudent::create([
          
            // "minorstudent_id" => $index,
            // "present" => false,
            // "motif" => $request->input("motif-"+$index)
            // ]);
            //   $appel->minorstudents()->associate($appelstudent);
           
         
            // $appel->minorstudents()->updateExistingPivot($request->minorstudents, ['motif' => $request->motif]);
          
            // $appel->students()->attach($request->students, [
            //  "present" => true,
            // // "motif" => $request->input('motif'),
            // // "motif" => $request->motif,
            // ]
            //               );
            // $appel->students()->attach(Student::whereNotIn("id", $request->students)->get());
            //  $appel->students()->attach(["motif" =>$request->input('motif')]);
            // $appel->students()->syncWithPivotValues([1],['motif' => $request->motif]);
            //  dd($appel->students()->motif = $request->motif);
            //  $appel->students()->attach($request->students);
            //  $appel->students()->updateExistingPivot($request->students, ['motif' => $request->input('motif')]);
             $appel = Appel::create([
            "createur" => $request->createur,
             ]);
            $appel->minorstudents()->attach($request->minorstudents, [
            "present" => true, 
             ]
             );
            //  $appel->students()->attach($request->students, [
            // "present" => true, 
            //  ]
            //  );
            //  $request->minorstudents=array('0');
            $appel->minorstudents()->attach
            (Minorstudent::whereNotIn("id", $request->minorstudents)->get()); 
            // $request->students=array('0');
            // $appel->students()->attach(Student::whereNotIn("id", $request->students)->get()); 
            $appel->save();
            return redirect("/appel");
        }
    }

    // Function de recherche par uuid pour les code bar
    public function search_codebar(request $request): JsonResponse
    {
        $q = $request->input('q');
        $minorstudents = Minorstudent::where('uuid', 'like', $q)->get();
        return response()->json([
      "minorstudents" => $minorstudents]);
    }

    //Affichage des appels
    public function list_appel($id)
    {
        return view('list_appel', ["appel" => Appel::find($id)]);
    }

 //Appel en PDF
    public function csvappel($id)
    {
      $pdf = PDF::loadView('list_appel_pdf',["appel" => Appel::find($id)]);
        return $pdf->download('liste_etudiant_stage.pdf');
    
    }

    //Supprimer appel
    public function del(Request $request, $id){
          $appel = Appel::where('id',$id)->first();
          if ($appel) {
              $appel->delete();
              connectify('success','Succès',"L'appel a été supprimée 🖐️");
          }
          else {
            connectify('error','Erreur',"Erreur à la suppression de l'appel 😡");
          }
          return redirect('/appel');
      }
}