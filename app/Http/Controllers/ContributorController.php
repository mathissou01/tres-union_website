<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Subject;
use Illuminate\Support\Facades\Validator;
use App\Models\Contributor;
use App\Models\School;
use PDF;
use Carbon;

class ContributorController extends Controller
{
  // Afficher tout les intervenants
  public function show(){
    return view('contributor', ["contributors" => Contributor::all()]);
  }

  public function new(){
    return view('newcontributor', ["subjects"=>Subject::all()->sortBy('name'),"schools" => School::all()]);
  }

  public function add(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
          'digits' => 'Le champ ":attribute" ne contient pas le bon nombre de chiffre',
          'email' => 'Le champ ":attribute" doit être un email valide',

        ];

        $attributes = [
          'firstname' => 'Prénom',
          'lastname' => 'Nom',
          'email' => 'Email',
          'phone' => 'Téléphone',
          'street' => 'Adresse',
          'city' => 'Ville',
          'postal_code' => 'Code Postal',
          'rules' => 'Réglement',
          'diploma' => 'Diplome',
          'contract' => 'Type de contrat',
          'school' => 'Établissement',
        ];

        $validateData = Validator::make($request->all(), [
          'firstname' => 'required | string',
          'lastname' => 'required | string',
          'email' => 'required | email',
          'phone' => 'nullable | digits:10',
          'street' => 'required | string',
          'city' => 'required | string',
          'postal_code' => 'required | digits:5',
          'rules' => 'required',
          'diploma' => 'required | string',
          'contract' => 'required | string',
          'school' => 'required | string',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect('/contributor/new');
        }
        else {
          $contributor = new Contributor;
          $contributor->firstname = $request->firstname;
          $contributor->lastname = $request->lastname;
          $contributor->email = $request->email;
          $contributor->phone = $request->phone;
          $contributor->street = $request->street;
          $contributor->city = $request->city;
          $contributor->postal_code = $request->postal_code;
          $contributor->rules = 1;
          $contributor->diploma = $request->diploma;
          $contributor->contract = $request->contract;
          if ($request->school!="none") {
              $contributor->school()->associate($request->school);
          }
          $contributor->save();
          $contributor->subjects()->attach($request->subjects);
          return redirect('/contributor');
        }
  }

  public function show_details($id){
    return view('contributordetails', ["contributor" => Contributor::find($id)]);
  }

  public function show_horaire($id){
    return view('ficheHoraire', ["contributor" => Contributor::find($id)]);
  }

  public function showMod($id){
    if (Contributor::find($id)===Null) {
      abort(404);
    }
    $teached_subject = Contributor::find($id)->subjects;
    $teached_subject_id=[];
    $i = 0;
    foreach ($teached_subject as $subject) {
        $teached_subject_id[$i]=$subject->id;
        $i++;
    }
    return view('modcontributor', ["contributor" => Contributor::find($id),"schools" => School::all(),"subjects" => Subject::all(), "teached_subjects" => $teached_subject_id]);
  }

  public function mod(Request $request, $id){
    $messages = [
      'required' => 'Le champ ":attribute" est manquant',
      'string' => ':attribute n\'est pas valide',
      'digits' => 'Le champ ":attribute" ne contient pas le bon nombre de chiffre',
      'email' => 'Le champ ":attribute" doit être un email valide',

    ];

    $attributes = [
      'firstname' => 'Prénom',
      'lastname' => 'Nom',
      'email' => 'Email',
      'phone' => 'Téléphone',
      'street' => 'Adresse',
      'city' => 'Ville',
      'postal_code' => 'Code Postal',
      'rules' => 'Réglement',
      'diploma' => 'Diplome',
      'contract' => 'Type de contrat',
      'school' => 'Établissement',
    ];

    $validateData = Validator::make($request->all(), [
      'firstname' => 'required | string',
      'lastname' => 'required | string',
      'email' => 'required | email',
      'phone' => 'nullable | digits:10',
      'street' => 'required | string',
      'city' => 'required | string',
      'postal_code' => 'required | digits:5',
      'diploma' => 'required | string',
      'contract' => 'required | string',
      'school' => 'required | string',
    ], $messages, $attributes);

    if ($validateData->fails()) {
        $errors = $validateData->errors();
        foreach ($errors->all() as $message) {
          connectify('error','Erreur',$message);
        }
        return redirect("/contributor/".$id."/mod");
    }
    else {
      $contributor = Contributor::where('id',$id)->first();
      $contributor->firstname = $request->firstname;
      $contributor->lastname = $request->lastname;
      $contributor->email = $request->email;
      $contributor->phone = $request->phone;
      $contributor->street = $request->street;
      $contributor->city = $request->city;
      $contributor->postal_code = $request->postal_code;
      $contributor->diploma = $request->diploma;
      $contributor->contract = $request->contract;
      if ($request->school!="none") {
        $contributor->school()->associate($request->school);
      }
      else{
        $contributor->school()->dissociate();
      }
      $contributor->save();
      $contributor->subjects()->detach();
      $contributor->subjects()->attach($request->subjects);
      return redirect('/contributor/'.$id.'/show',);
    }
  }

  public function contributorsBySubject(Request $request){
    $contributors = Subject::where('id',$request->subject)->first()->contributor;
    return $contributors;
  }

  public function horaireDebut(request $request){
    $hdt = $request->lessons->starting;
    return view('ficheHoraire')->with($hdt);
  }
  
   public function del(Request $request, $id){
          $contributor = Contributor::where('id',$id)->first();
          if ( $contributor) {
               $contributor->delete();
              connectify('success','Succès',"L'intervenant a été supprimée 👍");
          }
          else {
            connectify('error','Erreur',"Erreur à la suppression de l'intervenant 😢");
          }
          return redirect('/contributor');
          }
}
