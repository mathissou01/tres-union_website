<?php

namespace App\Http\Controllers;

use App\Models\Familly;
use App\Models\Student;
use App\Models\Contributor;
use App\Models\Minorstudent;
use App\Models\CourseSession;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function home() {
        $intervenant = Contributor::where('contract', 'Intervenant')->count();
        $benevole = Contributor::where('contract', 'Bénévole')->count();
        $professeur = Contributor::where('contract', 'Professeur')->count();
        $stagiaire = Contributor::where('contract', 'Stagiaire')->count();
        $service_civique = Contributor::where('contract', 'Service civique')->count();

        $attente = Student::where('choices', 'Paiement en attente');
        $payer = Student::where('choices', 'Paiement effectué');
        $partiel = Student::where('choices', 'Paiement partiel');

        $attente_min = Minorstudent::where('choices', 'Paiement en attente');
        $payer_min = Minorstudent::where('choices', 'Paiement effectué');
         $partiel_min = Minorstudent::where('choices', 'Paiement partiel');

        return view('dashboard',['courseSessions'=>CourseSession::all()])->with(["minorstudent" => Minorstudent::all()])->with(["student" => Student::all()])
    ->with(["familly" => Familly::all()])->with(["contributors" => Contributor::all()])->with("intervenant", $intervenant)
    ->with("benevole", $benevole)->with("professeur", $professeur)->with("stagiaire", $stagiaire)->with("service_civique", $service_civique)->with("payer", $payer)->with("attente", $attente)
    ->with("payer_min", $payer_min)->with("attente_min", $attente_min)->with("partiel", $partiel)->with("partiel_min", $partiel_min);
    }
}
