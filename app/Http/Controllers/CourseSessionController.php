<?php

namespace App\Http\Controllers;
use DB;
use PDF;
use DateTime;
use App\Models\Level;
use App\Models\Lesson;
use App\Models\School;
use App\Models\Subject;
use App\Models\Contributor;
use Illuminate\Http\Request;
use App\Models\CourseSession;
use Illuminate\Http\Response;
use App\Models\SessionStudent;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContributorPlanningMailer;
use Illuminate\Support\Facades\Validator;
use App\Actions\GetTotalContributionsAction;
use App\Mail\ContributorPlanningPersoMailer;


class CourseSessionController extends Controller
{
    public function show()
    {
        return view('coursesession', ['courseSessions'=>CourseSession::all()]);
    }

    public function new()
    {
        return view('newcoursesession', ['schools'=>School::all()]);
    }
    //Fonction pour voir les intervenant dans chaque stage
    public function voir()
    {
        return view('fichecoursesession', ['courseSessions'=>CourseSession::all()], ["contributors" => Contributor::all(),"schools"=>School::all()]);
    }

    public function list($id)
    {
        $session = CourseSession::find($id);
        $courseSession = CourseSession::where('id', $id)->first();
        $levels = CourseSession::where('id', $id)->first();
        return view('fichestagepdf', ['session'=>$session], ["contributors" => Contributor::all(),'courseSession'=>$courseSession, 'levels'=>Level::all()]);
    }

    public function pdfliststage($id)
    {
        $session = CourseSession::find($id);
        $courseSession = CourseSession::where('id', $id)->first();
        $heuretotale = DB::select(DB::raw("SELECT CONCAT(contributors.firstname, ' ', contributors.lastname)
     as contributorFullName, CAST(SUM(TIMEDIFF(`endding`,`starting`))AS TIME) as totalHeure FROM lessons INNER JOIN
      contributors ON contributor_id = contributors.id WHERE course_session_id = $id GROUP BY contributors.firstname, contributors.lastname"));
        $pdf = PDF::loadView('fichestagepdf', ['session'=>$session], ["contributors" => Contributor::all(),'courseSession'=>$courseSession, 'levels'=>Level::all(),'heuretotale'=>$heuretotale]);
        return $pdf->download('liste-intervenant-stage'.$session->starting.'.pdf');
    }
    

    public function pdfshow($id)
    {
        $session = CourseSession::find($id);
        $courseSession = CourseSession::where('id', $id)->first();
     
        return view('fichestagepdf', ['session'=>$session], ["contributors" => Contributor::all(),'courseSession'=>$courseSession, 'levels'=>Level::all()]);
    }
    

    public function add(Request $request)
    {
        $messages = [
        'required' => 'Le champ ":attribute" est manquant',
        'string' => ':attribute n\'est pas valide',
        'integer' => ':attribute n\'est pas valide',
        'email' => 'L\'email n\'est pas valide',
        'date_format' => 'Le champ ":attribute" n\'est pas une date valide',
        'max-places.digits_between' => 'Le nombre place n\'est pas cohérent'
      ];

        $attributes = [
        'school' => 'Établissement',
        'start_date' => 'Date de début',
        'stop_date' => 'Date de fin',
        'max_places' => 'Nombre de places'
      ];

        $validateData = Validator::make($request->all(), [
        'school' => 'required | integer',
        'start_date' => 'required | date_format:"d/m/Y"',
        'stop_date' => ' required | date_format:"d/m/Y"',
        'max_places' => ' required | integer | digits_between:1,500',
      ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
                connectify('error', 'Erreur', $message);
            }
            return redirect("/session/new");
        } else {
            $start_date = Carbon::createFromFormat('d/m/Y', $request->start_date);
            $stop_date = Carbon::createFromFormat('d/m/Y', $request->stop_date);
            if ($start_date->gt($stop_date)) {
                connectify('error', 'Erreur', 'La date de fin doit être supérieur ou égale à la date de début du stage');
                return redirect("/session/new");
            } else {
                $courseSession = new CourseSession;
                $courseSession->starting = $start_date;
                $courseSession->endding = $stop_date;
                $courseSession->school_id = $request->school;
                $courseSession->places = $request->max_places;
                $courseSession->save();
            }
        }
        return redirect("/session");
    }

    public function show_details($id)
    {
        $courseSession = CourseSession::where('id', $id)->first();
        $finished = false;
        if (Carbon::createFromFormat('Y-m-d', $courseSession->starting) <= Carbon::now()) {
            $finished=true;
        }
        return view('coursesessiondetails', ['courseSession'=>$courseSession,'finished'=>$finished, 'registred'=>sizeof($courseSession->students)]);
    }

    public function show_mailers($id)
    {
        $courseSession = CourseSession::where('id', $id)->first();
        $finished = false;
        if (Carbon::createFromFormat('Y-m-d', $courseSession->starting) <= Carbon::now()) {
            $finished=true;
        }
        return view('coursesessionmailer', ['courseSession'=>$courseSession,'finished'=>$finished, 'registred'=>sizeof($courseSession->students),"contributor" => Contributor::all()]);
    }

    public function ficheDeStage($id)
    {
        $courseSession = CourseSession::where('id', $id)->first();

        $heuretotale = DB::select(DB::raw("SELECT CONCAT(contributors.firstname, ' ', contributors.lastname)
     as contributorFullName, CAST(SUM(TIMEDIFF(`endding`,`starting`))AS TIME) as totalHeure FROM lessons INNER JOIN
      contributors ON contributor_id = contributors.id WHERE course_session_id = $id GROUP BY contributors.firstname, contributors.lastname"));
    
        return view('ficheStage', ['courseSession'=>$courseSession,'heuretotale'=>$heuretotale]);
    }

    //tentative de fonction pour voire les intervenants
    public function show_Intervenant($id)
    {
        $contributor = Contributor::where('id', $id)->first();
        return view('contributordetails', ['contributor'=>Contributor::find($id)]);
    }
    public function show_ecole($id)
    {
        $school = School::where('id', $id)->first();
        return view('schooldetails', ['school'=>School::find($id)]);
    }
    public function show_Date($id)
    {
        setlocale(LC_ALL, 'fr_FR');
        $courseSession = CourseSession::where('id', $id)->first();
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        return view('sessiondate', ['courseSession'=>$courseSession,'dates'=>$date_starting]);
    }

    public function show_planning($id)
    {
        setlocale(LC_ALL, 'fr_FR');
        $courseSession = CourseSession::where('id', $id)->first();
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        $days = $date_starting->diffInDays($date_endding)+1;
        $date = [];
        $finished = false;
        if ($date_starting < Carbon::now()) {
            $finished = true;
        }
        $j=0;
        for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) {
            $date[$j]= clone $i;
            $j++;
        }
        return view('sessionplanning', ['courseSession'=>$courseSession,'days'=>$days,'dates'=>$date,'finished'=>$finished]);
    }

    public function modify_planning($id)
    {
        setlocale(LC_ALL, 'fr_FR');
        $courseSession = CourseSession::where('id', $id)->first();
        if ($courseSession===null) {
            abort(404);
        }
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        if ($date_starting < Carbon::now()) {
            abort(403, "Le stage a déjà commencé, ou est déjà terminé, il est impossible de modifier le planning");
        } else {
            $days = $date_starting->diffInDays($date_endding)+1;
            $date = [];
            $j=0;
            for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) {
                $date[$j]= clone $i;
                $j++;
            }
            return view('modifysessionplanning', ['courseSession'=>$courseSession,'days'=>$days,'dates'=>$date, 'subjects'=>Subject::all(), 'levels'=>Level::all(), 'contributors'=>Contributor::all()]);
        }
    }

    public function modify($id)
    {
        $courseSession = CourseSession::where('id', $id)->first();
        if (Carbon::createFromFormat('Y-m-d', $courseSession->starting) <= Carbon::now()) {
            abort(403, 'Le stage à déja commencé, il est imossible de le modifier');
        }
        return view('modifycoursesession', ['courseSession'=>$courseSession, 'schools'=>School::all()]);
    }

    public function applyModify(Request $request, $id)
    {
        $messages = [
      'required' => 'Le champ ":attribute" est manquant',
      'string' => ':attribute n\'est pas valide',
      'integer' => ':attribute n\'est pas valide',
      'email' => 'L\'email n\'est pas valide',
      'date_format' => 'Le champ ":attribute" n\'est pas une date valide',
      'max-places.digits_between' => 'Le nombre place n\'est pas cohérent'
    ];

        $attributes = [
      'school' => 'Établissement',
      'start_date' => 'Date de début',
      'stop_date' => 'Date de fin',
      'max_places' => 'Nombre de places'
    ];

        $validateData = Validator::make($request->all(), [
      'school' => 'required | integer',
      'start_date' => 'required | date_format:"d/m/Y"',
      'stop_date' => ' required | date_format:"d/m/Y"',
      'max_places' => ' required | integer | digits_between:1,500',
    ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
                connectify('error', 'Erreur', $message);
            }
            return redirect("/session/".$id."/modify");
        } else {
            $start_date = Carbon::createFromFormat('d/m/Y', $request->start_date);
            $stop_date = Carbon::createFromFormat('d/m/Y', $request->stop_date);
            if ($start_date->gt($stop_date)) {
                connectify('error', 'Erreur', 'La date de fin doit être supérieur ou égale à la date de début du stage');
                return redirect("/session/".$id."/modify");
            } else {
                $courseSession = CourseSession::where('id', $id)->first();
                if (Carbon::createFromFormat('Y-m-d', $courseSession->starting) <= Carbon::now()) {
                    abort(403, 'Le stage à déja commencé, il est imossible de le modifier');
                }
                $courseSession->starting = $start_date;
                $courseSession->endding = $stop_date;
                $courseSession->school_id = $request->school;
                $courseSession->places = $request->max_places;
                $courseSession->save();
            }
        }
        return redirect("/session/".$id."/show");
    }

    public function addLesson(Request $request)
    {
        $messages = [
      'required' => 'Le champ ":attribute" est manquant',
      'string' => ':attribute n\'est pas valide',
      'integer' => ':attribute n\'est pas valide',
      'date_format' => 'Le champ ":attribute" n\'est pas une date ou un horraire valide',
    ];

        $attributes = [
      'starting_time' => 'Heure Début',
      'endding_time' => 'Heure Fin',
      'date' => 'Date',
      'subject' => 'Matière',
      'contributor' => 'Intervenant',
      'session' => 'Stage',
      'level' => 'Niveau',
      'room' => 'Salle'
    ];

        $validateData = Validator::make($request->all(), [
      'starting_time' => 'required | date_format:"H:i"',
      'endding_time' => 'required | date_format:"H:i"',
      'date' => 'required | date_format:"d/m/Y"',
      'subject' => 'required | integer ',
      'contributor' => 'required | integer ',
      'session' => 'required | integer',
      'level' => 'required | integer',
      'room' => 'required | string'
    ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
                connectify('error', 'Erreur', $message);
            }
            return redirect(url()->previous());
        } else {
            $session = CourseSession::where('id', $request->session)->get();
            $starting_date = Carbon::createFromFormat('Y-m-d', $session[0]->starting);
            $endding_date = Carbon::createFromFormat('Y-m-d', $session[0]->endding);
            $current_date = Carbon::createFromFormat('d/m/Y', $request->date);
            $starting_time = Carbon::createFromFormat('H:m', $request->starting_time);
            $endding_time = Carbon::createFromFormat('H:m', $request->endding_time);
            if ($starting_time->gt($endding_time)) {
                connectify('error', 'Erreur', 'L\'heure de fin doit être supérieur ou égale à l\'heure de début de la session');
                return redirect(url()->previous());
            } elseif (sizeof($session)===0) {
                connectify('error', 'Erreur', 'Le stage pour lequel vous souhaitez ajouté cette session n\'existe pas');
                return redirect(url()->previous());
            } elseif ($starting_date < Carbon::now()) {
                connectify('error', 'Erreur', 'Le stage pour lequel vous souhaitez ajouté cette session à commencé, vous ne pouvez plus le modifier');
                return redirect(url()->previous());
            } elseif ($current_date->gt($endding_date) || $current_date->lt($starting_date)) {
                connectify('error', 'Erreur', 'Le jour de la session n\'est pas un jour présent dans la période de stage définie '.$current_date);
                return redirect(url()->previous());
            } elseif (sizeof(Subject::where('id', $request->subject)->get())===0) {
                connectify('error', 'Erreur', 'La matière enseigné à cette session n\'est pas connue par le logiciel');
                return redirect(url()->previous());
            } elseif (sizeof(Contributor::where('id', $request->contributor)->get())===0) {
                connectify('error', 'Erreur', 'L\'intervenant séléctionné n\'existe pas');
                return redirect(url()->previous());
            } elseif (sizeof(Level::where('id', $request->level)->get())===0) {
                connectify('error', 'Erreur', 'Le niveau séléctionné n\'existe pas');
                return redirect(url()->previous());
            } else {
                $lesson = new Lesson;
                $lesson->date = $current_date;
                $lesson->endding = $endding_time;
                $lesson->starting = $starting_time;
                $lesson->subject_id = $request->subject;
                $lesson->contributor_id = $request->contributor;
                $lesson->course_session_id = $request->session;
                $lesson->level_id = $request->level;
                $lesson->room = $request->room;
                $lesson->save();
            }
        }
        return redirect(url()->previous());
    }

    public function modifyLesson(Request $request)
    {
        $messages = [
      'required' => 'Le champ ":attribute" est manquant',
      'string' => ':attribute n\'est pas valide',
      'integer' => ':attribute n\'est pas valide',
      'date_format' => 'Le champ ":attribute" n\'est pas une date ou un horraire valide',
    ];

        $attributes = [
      'lesson' => 'Cours',
      'starting_time' => 'Heure Début',
      'endding_time' => 'Heure Fin',
      'contributor' => 'Intervenant',
      'level' => 'Niveau',
      'room' => 'Salle'
    ];

        $validateData = Validator::make($request->all(), [
      'starting_time' => 'required | date_format:"H:i"',
      'endding_time' => 'required | date_format:"H:i"',
      'contributor' => 'required | integer ',
      'level' => 'required | integer',
      'lesson' => 'required | integer',
      'room' => 'required | string'
    ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
                connectify('error', 'Erreur', $message);
            }
            return redirect(url()->previous());
        } else {
            $lesson = Lesson::where('id', $request->lesson)->first();
            if ($lesson===null) {
                connectify('error', 'Erreur', 'Le cours à modifier n\'exsite plus');
                return redirect(url()->previous());
            } else {
                $starting_time = Carbon::createFromFormat('H:i', $request->starting_time);
                $endding_time = Carbon::createFromFormat('H:i', $request->endding_time);
                if ($starting_time->gt($endding_time)) {
                    connectify('error', 'Erreur', 'L\'heure de fin doit être supérieur ou égale à l\'heure de début de la session');
                    return redirect(url()->previous());
                } elseif (sizeof(Contributor::where('id', $request->contributor)->get())===0) {
                    connectify('error', 'Erreur', 'L\'intervenant séléctionné n\'existe pas');
                    return redirect(url()->previous());
                } elseif (sizeof(Level::where('id', $request->level)->get())===0) {
                    connectify('error', 'Erreur', 'Le niveau séléctionné n\'existe pas');
                    return redirect(url()->previous());
                } else {
                    $lesson->endding = $endding_time;
                    $lesson->starting = $starting_time;
                    $lesson->contributor_id = $request->contributor;
                    $lesson->level_id = $request->level;
                    $lesson->room = $request->room;
                    $lesson->save();
                }
            }
        }
        return redirect(url()->previous());
    }

    public function publish($id)
    {
        $session = CourseSession::where('id', $id)->first();
        $session->published = true;
        $session->save();
        return redirect('/session');
    }

    public function halt($id)
    {
        $session = CourseSession::where('id', $id)->first();
        $session->published = false;
        $session->save();
        return redirect('/session');
    }


    public function lessonsByLevel(Request $request)
    {
        $query = Lesson::where(['level_id'=>$request->level,'course_session_id'=>$request->session])->get();
        $lessons = [];
        foreach ($query as $key => $lesson) {
            if ($lesson->courseSession->places > sizeof($lesson->students)) {
                array_push($lessons, $lesson);
            }
        }
    
        return $lessons;
    }

    public function deleteLesson(Request $request)
    {
        $lesson = Lesson::find($request->id);
        $lesson->delete();
        return response()->json(['id' => $request->id], 200);
    }

    public function email($id)
    {
        $student = SessionStudent::find($id);
        $courseSession = $student->courseSession();
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        $days = $date_starting->diffInDays($date_endding)+1;
        $date = [];
        $j=0;
        for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) {
            $date[$j]= clone $i;
            $j++;
        }
        return view('email.studentplanning', ['student'=>$student,'courseSession'=>$courseSession,'dates'=>$date]);
    }

    public function attendance($id)
    {
        $session =CourseSession::find($id);
        $html="";
        foreach ($session->lessons as $key => $lesson) {
            $view = view('attendance', ['lesson'=>$lesson]);
            $html .= $view->render();
        }

        $pdf = PDF::loadHTML($html);
        return $pdf->download('listes-appel-'.$session->starting.'-a-'.$session->endding.'.pdf');
    }


    public function email_contributors($id, Request $request)
    {
        $courseSession = CourseSession::where('id', $id)->first();
     
        $mails = DB::select(DB::raw("SELECT email, contributor_id, contributors.firstname as firstname, contributors.lastname as lastname FROM lessons INNER JOIN contributors
        ON contributor_id = contributors.id WHERE course_session_id = $id GROUP BY contributors.email, contributor_id, contributors.firstname, contributors.lastname"));
    
        $lesson = Lesson::find($id);
     
        $message = $request->input('message');
      
        foreach ($mails as $mail) {
            $url = URL::signedRoute('sessioncontributorplanning', ['id_contributor' => $mail->contributor_id , 'id_course_session' => $id]);
            Mail::to($mail->email)->send(new ContributorPlanningMailer($courseSession, $mail->firstname, $mail->lastname, $message, $url));
           
        }
         emotify('success','Les mails on été envoyés !');
            return redirect('/session');
    }
    // {id_course_session}/{id_contributor}
    public function planning_contributor($id_contributor, $id_course_session)
    {
           {
        setlocale(LC_ALL, 'fr_FR');
        $contributor = DB::table('contributors')->where('id', $id_contributor)->first();
        $courseSession = CourseSession::where('id', $id_course_session)->first();
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        $days = $date_starting->diffInDays($date_endding)+1;
        $date = [];
        $finished = false;
        if ($date_starting < Carbon::now()) {
            $finished = true;
        }
        $j=0;
        for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) {
            $date[$j]= clone $i;
            $j++;
        }
    }
        return view('contributorplanning',['contributor'=>$contributor,'courseSession'=>$courseSession,'dates'=>$date,'finished'=>$finished]);
    }

    public function email_contributors_perso($id, Request $request)
    {
        $courseSession = CourseSession::where('id', $id)->first();
         $message = $request->input('message');
        // $contributorid = Contributor::find($id);
        // dd($contributor = $request->input('email'));
       
        $contributor = $request->email;
        $message = $request->input('message_perso');

        $mail = DB::select(DB::raw("SELECT email FROM contributors WHERE id = $contributor" ));
        $infos = DB::select(DB::raw("SELECT firstname , lastname FROM contributors WHERE id = $contributor GROUP BY firstname, lastname" ));

        $lesson = Lesson::find($id);
      
        $url = URL::signedRoute('sessioncontributorplanning', ['id_contributor' => $contributor , 'id_course_session' => $id]);
        Mail::to($mail[0]->email)->send(new ContributorPlanningPersoMailer($courseSession, $infos[0]->firstname, $infos[0]->lastname, $message, $url));
      
         emotify('success','Le mail a été envoyé à !');
            return redirect('/session');
    }
}