<?php

namespace App\Http\Controllers;

use App\Models\Images;
use App\Models\School;
use App\Models\Familly;
use App\Models\Minorstudent;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Exports\MinorstudentExport;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Excel;

class FamillyController extends Controller
{
  private $excel;
  // Afficher toutes les familles
  public function show(){
    return view('familly',['famillies'=>Familly::all()]);
  }
// Afficher la page de création d'une famille
  public function new(){
    return view('newfamilly');
  }
// Fonction qui permet d'afficher la page qui permet d'ajouter un enfant donc Pré-BAC a certaine famille
  public function new_minor($id){
    $schools = School::all();
    $familly_id = $id;
      return view('newchild', compact("schools", "familly_id"));
      
  }
// Fonction qui permet de faire la requete qui permet d'ajouter un enfant donc Pré-BAC a certaine famille
  public function add_minor(Request $request, $id){
  
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'phone_number.digits' => 'Le numéro de téléphone doit contenir dix chiffres',
          'postal_code.digits' => 'Le code postal est un code à 5 chiffres',
          'string' => ':attribute n\'est pas valide',
          'integer' => ':attribute n\'est pas valide',
          'email' => 'L\'email n\'est pas valide',
        ];

        $attributes = [
          'first_name' => 'Prénom',
          'last_name' => 'Nom',
          'email_address' => 'Email',
          'phone_number' => 'Téléphone',
          'school' => 'Établissement',
          'level' => 'Classe',
          'option' => 'Option',
        ];

        $validateData = Validator::make($request->all(), [
          'first_name' => 'required | string',
          'last_name' => 'required | string',
          'phone_number' => 'nullable | digits:10 ',
          'email_address' => 'nullable | email ',
          'school' => 'required | integer',
          'option' => 'required | string',
          'level' => 'required | string'
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect("/familly/$id/student/new");
        }
        else {
          
          $minorstudent = new Minorstudent;
          $minorstudent->familly()->associate($id);
          $minorstudent->update(["familly_id" => $id]);
       
          $minorstudent->first_name = $request->first_name;
          $minorstudent->last_name = $request->last_name;
          $minorstudent->phone_number = $request->phone_number;
          $minorstudent->street_address = $request->street_address;
          $minorstudent->postal_code = $request->postal_code;
          $minorstudent->city = $request->city;
          $minorstudent->email_address = $request->email_address;
          $minorstudent->level = $request->level;
          $minorstudent->option = $request->option;
          if ($request->school!="none") {
              $minorstudent->school()->associate($request->school);
          }
          $minorstudent->save();
  
          // $filename = time() . '.' . $request->attestation->extension(); 
          //  $path = $request->file('attestation')->storeAs('attestations',$filename,'public');
          //  $image = new Images();
          //  $image->path = $path;
          //  $minorstudent->images()->save($image);

       //attestations a refaire
         
          return redirect('/familly');
        }

  }
  // Requete pour supprimer une étudiant mineur donc pré Bac qui est bien sur relier a une famille donc supprimer un étudiant qui appartient a une famille
  public function del_minor($id){
          $minorstudent = Minorstudent::where('id',$id)->first();
          if ($minorstudent) {
              $minorstudent->delete();
              connectify('success','Succès',"L'étudiant mineur a été supprimée");
          }
          else {
            connectify('error','Erreur',"Erreur à la suppression de l'étudiant!");
          }
          return back();
  }
    //  Afficher la page qui modifie un étudiant Pré-BAC
  public function showmod_minorstudent(Request $request, $id){
      $minorstudent = Minorstudent::where('id',$id)->first();
       if ($minorstudent) {
         return view('modminorstudent', ["minorstudent" => Minorstudent::find($id),"schools" => School::all(),
            'first_name'=>$minorstudent->first_name,
            'last_name'=>$minorstudent->last_name,
            'email_address'=>$minorstudent->email_address,
            'phone_number'=>$minorstudent->phone_number,
            'street_address'=>$minorstudent->street_address,
            'postal_code'=>$minorstudent->postal_code,
            'city'=>$minorstudent->city,
            'choices'=>$minorstudent->choices,
            'level'=>$minorstudent->level,
            'school'=>$minorstudent->school,
            'paiement'=>$minorstudent->paiement,
            'id'=>$minorstudent->id]);
           
       }
       else {
                 connectify('error','Erreur',"Erreur l'étudiant que vous souhaitez modifier n'éxiste pas");
                 return redirect('/student');
               }
  }
// Requete qui modifie un étudiant mineur donc Pré-BAC
  public function mod_minorstudent(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'first_name' => 'Prénom',
          'last_name' => 'Nom',
          'email_address' => 'Email',
          'phone_number' => 'Téléphone',
          'street_address' => 'Adresse',
          'postal_code' => 'Code postal',
          'city' => 'Ville',
          'choices' =>'Etat',
          'level' =>'Classe',
          'school' =>'Ecole',
          'id' => 'ID',
        ];

        $validateData = Validator::make($request->all(), [
          'first_name' => 'required | string',
          'last_name' => 'required | string',
          'phone_number' => 'required | digits:10',
          'street_address' => 'required | string',
          'postal_code' => 'required | digits:5',
          'city' => 'required | string',
          'level' => 'required | string',
          'school' => 'required | string',
          'paiement' => 'integer | nullable',
          'email_address' => 'required | email',
          'choices' => ['required', Rule::in(['Paiement en attente','Paiement effectué','Paiement partiel']),],
          'id' => 'required | integer',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $minorstudent = Minorstudent::where('id',$request->id)->first();
          if ($minorstudent) {
       
              $minorstudent->first_name = $request->first_name;
              $minorstudent->last_name = $request->last_name;
              $minorstudent->email_address = $request->email_address;
              $minorstudent->phone_number = $request->phone_number;
              $minorstudent->street_address = $request->street_address;
              $minorstudent->postal_code = $request->postal_code;
              $minorstudent->city = $request->city;  
              $minorstudent->level = $request->level;  
              $minorstudent->choices = $request->choices;
              $minorstudent->paiement = $request->paiement;
              if ($request->school!="none") {
              $minorstudent->school()->associate($request->school);
              }
              else{
                $minorstudent->school()->dissociate();
              }
          //   $filename = time() . '.' . $request->attestation->extension(); 
          //  $path = $request->file('attestation')->storeAs('attestations',$filename,'public');
          //  $image = new Images();
          //  $image->path = $path;
          //  $minorstudent->images()->save($image);
          // attestation a refaire
              $minorstudent->save();
              
              }
              else {
              connectify('error','Erreur',"La famille à modifier n'éxiste pas");
              }
              return redirect('/familly');
            }
  }
// Requete qui créer une nouvelle famille donc les infos du responsable legal
  public function add(Request $request){
    $messages = [
        'required' => 'Le champ ":attribute" est manquant',
        'phone_number.digits' => 'Le numéro de téléphone doit contenir dix chiffres',
        'postal_code.digits' => 'Le code postal est un code à 5 chiffres',
        'string' => ':attribute n\'est pas valide',
        'integer' => ':attribute n\'est pas valide',
        'email' => 'L\'email n\'est pas valide',
      ];

      $attributes = [
        'first_name' => 'Prénom',
        'last_name' => 'Nom',
        'email_address' => 'Email',
        'phone_number' => 'Téléphone',
        'street_address' => 'Adresse',
        'postal_code' => 'Code postal',
        'city' => 'Ville',
        'rules' => 'J\'accèpte le réglement',
      ];

      $validateData = Validator::make($request->all(), [
        'first_name' => 'required | string',
        'last_name' => 'required | string',
        'phone_number' => 'required | digits:10',
        'street_address' => 'required | string',
        'postal_code' => 'required | digits:5',
        'city' => 'required | string',
        'email_address' => 'required | email',
      ], $messages, $attributes);

      if ($validateData->fails()) {
          $errors = $validateData->errors();
          foreach ($errors->all() as $message) {
            connectify('error','Erreur',$message);
          }
          return redirect('/familly/new');
      }
      else {
  
        if ( $request->get('add_student_next') !== null ) {
          $familly = new Familly;
          $familly->first_name = $request->first_name;
          $familly->last_name = $request->last_name;
          $familly->phone_number = $request->phone_number;
          $familly->street_address = $request->street_address;
          $familly->postal_code = $request->postal_code;
          $familly->city = $request->city;
          $familly->email_address = $request->email_address;
          $familly->absence = 1;
          $familly->rules = 1;
          $familly->save();   
          return redirect("/familly/$familly->id/student/new")->with(['familly_id'=>$familly->id]);
        }
        else {
          $familly = new Familly;
          $familly->first_name = $request->first_name;
          $familly->last_name = $request->last_name;
          $familly->phone_number = $request->phone_number;
          $familly->street_address = $request->street_address;
          $familly->postal_code = $request->postal_code;
          $familly->city = $request->city;
          $familly->email_address = $request->email_address;
          $familly->absence = 1;
          $familly->rules = 1;
          $familly->save();
          return redirect('/familly');
        }
      }
  }
// Affichage des détails d'un repsonsable d'une famille donc pré-BAC
  public function show_details_minor_student($id){
  
    return view('minorstudentdetails', ["familly" => Familly::find($id), "minorstudent" => Minorstudent::find($id)]);
    
  }
// Affichage des détailsd'un étudiant mineur donc pré-BAC qui appartient a une famille
  public function show_details_only_minor_student($id){

    return view('minorstudentonlydetails', ["minorstudent" => Minorstudent::find($id)]);

  }
// Function pour supprimer une famille donc les infos du repsonsable legal et les étudiant associé a la famille
  public function del(Request $request, $id){
          $familly = Familly::where('id',$id)->first();
          if ($familly) {
              $familly->delete();
              connectify('success','Succès',"La famille a été supprimée 👊");
          }
          else {
            connectify('error','Erreur',"Erreur à la suppression de la famille 🕵️‍♀️");
          }
          return redirect('/familly');
  }
// Affichage de la page de modification d'une famille
  public function showMod(Request $request, $id){
        $familly = Familly::where('id',$id)->first();
        if ($familly) {
            return view('modfamilly',['first_name'=>$familly->first_name,
            'last_name'=>$familly->last_name,
            'email_address'=>$familly->email_address,
            'phone_number'=>$familly->phone_number,
            'street_address'=>$familly->street_address,
            'postal_code'=>$familly->postal_code,
            'city'=>$familly->city,
            'id'=>$familly->id]);
        }
        else {
          connectify('error','Erreur',"Erreur la famille que vous souhaitez modifier n'éxiste pas");
          return redirect('/familly');
        }
  }
// Requete pour modifier les infos d'une famille donc son responsable legal
  public function mod(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'first_name' => 'Prénom',
          'last_name' => 'Nom',
          'email_address' => 'Email',
          'phone_number' => 'Téléphone',
          'street_address' => 'Adresse',
          'postal_code' => 'Code postal',
          'city' => 'Ville',
          'id' => 'ID',
        ];

        $validateData = Validator::make($request->all(), [
          'first_name' => 'required | string',
          'last_name' => 'required | string',
          'phone_number' => 'required | digits:10',
          'street_address' => 'required | string',
          'postal_code' => 'required | digits:5',
          'city' => 'required | string',
          'email_address' => 'required | email',
          'id' => 'required | integer',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $familly = Familly::where('id',$request->id)->first();
          if ($familly) {
              $familly->first_name = $request->first_name;
              $familly->last_name = $request->last_name;
              $familly->email_address = $request->email_address;
              $familly->phone_number = $request->phone_number;
              $familly->street_address = $request->street_address;
              $familly->postal_code = $request->postal_code;
              $familly->city = $request->city;   
              $familly->save();
          }
          else {
              connectify('error','Erreur',"La famille à modifier n'éxiste pas");
          }
          return redirect('/familly');
        }
  }

  public function show_details($id){
      return view('famillydetails', ["familly" => Familly::find($id)]);
  }
// Function pour rechercher une famille
  public function search(){
  
      request()->validate([
      'q' => 'required|min:1'
      ]);

    $q = request()->input('q');

    $famillies = Familly::where('first_name', 'like', "%$q%")
    ->orWhere('last_name', 'like', "%$q%")
    ->orWhere('phone_number', 'like', "%$q%")
    ->paginate(6);
    return view ('resultfamilly_search')->with('famillies', $famillies);                                
  }
   
  public function __construct(Excel $excel){  
        $this->excel = $excel;
  }
//Function qui genere un excel avec les familles et ses étudiant associées
  public function  csvlistminorstudent(){    
     return $this->excel->download(new MinorstudentExport, 'liste_etudiant_mineurs_familles.xlsx');
  }
    
}
