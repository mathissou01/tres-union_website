<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Actions\Fortify\CreateNewUser;

class RegisterController extends Controller
{
    public function showRegister()
    {
        return view('auth.register');
    }

    public function register(Request $request)
    {
        $usercreation = new CreateNewUser();
        $usercreation->create(['name' => $request->name, 'email'=>$request->email,'password'=>$request->password,'password_confirmation'=>$request->password_confirmation]);
        emotify('success','L\'utilisateur à été ajouté !');
        return redirect(route('dashboard'));
    }
}
