<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\School;
use Image;

class SchoolController extends Controller
{
  // Afficher tout les écoles
    public function show(){
      return view('school',['schools'=>School::all()]);
    }
// Redirect à la page pour créer une école
    public function new(){
      return view('newschool');
    }
// Reqiete qui créer une école
    public function add(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'name' => 'Nom',
          'city' => 'Commune',
        ];

        $validateData = Validator::make($request->all(), [
          'name' => 'required | string',
          'city' => 'required | string',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect('/school/new');
        }
        else {
          $school = new School;
          $school->name = $request->name;
          $school->city = $request->city;
          $school->save();
          return redirect('/school');
        }
    }
// Requete supprimer une école
    public function del(Request $request, $id){
        $school = School::where('id',$id)->first();
        if ($school) {
            $school->delete();
            connectify('success','Succès',"L'établissement' a été supprimée 👋");
        }
        else {
          connectify('error','Erreur',"Erreur à la suppression de l'établissement 😤");
        }
        return redirect('/school');
    }
// Afficher la page pour modifier une école
    public function showMod(Request $request, $id){
        $school = School::where('id',$id)->first();
        if ($school) {
            return view('modschool',['name'=>$school->name,'city'=>$school->city,'id'=>$school->id]);
        }
        else {
          connectify('error','Erreur',"Erreur l'établissement que vous souhaitez modifier n'éxiste pas");
          return redirect('/school');
        }
    }
// Requete pour modifier une école
    public function mod(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'name' => 'Nom',
          'city' => 'Commune',
          'id' => 'ID',
        ];

        $validateData = Validator::make($request->all(), [
          'name' => 'required | string',
          'city' => 'required | string',
          'id' => 'required | integer',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $school = School::where('id',$request->id)->first();
          if ($school) {
              $school->name = $request->name;
              $school->city = $request->city;
              $school->save();
          }
          else {
              connectify('error','Erreur',"L'établissement à modifier n'éxiste pas");
          }
          return redirect('/school');
        }
    }
}
