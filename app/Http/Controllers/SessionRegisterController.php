<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CourseSession;
use App\Models\Level;
use App\Models\Subject;
use App\Models\Lesson;
use App\Mail\PlanningMailer;
use App\Models\SessionStudent;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class SessionRegisterController extends Controller
{
    public function show($id)
    {
        $courseSession = CourseSession::where('id',$id)->first();
        if ($courseSession ===  Null) {
            abort(404);
        }
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        if ($date_starting > Carbon::now() && $courseSession->published) {
            $days = $date_starting->diffInDays($date_endding)+1;
            $date = [];
            $j=0;
            for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) { 
            $date[$j]= clone $i;
            $j++;
            }
            return view('sessionregister',["levels"=>Level::all(), "courseSession"=>$courseSession, 'days'=>$days,'dates'=>$date, 'id' => $id, "subjects"=>Subject::all()]);
        }
        else if ($date_starting < Carbon::now() ){
            abort(403,'Les inscriptions pour ce stage sont terminées');
        }
        else{
            abort(403,'Ce stage n\'est pas encore ouvert aux inscriptions, veuillez patienter');
        }
        
    }

    public function register(Request $request)
    {
        $messages = [
            'sessionid.requires' => 'L\'inscription n\'est pas liée à un stage, rechargez la page et rééssayez',
            'lessons.required' => 'Vous devez choisir au moins un cours pour vous inscrire',
            'required' => 'Le champ ":attribute" est manquant',
            'string' => ':attribute n\'est pas valide',
            'integer' => ':attribute n\'est pas valide',
            'email' => 'L\'email n\'est pas valide',
            'date_format' => 'Le champ ":attribute" n\'est pas une date valide',
            'digits' => ':attribute n\'est pas valide'
          ];
    
          $attributes = [
            'sessionid' => 'Session',
            'student_lastname' => 'Nom de famille de l\'étudiant',
            'student_firstname'=> 'Prénom de l\'étudiant',
            'student_email'=> 'Email de l\'étudiant',
            'student_phone'=> 'Téléphone de l\'étudiant',
            'student_school'=> 'Établissement Scolaire',
            'level' => 'Niveau Scolaire',
            'lastname' => 'Nom de famille du responsable',
            'firstname'=> 'Prénom du responsable',
            'email'=> 'Email du responsable',
            'phone'=> 'Téléphone du responsable',
            'street' => 'Rue',
            'postal_code' => 'Code Postal',
            'city' => 'Ville',
            'lessons' => 'Cours'
          ];
    
          $validateData = Validator::make($request->all(), [
            'sessionid' => 'required',
            'student_lastname' => 'required | string',
            'student_firstname' => 'required | string',
            'student_email' => 'required | email',
            'student_phone' => 'nullable | digits:10',
            'student_school'=> 'required | string',
            'level'=> 'required | integer',
            'lastname' => 'required | string',
            'firstname' => 'required | string',
            'email' => 'required | email',
            'phone' => 'required | digits:10',
            'street' => 'required | string',
            'postal_code' => ' required | digits:5',
            'city' => 'required | string',
            'lessons' => ' required'
          ], $messages, $attributes);
    
          if ($validateData->fails()) {
              $errors = $validateData->errors();
              foreach ($errors->all() as $message) {
                connectify('error','Erreur',$message);
              }
              return redirect("/registertosession/".$request->sessionid);
          }
          else{
            $lessons = Lesson::find($request->lessons);
            //dd($lessons[0]->name);
            foreach ($lessons as $key => $lesson) {
                if ($lesson->course_session_id != $request->sessionid) {
                    connectify('error','Erreur','Le cours '.$lesson->subject->name .'-'.$lesson->level->name.' appartient à un autre stage');
                    return redirect("/registertosession/".$request->sessionid);
                }
                else if(!$lesson->courseSession->published){
                    connectify('error','Erreur','Le cours '.$lesson->subject->name .'-'.$lesson->level->name.' appartient à un stage pour lequel les inscriptions ne sont pas ouvertes');
                    return redirect("/registertosession/".$request->sessionid);
                }
                else if(Carbon::createFromFormat('Y-m-d', $lesson->courseSession->starting) < Carbon::now()){
                    connectify('error','Erreur','Le cours '.$lesson->subject->name .'-'.$lesson->level->name.' appartient à un stage qui à déjà commencé');
                    return redirect("/registertosession/".$request->sessionid);
                }
                else if ($lesson->level_id != $request->level) {
                    connectify('error','Erreur','Le cours '.$lesson->subject->name .'-'.$lesson->level->name.' ne correspond pas à votre niveau scolaire');
                    return redirect("/registertosession/".$request->sessionid);
                }
                else if (sizeof($lesson->students)>=12){
                    connectify('error','Erreur','Le cours '.$lesson->subject->name .'-'.$lesson->level->name.' est déjà complet');
                    return redirect("/registertosession/".$request->sessionid);
                }
                else{
                    foreach ($lessons as $keylesson => $otherlesson) {
                        if ($lesson->date == $otherlesson->date && $lesson->id != $otherlesson->id) {
                            $start_1 = Carbon::createFromFormat('H:m:s', $lesson->starting);
                            $start_2 = Carbon::createFromFormat('H:m:s', $otherlesson->starting);
                            $end_1 = Carbon::createFromFormat('H:m:s', $lesson->endding);
                            $end_2 = Carbon::createFromFormat('H:m:s', $otherlesson->endding);
                            if (($start_1->gte($start_2) && $start_1->lt($end_2)) || ($end_1->gt($start_2) && $end_1->lte($end_2) ) || ($start_2->gte($start_1) && $start_2->lt($end_1)) || ($end_2->gt($start_1) && $end_2->lte($end_1))) {
                                connectify('error','Erreur','Vous avez séléctionné deux cours sur la même plage horraire !');
                                return redirect("/registertosession/".$request->sessionid);
                            }
                        }
                    }
                }              
            }
            $all_lessons = Lesson::where('course_session_id',$request->sessionid)->get();
            
            $already_registred = False;
            foreach ($all_lessons as $key => $lesson) {
                foreach ($lesson->students as $key => $student) {
                    if ($student->student_email===$request->student_email) {
                        $already_registred = True;
                    }
                }
                
            }
            if ($already_registred) {
                connectify('error','Erreur','Cet étudiant est déjà inscrit au stage, n\'oublier pas d\'utiliser une adresse mail différente pour chaque étudiant inscrit');
                return redirect("/registertosession/".$request->sessionid);
            }
            $student = new SessionStudent;
            $student->student_firstname = $request->student_firstname;
            $student->student_lastname = $request->student_lastname;
            $student->student_email = $request->student_email;
            if ($request->student_phone!="") {
            $student->student_phone = $request->student_phone;
            }
            $student->level_id = $request->level;
            $student->school = $request->student_school;
            $student->legal_firstname = $request->firstname;
            $student->legal_lastname = $request->lastname;
            $student->legal_email = $request->email;
            $student->legal_phone = $request->phone;
            $student->street = $request->street;
            $student->postal_code = $request->postal_code;
            $student->city = $request->city;
            $student->course_session_id = $request->sessionid;
            $student->save();
            $student->lessons()->attach($request->lessons);
        }
        $courseSession = $student->courseSession;
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        $days = $date_starting->diffInDays($date_endding)+1;
        $date = [];
        $j=0;
        for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) { 
        $date[$j]= clone $i;
        $j++;
        }
        $url = URL::signedRoute('sessionstudentplanning', ['id' => $student->id]);
        Mail::to($student->student_email)->send(new PlanningMailer($student, $courseSession, $url));
        emotify('success','Votre inscription est réussie !');
        return redirect("/registertosession/".$request->sessionid);
    }
}
