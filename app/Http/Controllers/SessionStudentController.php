<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\Models\CourseSession;
use App\Models\SessionStudent;
use Illuminate\Support\Carbon;
use App\Exports\SessionStudentExport;

use Maatwebsite\Excel\Excel;

class SessionStudentController extends Controller
{
    private $excel;
    public function list($id)
    {
        $session = CourseSession::find($id);
        return view('sessionstudent',['session'=>$session]);
    }
// Fonction création de la liste (format PDF) des étudiant inscrit au stage 
    public function pdflist($id)
    {
        $session = CourseSession::find($id);
        $pdf = PDF::loadView('sessionstudentpdf',['session'=>$session]);
        return $pdf->download('liste-eleves-stage-'.$session->starting.'.pdf');
    }

    public function pdfshow($id)
    {
        $session = CourseSession::find($id);
        return view('sessionstudentpdf',['session'=>$session]);
    }


    public function detail($id){
         $student = SessionStudent::find($id);
        return view('sessionstudentdetails',['student'=>$student]);
    }

    public function planning($id)
    {
        $student = SessionStudent::find($id);
        $courseSession = $student->courseSession;
        $date_starting = Carbon::createFromFormat('Y-m-d', $courseSession->starting);
        $date_endding = Carbon::createFromFormat('Y-m-d', $courseSession->endding);
        $days = $date_starting->diffInDays($date_endding)+1;
        $date = [];
        $j=0;
        for ($i= clone $date_starting; $i->lte($date_endding); $i->addDays(1)) { 
            $date[$j]= clone $i;
        $j++;
        }
        return view('studentplanning',['student'=>$student,'courseSession'=>$courseSession,'dates'=>$date]);
    }
    // Fonction création de la liste (format Excel) des étudiant inscrit au stage 
    public function __construct(Excel $excel)
    {
        $this->excel = $excel;
    }
    public function csvlist(){
        
     return $this->excel->download(new SessionStudentExport, 'liste_etudiant_stage.xlsx');

     }
}
