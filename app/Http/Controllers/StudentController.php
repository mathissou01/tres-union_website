<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\Familly;
use App\Models\Student;
use App\Models\Minorstudent;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Exports\StudentExport;
use Illuminate\Validation\Rule;
use App\Controllers\FamillyController;
use Illuminate\Support\Facades\Validator;


class StudentController extends Controller
{
  private $excel;
// Afficher tout les étudiants des deux tables donc POST-Bac et Pré-BAC
    public function show(){
      return view('student',['students'=>Student::all()])
      ->with('minorstudents', Minorstudent::all());
    }
// Page qui affiche la création d'un étudiant POT-Bac
    public function new_major(){
      return view('newstudent')->with(["schools" => School::all()]);
    }
// Requete qui créer une étudiant (majeur) donc POST-Bac 
    public function add_major(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'phone_number.digits' => 'Le numéro de téléphone doit contenir dix chiffres',
          'postal_code.digits' => 'Le code postal est un code à 5 chiffres',
          'string' => ':attribute n\'est pas valide',
          'integer' => ':attribute n\'est pas valide',
          'email' => 'L\'email n\'est pas valide',
        ];

        $attributes = [
          'first_name' => 'Prénom',
          'last_name' => 'Nom',
          'email_address' => 'Email',
          'phone_number' => 'Téléphone',
          'school' => 'Établissement',
          'level' => 'Etude',
          'rules' => 'Réglement',
          'school' => 'Ecole'
        ];

        $validateData = Validator::make($request->all(), [
          'first_name' => 'required | string',
          'last_name' => 'required | string',
          'level' => 'string',
          'phone_number' => 'nullable | digits:10 ',
          'email_address' => 'nullable | email ',
          'school' => 'required | string',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect("/student/new");
        }
        else {
          $student = new Student;
          $student->first_name = $request->first_name;
          $student->last_name = $request->last_name;
          $student->phone_number = $request->phone_number;
          $student->street_address = $request->street_address;
          $student->postal_code = $request->postal_code;
          $student->city = $request->city;
          $student->email_address = $request->email_address;
          $student->level = $request->level;
        
          $student->rules = 1;
             if ($request->school!="none") {
              $student->school()->associate($request->school);
          }
          $student->save();
        
          return redirect('/student');
        }
    }
    // Requete qui supprime une étudiant POST-Bac
    public function del($id){
          $student = Student::where('id',$id)->first();
          if ($student) {
              $student->delete();
              connectify('success','Succès',"L'étudiant majeur a été supprimée 🙏");
          }
          else {
            connectify('error','Erreur',"Erreur à la suppression de l'étudiant 🤯");
          }
          return redirect('/student');
      }
// Afficher la page pour modifier un étudiant POST-BAC
    public function showMod($id){
    if (Student::find($id)===Null) {
      abort(404);
    }
   
    return view('modstudent', ["student" => Student::find($id),"schools" => School::all()]);
  }
// Requete qui modifie une étudiant POST-Bac
    public function mod(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
          'integer' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'first_name' => 'Prénom',
          'last_name' => 'Nom',
          'email_address' => 'Email',
          'phone_number' => 'Téléphone',
          'street_address' => 'Adresse',
          'postal_code' => 'Code postal',
          'city' => 'Ville',
          'school' => 'Ecole',
          'level'=> 'Etude',
          'choices' => 'Etat',
          'id' => 'ID',
        ];

        $validateData = Validator::make($request->all(), [
          'first_name' => 'required | string',
          'last_name' => 'required | string',
          'phone_number' => 'required | digits:10',
          'street_address' => 'required | string',
          'postal_code' => 'required | digits:5',
          'level'=> 'required | string',
          'school' => 'required | string',
          'city' => 'required | string',
          'email_address' => 'required | email',
          'choices' => ['required', Rule::in(['Paiement en attente','Paiement effectué','Paiement partiel']),],
          'id' => 'required | integer',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect(url()->previous());
        }
        else {
          $student = Student::where('id',$request->id)->first();
          if ($student) {
              $student->first_name = $request->first_name;
              $student->last_name = $request->last_name;
              $student->email_address = $request->email_address;
              $student->phone_number = $request->phone_number;
              $student->street_address = $request->street_address;
              $student->postal_code = $request->postal_code;
              $student->city = $request->city;
              $student->paiement = $request->paiement;
              $student->level = $request->level;
              $student->choices = $request->choices;  
           
              if ($request->school!="none") {
        $student->school()->associate($request->school);
      }
      else{
        $student->school()->dissociate();
      }
              $student->save();
          }
          else {
              connectify('error','Erreur',"L'étudiant' à modifier n'éxiste pas");
          }
          return redirect('/student');
        }
    }
// Afficher les details d'un étudiant POST-Bac donc tout ses infos sur sa bdd
    public function show_details($id){
    return view('studentdetails', ["student" => Student::find($id)]);
    }

    /* Fonction de recherche de tout les étudiant donc pré-BAC et POST-BAC */
    public function search(){
    
      request()->validate([
      'q' => 'required|min:1'
      ]);

      $q = request()->input('q');

      $students = Student::where('first_name', 'like', "%$q%")
      ->orWhere('last_name', 'like', "%$q%")
      ->orWhere('phone_number', 'like', "%$q%")
      ->paginate(10);
      
      $minorstudents = Minorstudent::where('first_name', 'like', "%$q%")
      ->orWhere('last_name', 'like', "%$q%")
      ->orWhere('phone_number', 'like', "%$q%")
      ->paginate(10);
      return view ('result_search')->with('students', $students)
      ->with('minorstudents', $minorstudents);
                                  
    }
  //  Afficher le code bar d'un étudiant POST-BAC
    public function show_codebar_student(Request $request, $id){
        return view('barcode_student', ["student" => Student::find($id)]);
  
    }
 //  Afficher le code bar d'un étudiant Pré-BAC
    public function show_codebar_minorstudent(Request $request, $id){
        return view('barcode_minorstudent', ["minorstudent" => Minorstudent::find($id)]);
  
    }
 //  Requete qui genere un excel avec tout les étudiant POST-BAC et leur information associé
    public function __construct(Excel $excel)
    {
    $this->excel = $excel;
    }

    public function csvliststudent(){  
     return $this->excel->download(new StudentExport, 'liste_etudiant_majeur.xlsx');
    }
}

