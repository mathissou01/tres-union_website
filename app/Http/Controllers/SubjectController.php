<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Subject;

class SubjectController extends Controller
{
  // Afficher tout les matières
    public function show(){
        return view('subject',['subjects'=>Subject::all()]);
    }
// Afficher la page pour créer une matières
    public function new(){
      return view('newsubject');
    }
//Requete qui ajoute une matières
    public function add(Request $request){
      $messages = [
          'required' => 'Le champ ":attribute" est manquant',
          'string' => ':attribute n\'est pas valide',
        ];

        $attributes = [
          'name' => 'Nom',
        ];

        $validateData = Validator::make($request->all(), [
          'name' => 'required | string',
        ], $messages, $attributes);

        if ($validateData->fails()) {
            $errors = $validateData->errors();
            foreach ($errors->all() as $message) {
              connectify('error','Erreur',$message);
            }
            return redirect('/subject/new');
        }
        else {
          $subject = new Subject;
          $subject->name = $request->name;
          $subject->save();
          return redirect('/subject');
        }
    }
// Requete qui supprime une matières
    public function del(Request $request, $id){
        $subject = Subject::where('id',$id)->first();
        if ($subject) {
            $subject->delete();
            connectify('success','Succès','La matière a été supprimée 🤟');
        }
        else {
          connectify('error','Erreur','Erreur à la suppression de la matière 😟');
        }
        return redirect('/subject');
    }
}
