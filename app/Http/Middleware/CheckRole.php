<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
      if ( Auth::user()->hasRole('admin')) {
        return $next($request);
      }
      else {
        connectify('error','Erreur','Vous n\'avez pas l\'authorisation d\'accéder à cette partie du site');
        return redirect('dashboard');
      }

    }
}
