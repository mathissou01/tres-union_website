<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\CourseSession;
use App\Models\Contributor;
use App\Models\Lesson;

class ContributorPlanningMailer extends Mailable
{
    use Queueable, SerializesModels;

    public $courseSession;
    public $firstname;
    public $lastname;
    public $message;
    public $url;
   
 
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(CourseSession $courseSession, $firstname, $lastname, $message, $url)
    {
        $this->courseSession = $courseSession;
        $this->firstname =  $firstname;
        $this->lastname = $lastname;
        $this->message = $message;
        $this->url = $url;
      
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('email.contributorplanning')
        ->subject('Tres Union : Planning de stage');
        // ->from('test.tresunion@gmail.com');
     
    }
}
