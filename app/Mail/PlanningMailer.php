<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\SessionStudent;
use App\Models\CourseSession;

class PlanningMailer extends Mailable
{
    use Queueable, SerializesModels;

    public $student;
    public $courseSession;
    public $url;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(SessionStudent $student, CourseSession $courseSession, $url)
    {
        $this->student = $student;
        $this->courseSession = $courseSession;
        $this->url = $url;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.studentplanning');
    }
}
