<?php

namespace App\Models;


use App\Models\Student;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Carbon;
use App\Models\Minorstudent;


class Appel extends Model
{
    use HasFactory;

    protected $fillable = [
        'createur'
    ];

    // public function students(){
    //     return $this->BelongsToMany(Student::class, "appel_student")->withTimestamps();
    // }
    public function minorstudents(){
        return $this->BelongsToMany(Minorstudent::class, "appel_students")->withPivot('present')->withTimestamps();
    }
    // public function students(){
    //     return $this->BelongsToMany(Student::class, "appel_students")->withPivot('present')->withTimestamps();
    // }

}



