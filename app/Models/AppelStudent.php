<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Student;
use App\Models\Appel;
use App\Models\Minorstudent;

class AppelStudent extends Model
{
    use HasFactory;

    protected $fillable = [
        'present','motif',
    ];
     
   
}
