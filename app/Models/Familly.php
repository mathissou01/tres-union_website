<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use App\Models\Minorstudent;

class Familly extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name','last_name','email_address','phone_number','street_address','postal_code','city','rules','absence',
    ];

       public function minorstudents()
       {
           return $this->hasMany(Minorstudent::class);
       }
}

