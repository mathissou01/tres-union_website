<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Contributor;
use App\Models\CourseSession;
use App\Models\Subject;
use App\Models\SessionStudent;

class Lesson extends Model
{
    use HasFactory;
    public function students()
    {
        return $this->belongsToMany(SessionStudent::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
    
    public function contributor()
    {
        return $this->belongsTo(Contributor::class);
    }

    public function courseSession()
    {
        return $this->belongsTo(CourseSession::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }
}
