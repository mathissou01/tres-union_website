<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
Use App\Models\Familly;
use App\Models\Traits\HasUuid;
use App\Models\Appel;

class Minorstudent extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = [
        'first_name','last_name','email_address','phone_number','street_address','postal_code','city','school','rules','choices',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'int';

    public function familly()
    {
        return $this->belongsTo(Familly::class);
    }
    public function school()
    {
        return $this->belongsTo(School::class);
    }
    public function appels(){
        return $this->BelongsToMany(Appel::class);
}
public function images()
    {
        return $this->hasOne(Images::class);
    }

}
