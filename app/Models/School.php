<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Contributor;
use App\Models\CourseSession;
use App\Models\Student;

class School extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'city',
    ];

    public function contributors()
    {
        return $this->hasMany(Contributor::class);
    }

    public function courseSession()
    {
        return $this->hasMany(CourseSession::class);
    }
    public function minorstudents()
    {
        return $this->hasMany(Minorstudent::class);
    }
    public function students()
    {
        return $this->hasMany(Student::class);
    }
}
