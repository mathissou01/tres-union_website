<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Lesson;


class SessionStudent extends Model
{
    use HasFactory;
    public function lessons()
    {
        return $this->belongsToMany(Lesson::class);
    }

    public function courseSession()
    {
        return $this->belongsTo(CourseSession::class);
    }

    public function level()
    {
        return $this->belongsTo(Level::class);
    }
    
}
