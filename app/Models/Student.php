<?php

namespace App\Models;

use App\Models\Appel;
use App\Models\School;
use Illuminate\Support\Str;
use App\Models\Traits\HasUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Student extends Model
{
    use HasFactory, HasUuid;

    protected $fillable = [
        'first_name','last_name','email_address','phone_number','street_address','city','postal_code','school','level','rules','choices',
    ];
    protected $primaryKey = 'id';
    protected $keyType = 'int';

   public function school()
    {
        return $this->belongsTo(School::class);
    }
    }
    

