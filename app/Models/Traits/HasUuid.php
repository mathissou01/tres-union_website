<?php
namespace App\Models\Traits;

use Illuminate\Support\Str;

trait HasUuid {

    protected static function bootHasUuid()
    {
        static::creating(function($model){
          if(empty($model->uuid)) {
            $excludedChars = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
            
            $model->uuid = str_replace($excludedChars, '', Str::uuid()->getHex());
            $model->uuid = substr("$model->uuid",0,12);

            $digits =(string)$model->uuid;
            $even_sum = $digits[1] + $digits[3] + $digits[5] + $digits[7] + $digits[9] + $digits[11];
            $even_sum_three = $even_sum * 3;
            $odd_sum = $digits[0] + $digits[2] + $digits[4] + $digits[6] + $digits[8] + $digits[10];
            $total_sum = $even_sum_three + $odd_sum;
            $next_ten = (ceil($total_sum/10))*10;
            $check_digit = $next_ten - $total_sum;
            $model->uuid = $digits . $check_digit;
          }
            
        });
        
    }
    

}
?>