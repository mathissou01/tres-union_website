<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('session_students', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('student_lastname');
            $table->string('student_firstname');
            $table->string('student_email');
            $table->string('student_phone')->nullable();
            $table->integer('level_id');
            $table->string('school');
            $table->string('legal_lastname');
            $table->string('legal_firstname');
            $table->string('legal_email');
            $table->string('legal_phone');
            $table->string('street');
            $table->string('postal_code');
            $table->string('city');
            $table->integer('course_session_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('session_students');
    }
}
