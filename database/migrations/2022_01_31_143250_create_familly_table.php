<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamillyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('famillies', function (Blueprint $table) {
        $table->id();
            $table->timestamps();
            $table->string("first_name");
            $table->string("last_name");
            $table->string("email_address");
            $table->string("phone_number")->nullable();
            $table->string("street_address");
            $table->string("city");
            $table->string("postal_code");
            $table->boolean("absence");  
            $table->boolean("rules");
            }); 
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('famillies');
    }
}
