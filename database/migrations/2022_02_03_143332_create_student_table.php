<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {     
          $table->id();
          $table->uuid('uuid')->unique();
          $table->timestamps();
          $table->string("first_name");
          $table->string("last_name");
          $table->string("email_address");
          $table->string("phone_number")->nullable();
          $table->string("street_address");
          $table->string("city");
          $table->string("postal_code");
          $table->integer("school_id")->nullable();
          $table->string("level");
          $table->boolean("rules");
          $table->enum('choices', ['Paiement en attente', 'Paiement effectué','Paiement partiel'])->default('Paiement en attente');
          $table->integer("paiement")->nullable();  
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
