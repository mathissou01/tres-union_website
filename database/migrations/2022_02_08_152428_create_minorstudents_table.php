<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMinorstudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('minorstudents', function (Blueprint $table) {
          $table->id();
          $table->uuid('uuid')->unique();
          $table->unsignedBigInteger('familly_id');
          $table->foreign('familly_id')->references('id')->on('famillies')->onDelete('cascade');
          $table->string("first_name");
          $table->string("last_name");
          $table->string("email_address");
          $table->string("phone_number");
          $table->string("street_address");
          $table->string("city");
          $table->string("postal_code");
          $table->integer("school_id")->nullable();
          $table->enum('level', ['CP', 'CE1','CE2','CM1','CM2','6ème','5ème','4ème','3ème','2nd','1ère','Terminale']);
          $table->enum('option', ['Aucune option','Histoire Géographique','Maths','Littérature, Langues et Cultures de Antiquité',
          'Humanités, Littérature et Philosophie','Numérique et Sciences Informatiques','SVT','Sciences de ingénieur',
          'Sciences Économiques et Sociales','Physique Chimie','Arts:Histoire des arts...','Biologie Écologie','Éducation Physique']);
          $table->enum('choices', ['Paiement en attente', 'Paiement effectué','Paiement partiel'])->default('Paiement en attente'); 
        	 $table->timestamps();
          $table->integer("paiement")->nullable();
        });
    }
   
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('minorstudents');
    }
}
