<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppelStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appel_students', function (Blueprint $table) {
            $table->id();
            $table->foreignId('appel_id')->constrained()->onDelete('cascade');
            $table->foreignId('minorstudent_id')->nullable()->constrained()->onDelete('cascade');
            // $table->foreignId('student_id')->nullable()->constrained();
            $table->boolean('present')->default(false);
          
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appel_students');
    }
}
