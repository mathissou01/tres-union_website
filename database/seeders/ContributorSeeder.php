<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Contributor;

class ContributorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Contributor::create([
            'lastname'=>'Roubet',
            'firstname'=>'Henry',
            'email'=>'test@mail.test',
            'phone'=>'0647896321',
            'street'=>'85 chemin du clef',
            'city'=>'Creches',
            'postal_code'=>'01540',
            'rules' =>'1',
            'diploma' =>'Bac',
            'contract' =>'Intervenant',
            'school_id' =>'1',
        ]);
         Contributor::create([
            'lastname'=>'Yuyu',
            'firstname'=>'Lolo',
            'email'=>'test2@mail.test',
            'phone'=>'0647896321',
            'street'=>'85 chemin du clef',
            'city'=>'Creches',
            'postal_code'=>'01540',
            'rules' =>'1',
            'diploma' =>'Bac',
            'contract' =>'Intervenant',
            'school_id' =>'2',
        ]);
    }
}
