<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      Model::unguard();

          $this->call(RolesTableSeeder::class);
          $this->call(LevelSeeder::class);
          $this->call(UserSeeder::class);
          $this->call(SchoolSeeder::class);
          $this->call(SubjectSeeder::class);
          $this->call(ContributorSeeder::class);
          $this->call(FamillySeeder::class);
          $this->call(MinorstudentSeeder::class);
          $this->call(StudentSeeder::class);

      Model::reguard();
    }
}
