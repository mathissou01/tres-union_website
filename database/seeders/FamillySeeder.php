<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Familly;

class FamillySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Familly::create([
            'last_name'=>'Roubet',
            'first_name'=>'Henry',
            'email_address'=>'HenryRoubets@gmail.com',
            'phone_number'=>'0647896321',
            'street_address'=>'85 chemin du clef',
            'city'=>'Creches',
            'postal_code'=>'01540',
            'absence' =>'1',
            'rules' =>'1',
        ]);
         Familly::create([
            'last_name'=>'Guilbert',
            'first_name'=>'Ludovic',
            'email_address'=>'Ludgui@gmail.com',
            'phone_number'=>'0625896321',
            'street_address'=>'854 chemin du roti',
            'city'=>'Creches',
            'postal_code'=>'01521',
            'absence' =>'1',
            'rules' =>'1',
        ]);
    }
}
