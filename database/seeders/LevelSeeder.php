<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Level;

class LevelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Level::create([
            'name' => 'CM2',
        ]);
        Level::create([
            'name' => '6éme',
        ]);
        Level::create([
            'name' => '5éme',
        ]);
        Level::create([
            'name' => '4éme',
        ]);
        Level::create([
            'name' => '3éme',
        ]);
        Level::create([
            'name' => '2nd',
        ]);
        Level::create([
            'name' => '1ere',
        ]);
        Level::create([
            'name' => 'Term',
        ]);
        Level::create([
            'name' => 'Autre',
        ]);
    }
}
