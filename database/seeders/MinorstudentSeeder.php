<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Minorstudent;

class MinorstudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Minorstudent::create([
            'familly_id'=>'1',
            'first_name'=>'Matteo',
            'last_name'=>'Roubet',
            'email_address'=>'MatRoub@gmail.com',
            'phone_number'=>'0643987485',
            'street_address'=>'85 chemin du clef',
            'postal_code'=>'01458',
            'city'=>'Creches',
            'school_id' => '1',
            'option' => 'SVT',
            'level'=>'CE1',   
        ]);
        Minorstudent::create([
            'familly_id'=>'1',
            'first_name'=>'Lucye',
            'last_name'=>'Roubet',
            'email_address'=>'Luroubve@gmail.com',
            'phone_number'=>'0648587485',
            'street_address'=>'85 chemin du clef',
            'postal_code'=>'01458',
            'city'=>'Creches',   
            'school_id' => '2',
            'option' => 'SVT',
            'level'=>'Terminale'
        ]);
         Minorstudent::create([
            'familly_id'=>'2',
            'first_name'=>'Mathilde',
            'last_name'=>'Guilbert',
            'email_address'=>'Mathilde05@gmail.com',
            'phone_number'=>'0688587485',
            'street_address'=>'85 chemin du Routi',
            'postal_code'=>'01425',
            'city'=>'Creches',   
            'school_id' => '3',
            'option' => 'Numérique et Sciences Informatiques',
            'level'=>'1ère'
        ]);
         Minorstudent::create([
            'familly_id'=>'2',
            'first_name'=>'Marta',
            'last_name'=>'Guilbert',
            'email_address'=>'Maartdlde05@gmail.com',
            'phone_number'=>'0688512485',
            'street_address'=>'85 chemin du Routi',
            'postal_code'=>'01425',
            'city'=>'Creches',   
            'school_id' => '3',
            'option' => 'Numérique et Sciences Informatiques',
            'level'=>'1ère'
        ]);
         Minorstudent::create([
            'familly_id'=>'2',
            'first_name'=>'Valérie',
            'last_name'=>'Guilbert',
            'email_address'=>'Valérlde05@gmail.com',
            'phone_number'=>'0674587485',
            'street_address'=>'85 chemin du Routi',
            'postal_code'=>'01425',
            'city'=>'Creches',   
            'school_id' => '3',
            'option' => 'Numérique et Sciences Informatiques',
            'level'=>'1ère'
        ]);
    }
}
