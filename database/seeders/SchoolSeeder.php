<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\School;

class SchoolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        School::create([
            'name'=>'Le Chapitre',
            'city'=>'Chenôve',
        ]);
        School::create([
            'name'=>'Édouard Herriot',
            'city'=>'Chenôve',
        ]);
        School::create([
            'name'=>'Antoine',
            'city'=>'Chenôve',
        ]);
        School::create([
            'name'=>'Jean Rostand',
            'city'=>'Quetigny',
        ]);
        School::create([
            'name'=>'Jean-Philippe Rameau',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Jean-Marc Boivin',
            'city'=>'Chevigny',
        ]);
         School::create([
            'name'=>'Albert Camus',
            'city'=>'Genlis',
        ]);
         School::create([
            'name'=>'André Malraux',
            'city'=>'Dijon',
        ]);
         School::create([
            'name'=>'Les Arcades',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Boris Vian',
            'city'=>'Talant',
        ]);
        School::create([
            'name'=>'Camille Claudel',
            'city'=>'Chevigny-Saint-Sauveur',
        ]);
         School::create([
            'name'=>'Carnot',
            'city'=>'Dijon',
        ]);
          School::create([
            'name'=>'Le Castel',
            'city'=>'Dijon',
        ]);
         School::create([
            'name'=>'Champollion',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Charles de Gaulle',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Clos de Pouilly',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Gaston Bachelard',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Gaston Roupnel',
            'city'=>'Dijon',
        ]);
         School::create([
            'name'=>'Gustave Eiffel',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Henri Dunant',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Hippolyte Fontaine',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Les Lentillères',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Marcelle Pardé',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Les Marcs d\'Or',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Montchapet',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Notre-Dame',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Olivier de Serres',
            'city'=>'Quetigny',
        ]);
         School::create([
            'name'=>'Le Parc',
            'city'=>'Dijon',
        ]);
         School::create([
            'name'=>'Gaston Roupnel',
            'city'=>'Dijon',
        ]);
           School::create([
            'name'=>'Roland Dorgelès',
            'city'=>'Longvic',
        ]);
        School::create([
            'name'=>'Marcelle Pardé',
            'city'=>'Dijon',
        ]);
        School::create([
            'name'=>'Marcelle Pardé',
            'city'=>'Dijon',
        ]);
    }
}
