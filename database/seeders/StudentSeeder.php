<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Student;

class StudentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Student::create([
    
            'first_name'=>'Nessil',
            'last_name'=>'Abdou',
            'email_address'=>'NessAbdôve@gmail.com',
            'phone_number'=>'0643987485',
            'street_address'=>'74 route de Duti',
            'postal_code'=>'01458',
            'city'=>'Chenôve',
            'level'=>'DUT GEA',
            'rules' => "1",
            'school_id' => '2',
    
        ]);
        Student::create([
    
            'first_name'=>'Lisa',
            'last_name'=>'Franckot',
            'email_address'=>'LisaFranck@gmail.com',
            'phone_number'=>'0642387485',
            'street_address'=>'36 route de Bolozard',
            'postal_code'=>'01458',
            'city'=>'Lyon',
            'level'=>'Bachelor Techa',
            'rules' => "1",
            'school_id' => '10',
    
        ]);
        Student::create([
    
            'first_name'=>'Eloudy',
            'last_name'=>'Mazda',
            'email_address'=>'Elodanck@gmail.com',
            'phone_number'=>'0642387485',
            'street_address'=>'25 route de sing',
            'postal_code'=>'01458',
            'city'=>'Crottet',
            'level'=>'Bachelor du Vin',
            'rules' => "1",
            'school_id' => '2', 
    
        ]);
         Student::create([
    
            'first_name'=>'Nathan',
            'last_name'=>'Proutage',
            'email_address'=>'Nathuok@gmail.com',
            'phone_number'=>'0642387485',
            'street_address'=>'78 route de beton',
            'postal_code'=>'01458',
            'city'=>'Macon',
            'level'=>'Master WebTech',
            'rules' => "1",
            'school_id' => '5',
    
        ]);
    }
}
