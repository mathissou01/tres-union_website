<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subject;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subject::create([
            'name'=>'Mathématiques',
        ]);
        Subject::create([
            'name'=>'Français',
        ]);
        Subject::create([
            'name'=>'Physique-Chimie',
        ]);
        Subject::create([
            'name'=>'Anglais',
        ]);
        Subject::create([
            'name'=>'SVT',
        ]);
        Subject::create([
            'name'=>'Espagnol',
        ]);
        Subject::create([
            'name'=>'Allemand',
        ]);
        Subject::create([
            'name'=>'Histoire-Géographie',
        ]);
        Subject::create([
            'name'=>'Informatique',
        ]);
    }
}
