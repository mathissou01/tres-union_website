<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = config('roles.models.role')::where('name', '=', 'Admin')->first();
        if (config('roles.models.defaultUser')::where('email', '=', 'administrateur@tresunion.fr')->first() === null) {
            $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Administrateur',
                'email'    => 'administrateur@tresunion.fr',
                'password' => bcrypt('assoDcQ!vaKw'),
            ]);
            $newUser->attachRole($adminRole);
             $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Secrétaire',
                'email'    => 'secretariat@tresunion.fr',
                'password' => bcrypt('assoA!sFgts'),
            ]);
            $newUser->attachRole($adminRole);
             $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Trésorier',
                'email'    => 'tresorerie@tresunion.fr',
                'password' => bcrypt('assoxX&AcBc'),
            ]);
            $newUser->attachRole($adminRole);
             $newUser->attachRole($adminRole);
             $newUser = config('roles.models.defaultUser')::create([
                'name'     => 'Service-civique',
                'email'    => 'service-civique@tresunion.fr',
                'password' => bcrypt('assoZs8&zUD'),
            ]);
            $newUser->attachRole($adminRole);
        }
            
    }
}
