function DOM_is_ready_to_joke(e) {
    "loading" != document.readyState
        ? e()
        : document.addEventListener
        ? document.addEventListener("DOMContentLoaded", e)
        : document.attachEvent("onreadystatechange", function () {
              "complete" == document.readyState && e();
          });
}
function push_blablagues(l) {
    blablagues = document.querySelectorAll("div.blablagues");
    var n = Array();
    Object.keys(blablagues).forEach(function (e) {
        var t,
            a = blablagues[e];
        n.push(a.dataset),
            (void 0 !== l && l != e) ||
                ((a.innerText = ""),
                (t = a),
                [
                    "is_load",
                    "justify",
                    "center",
                    "blague",
                    "video",
                    "pepite",
                    "image",
                ].forEach(function (e) {
                    var a = new RegExp("(\\s|^)" + e + "(\\s|$)");
                    t.className = t.className.replace(a, " ");
                }));
    });
    var e = new XMLHttpRequest();
    function s(e, a, t, n, d, i) {
        var r = document.createElement(e);
        return (
            (r.innerHTML = i ? a : a.replace(/\r\n|\r|\n/g, "<br/>")),
            (r.className = t),
            (r.href = n),
            (r.src = d),
            r.href && r.setAttribute("target", "_blank"),
            "iframe" == e &&
                (r.setAttribute(
                    "allow",
                    "accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                ),
                r.setAttribute("allowfullscreen", ""),
                r.setAttribute("frameborder", "0")),
            r
        );
    }
    function a() {
        Object.keys(blablagues).forEach(function (e) {
            blablagues[e].appendChild(
                s(
                    "div",
                    "C'est ballot... la connexion vers blablagues.net a dÃ©connÃ© !",
                    "error"
                )
            );
        });
    }
    e.open("POST", "https://api.blablagues.net", !0),
        e.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"),
        (e.timeout = 1e4),
        e.send("para=" + JSON.stringify(n)),
        (e.onreadystatechange = function () {
            4 != e.readyState ||
                (200 != e.status && 0 != e.status) ||
                ((xhr_data = JSON.parse(e.responseText)),
                Object.keys(blablagues).forEach(function (e) {
                    var a, t, n, d, i, r;
                    (void 0 !== l && l != e) ||
                        ((a = blablagues[e]),
                        (data = xhr_data[e].data)
                            ? ((t = xhr_data[e].meta),
                              (n = data.content),
                              (text_head = n.text_head) &&
                                  a.appendChild(
                                      s("div", text_head, "text_head")
                                  ),
                              (text = n.text) &&
                                  a.appendChild(
                                      s(
                                          "div",
                                          text,
                                          "text",
                                          "",
                                          "",
                                          "pepite" == data.type ? 1 : ""
                                      )
                                  ),
                              (text_hidden = n.text_hidden) &&
                                  a.appendChild(
                                      s("div", text_hidden, "text hide")
                                  ),
                              (n.media || n.embed) &&
                                  ((d = s("div", "test", "media")),
                                  n.media &&
                                      ((i = s("img", "", "", "", n.media)),
                                      a.dataset.max_width &&
                                          (i.style.maxWidth =
                                              a.dataset.max_width + "px"),
                                      a.dataset.max_height &&
                                          (i.style.maxHeight =
                                              a.dataset.max_height + "px"),
                                      d.appendChild(i)),
                                  n.embed &&
                                      d.appendChild(
                                          s("iframe", "", "", "", n.embed)
                                      ),
                                  a.appendChild(d)),
                              (r = s("div", "", "testcopy")).appendChild(
                                  s("a", data.categorie, "", data.link)
                              ),
                              (r.innerHTML = r.innerHTML + " - "),
                              r.appendChild(s("a", t.site, "", t.link)),
                              a.appendChild(r),
                              (a.className +=
                                  " " +
                                  data.type +
                                  " is_load " +
                                  n.recommend_align),
                              (warning = xhr_data[e].warning) &&
                                  console.log("" + warning))
                            : console.error("" + xhr_data[e].error.message));
                }));
        }),
        (e.ontimeout = function () {
            a();
        }),
        (e.onerror = function () {
            a();
        });
}
DOM_is_ready_to_joke(function () {
    push_blablagues(),
        (check = ".blablagues .hide"),
        document.addEventListener(
            "click",
            function (e) {
                var a;
                e.target.matches(check + ", " + check + " *") &&
                    ((el = e.target.closest(check)),
                    (a = new RegExp("(\\s|^)hide(\\s|$)")),
                    (el.className = el.className.replace(a, " ")));
            },
            !1
        );
});
