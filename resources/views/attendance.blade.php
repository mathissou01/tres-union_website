<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<style>
  table {
  table-layout: fixed;
  }
  #page{
    page-break-inside: avoid !important;
  }
  table th:nth-child(1) { width:3cm; word-wrap:break-word;}
  table th:nth-child(2) { width:3cm; word-wrap:break-word;}
  table th:nth-child(3) { width:3cm; word-wrap:break-word;}
  table th:nth-child(4) { width:4cm; word-wrap:break-word;}
  table th:nth-child(5) { width:4cm; word-wrap:break-word;}
  table th:nth-child(5) { width:1cm; word-wrap:break-word;}
  table td:nth-child(1) { width:3cm; word-wrap:break-word;}
  table td:nth-child(2) { width:3cm; word-wrap:break-word;}
  table td:nth-child(3) { width:3cm; word-wrap:break-word;}
  table td:nth-child(4) { width:4cm; word-wrap:break-word;}
  table td:nth-child(5) { width:4cm; word-wrap:break-word;}
  table td:nth-child(5) { width:1cm; word-wrap:break-word;}
</style>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <div id='page' class="py-6" style="width:18cm">
      <div class="text-center" style="font-weight: bold; font-size: 17px; margin: 3px 0 3px 0;">Stage du {{date('d/m/Y', strtotime($lesson->courseSession->starting))}} au {{date('d/m/Y', strtotime($lesson->courseSession->endding))}}</div>
      <div class="text-center" style="font-weight: bold; font-size: 17px; margin: 3px 0 3px 0;">Cours de {{ $lesson->subject->name }} - {{ $lesson->level->name }} de {{date('H:i', strtotime($lesson->starting))}} à {{date('H:i', strtotime($lesson->endding))}}</div>
      <div class="text-center" style="font-weight: bold; font-size: 17px; margin: 3px 0 3px 0;">Encadré par {{ $lesson->contributor->firstname }} {{ $lesson->contributor->lastname }}</div>
      
      <table class="table table-bordered" style="page-break-after: always;">
        <thead>
          <tr class="divide-x divide-gray-400">
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase" >
              Nom
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase" >
              Prénom
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase" >
              Établissement d'origine
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase" >
              Responsable Légal
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase" >
              Contacts Responsable Légal
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Absent
            </th>
          </tr>
        </thead>
        <tbody>
          @foreach ($lesson->students as $student)
          <tr>
            <td class="px-2 py-1 " >
                    {{ $student->student_lastname }}
            </td>
            <td class="px-2 py-1 ">
                    {{ $student->student_firstname }} 
            </td>
            <td class="px-2 py-1 " >
              {{ $student->school }}
            </td>
            <td class="px-2 py-1 " >
                    {{ $student->legal_lastname }} {{ $student->legal_firstname }} 
            </td>
            <td class="px-2 py-1" >
                {{ $student->legal_phone }}
            </td>
            <td>
            </td>
          </tr>
          @endforeach
          <!-- More rows... -->
        </tbody>
      </table>
    </div>
