
<x-app-layout>
    <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    {{-- Script qui screen le code bar --}}
         <script type="text/javascript">
            function downloadimage() {
                /*var container = document.getElementById("image-wrap");*/ /*specific element on page*/
                var container = document.getElementById("cardToDownload");; /* full page */
                html2canvas(container, { scale:2, allowTaint: true, backgroundColor: '#fff' }).then(function (canvas) {

                    var link = document.createElement("a");
                    document.body.appendChild(link);
                    link.download = "CodeBar_de_{{ $minorstudent->first_name }}_{{ $minorstudent->last_name }}.png";
                    link.href = canvas.toDataURL();
                    link.target = '_blank';
                    link.click();
                });
            }
          </script>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('student') }}" class="text-blue-700">Enfants </a>/ Code bar de<i> ({{ $minorstudent->first_name }} {{ $minorstudent->last_name }})</i>
    </h2>
  </x-slot>
   <button onclick="downloadimage()" class="clickbtn">Télécharger le code bar &nbsp<i class="fas fa-download"></i></button>
  <div id="htmltoimage">  
    <div class="py-12" id="cardToDownload">
      <img src="{{ asset('/public/storage/images/logo.svg') }}" class="img-backgrund top left">
      <img src="{{ asset('/public/storage/images/logo.svg') }}" class="img-backgrund top right">
      <img src="{{ asset('/public/storage/images/logo.svg') }}" class="img-backgrund bottom left">
      <img src="{{ asset('/public/storage/images/logo.svg') }}" class="img-backgrund bottom right">
        <div class="col"> 
          <h2 class="prenom">{{ $minorstudent->first_name }} <span class="nom">{{ $minorstudent->last_name }} </span></h2>
           <div class="codebar">    
             <?php echo DNS1D::getBarcodeHTML($minorstudent->uuid, 'EAN13',6,66);?>
           </div>
        </div>
    </div>
  </div>
</x-app-layout>
<style>
  @import url('https://fonts.googleapis.com/css2?family=Dancing+Script&display=swap');
*{
  box-sizing: border-box;
}

  main{
    height: 100%;
  }

    .codebar {
      display: flex;
      justify-content: center;
      min-width: 500px;
      overflow: hidden;
      position: relative;
      padding: 20px;
    }

    .img-backgrund{
      position: absolute;
      height: 20%;
    }

    .top {
      top: 0px;
    }

    .left {
      left: 0px;
    }

    .right {
      right: 0px;
    }

    .bottom {
      bottom: 0px;
    }


    .prenom{
      vertical-align: center;
      margin-bottom: 2rem;
      text-align: center;
      font-size: 48px;
      font-family: 'Dancing Script', cursive;
      font-weight: 600;
    }
    .nom{
      text-transform: uppercase;
    }

    #htmltoimage{
      display: flex;
      justify-content: space-around;
      align-content: center;
      height: 100%;
      background-color: transparent;
      flex-flow: row nowrap:
    }

    .py-12{
      background-color: transparent;
      position: relative;
      border: solid 1px black;
      border-radius:10px;
    }
    .clickbtn{
      position: relative;
      left: 50%;
      transform: translateX(calc(-50% - 30px));
      box-sizing: border-box;
      margin: 60px 30px;
      align-items: center;
      appearance: none;
      background-color: #fff;
      border-radius: 24px;
      border-style: none;
      box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px,rgba(0, 0, 0, .14) 0 6px 10px 0,rgba(0, 0, 0, .12) 0 1px 18px 0;
      box-sizing: border-box;
      color: #3c4043;
      cursor: pointer;
      display: inline-flex;
      fill: currentcolor;
      font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
      font-weight: 500;
      height: 48px;
      justify-content: center;
      letter-spacing: .25px;
      line-height: normal;
      max-width: 100%;
      overflow: visible;
      padding: 2px 24px;
      position: relative;
      text-align: center;
      text-transform: none;
      font-size: 25px;
      color: rgb(0, 81, 255);
      transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1),opacity 15ms linear 30ms,transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
      width: auto;
      will-change: transform,opacity;
      z-index: 0;
    }

.clickbtn:hover {
  background: #00363d18;
  color: #174ea6;
}

.clickbtn:active {
  box-shadow: 0 4px 4px 0 rgb(60 64 67 / 30%), 0 8px 12px 6px rgb(60 64 67 / 15%);
  outline: none;
}

.clickbtn:focus {#4f92ff
  outline: none;
  border: 2px solid #4285f4;
}

.clickbtn:not(:disabled) {
  box-shadow: rgba(51, 66, 78, 0.3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
}

.clickbtn:not(:disabled):hover {
  box-shadow: rgba(60, 64, 67, .3) 0 2px 3px 0, rgba(60, 64, 67, .15) 0 6px 10px 4px;
}

.clickbtn:not(:disabled):focus {
  box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
}

.clickbtn:not(:disabled):active {
  box-shadow: rgba(60, 64, 67, .3) 0 4px 4px 0, rgba(60, 64, 67, .15) 0 8px 12px 6px;
}

.clickbtn:disabled {
  box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
}    
    }
</style>
  


  
  



 

  