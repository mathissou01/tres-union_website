<x-app-layout>
    <x-slot name="header">
        <x-jet-dropdown>
          <x-slot name="trigger">
            <button class="flex items-center hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
              <img src="https://img.icons8.com/external-justicon-lineal-color-justicon/40/000000/external-teacher-elearning-and-education-justicon-lineal-color-justicon.png"/>
                      <style>
                        img {
                          margin-right: 12px;
                            position: sticky  ;
                            margin-bottom: 5px;
                        }
                      </style>
              <div>Intervenants</div>
              <div class="ml-1">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                </svg>
              </div>
            </button>
          </x-slot>
          <x-slot name="content">
              <x-jet-dropdown-link href="{{ route('newcontributor') }}" class="bg-gray-100">
                  Ajouter un nouvel intervenant
              </x-jet-dropdown-link>
              <div class="border-t border-gray-100 "></div>
          </x-slot>
        </x-jet-dropdown>
    </x-slot>
    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
          <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Nom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Email
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Téléphone
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Contrat
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                        <th scope="col" class="px-6 py-3 bg-blue-50">
                          <span class="sr-only">Edit</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($contributors as $contributor)
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                          <div class="flex items-center">
                            <div class="ml-4">
                              <div class="text-sm font-medium text-gray-900">
                                {{ $contributor->firstname }} {{ $contributor->lastname }}
                              </div>
                            </div>
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                          <div class="text-dark text-sm hover:text-indigo-900 mr-2">
                            <a href="mailto:{{ $contributor->email }}">{{ $contributor->email }}</a>
                          </div>
                      </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {{ $contributor->phone }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                           @if($contributor->contract === "Professeur")
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-200 text-dark-900">
                              {{ $contributor->contract }}
                               </span>
                               @elseif($contributor->contract === 'Intervenant')
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-blue-200 text-dark-900">
                                {{ $contributor->contract }}
                               </span>                                                     
                            @else($contributor->contract === "Service civique" )
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-orange-200 text-dark-900">
                              {{ $contributor->contract }}
                               </span>                            
                            @endif                        
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium bg-white-50">                      
                          <a href="{{ route('modcontributor', ['id'=>$contributor->id])}}" class="text-indigo-600 hover:text-indigo-900 mr-2">Modifier</a>
                            <button name="{{ $contributor->id }}" class="text-red-600 hover:text-red-900 delete-button" data-toggle="modal" data-target="#confirmDeleteModal">Supprimer</button>
                              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium bg-blue-50">
                               <a href="{{ route('showcontributor', ['id'=>$contributor->id])}}" class="text-indigo-600 hover:text-indigo-900">Voir</a>
                              </td>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
 <!-- Modal pour confirmer la suppression -->
    <div class="modal fade" id="confirmDeleteModal" data-selected-subject="0" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header bg-red-600 ">
            <h5 class="modal-title text-light" id="confirmDeleteModalTitle">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Êtes-vous sur de vouloir supprimer cette intervenant ? </br>
            Vous supprimerez toutes ses informations !
          </div>   
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="confirmDelete" type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">Supprimer</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        $(".delete-button").click(function(){
            $("#confirmDeleteModal").attr("data-selected-subject",this.name)
        })
        $("#confirmDelete").click(function(){
            $(location).attr("href", "/contributor/del/"+$("#confirmDeleteModal").attr('data-selected-subject'));
        })
    </script>       
</x-app-layout>
