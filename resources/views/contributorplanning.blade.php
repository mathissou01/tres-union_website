<x-guest-layout>
<!-- CSS only -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<!-- JavaScript Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  {{--Script pour télécharger l'edt en image--}}
  <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
         <script type="text/javascript">

            function downloadimage() {
                /*var container = document.getElementById("image-wrap");*/ /*specific element on page*/     

                var documentPadding = document.querySelectorAll(".htcMB");
                var documentMatinPadding = document.querySelectorAll(".htcMB2");
                for (let element = 0; element < documentPadding.length; element++) {
                  var simple = documentPadding[element];
                  simple.classList.toggle("pb-4");
                }
                for (let element = 0; element < documentMatinPadding.length; element++) {
                  var loc = documentMatinPadding[element];
                  loc.classList.toggle("pb-2");
                }

                var container = document.getElementById("edt");; /* full page */
                html2canvas(container, { scale:2, allowTaint: true, backgroundColor: '#fff',logging : true, onclone: null,removeContainer : true,useCORS: true,}).then(function (canvas) {

                    var link = document.createElement("a");
                    document.body.appendChild(link);
                    link.download = "Planning_de_{{$contributor->firstname}}_{{$contributor->lastname}}_stage_du_{{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}}";
                    link.href = canvas.toDataURL();
                    link.target = '_blank';
                    link.click();
                });

                for (let element = 0; element < documentPadding.length; element++) {
                  var simple = documentPadding[element];
                  simple.classList.toggle("pb-4");
                }
                for (let element = 0; element < documentMatinPadding.length; element++) {
                  var loc = documentMatinPadding[element];
                  loc.classList.toggle("pb-2");
                }
            } 
        </script>
 

  <div class="py-12">
    <div class="justify-center d-flex pb-3">
      {{--Input de recherche pour edt personnalisé--}}
     <input type="hidden" id="myInput" placeholder="Chercher un intervenant, une classe, une salle..." title="Pour pouvoir inprimer des planning perso, faite une recherche" value="{{$contributor->firstname}} {{$contributor->lastname}}"><br/>
     <style>
       input{
          background-image: url('https://i.postimg.cc/L4j0hVxQ/chercher.png');
  background-position: 8px 4px;
  background-repeat: no-repeat;
  background-size: 17px 17px;
  padding-left: 30px;
         width:200px;
          display: flex;
           border: 2px solid rgba(0, 1, 65, 0.486);
            border-radius: 4px;
          
             background-color: #0026ff1c;"
  transition: width 0.2s ease-in-out;
}
input[type=text]:focus {
  width: 400px;
  transition: width 0.4s ease-in-out;
 
}
       </style>
    </div>
   
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
       <div id="edt"> 
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        {{--Header pour afficher les jours de la semaine--}}
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Planning du stage <br>
            {{$courseSession->school->name}} {{$courseSession->school->city}}
          </h3>
          
        </div>
        {{--Ensemble qui affiche l'edt--}}
        
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-3">
          <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="container-fluid">
       
               
              <div class="row border border-right-0 border-bottom-0" id="edt1">
               
                @if (sizeof($dates)<7)
                  @foreach ($dates as $key=>$date)
                 
                  <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                    <h5 class="row align-items-center">
                      <span class="date col-1">{{$date->format('d/m')}}</span>
                      <small class="col d-sm-none text-center text-gray-900">{{$date->translatedFormat('l')}}</small>
                      <span class="col-1"></span>
                    </h5>
                    
                    {{--Chaque balise "a" affiche une matière et ses infos--}}
                    <div class="htcMB2" style="font-size: 18px;">Matin</div>
                    @foreach ($courseSession->lessons as $keylesson => $lesson)
                      @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->lt(Carbon::createFromFormat('H:i', '12:00')))
                      <a class="event d-block rounded text-sm text-white bg-primary htcMB p-2 mt-1" id="first" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">
                        <span class="text-lg text-wrap"> {{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} </span><br> 
                        <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br>
                        <span class="text-lg text-wrap">Salle {{ $lesson->room }}</span><br>
                        <span class="text-lg text-wrap">{{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}}</span>
                      </a>
                      @endif                        
                    @endforeach
                    <div class="htcMB2" style="font-size: 18px;">Après-Midi</div>
                    @foreach ($courseSession->lessons as $keylesson => $lesson)
                      @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->gte(Carbon::createFromFormat('H:i', '12:00')))
                      <a class="event d-block htcMB pl-2 pr-2 mt-1 rounded text-sm bg-primary text-white p-2" id="first" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">
                        {{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> 
                        <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br>
                        <span class="text-lg text-wrap">Salle {{ $lesson->room }}</span><br>
                        <span class="text-lg text-wrap">{{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}}</span></a>
                      @endif                        
                    @endforeach
                  </div>
                  @if (($key+1)%7==0)
                    <div class="w-100"></div>
                  @endif
                  @endforeach
                @else
                  @for ($i = 0; $i < sizeof($dates)+(7-sizeof($dates)%7); $i++)
                    @if ($i<sizeof($dates))
                    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                      <h5 class="row align-items-center">
                        <span class="date col-1">{{$dates[$i]->format('d/m')}}</span>
                        <small class="col d-sm-none text-center  text-gray-900">{{$dates[$i]->translatedFormat('l')}}</small>
                        <span class="col-1"></span>
                      </h5>
                    
                      <div class="htcMB2">Matin</div>
                      @foreach ($courseSession->lessons as $keylesson => $lesson)
                        @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($dates[$i]) && Carbon::createFromFormat('H:i:s',$lesson->starting)->lt(Carbon::createFromFormat('H:i', '12:00')))
                        <a class="event d-block htcMB pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white p-2" id="first" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br><span class="text-lg text-wrap">Salle {{ $lesson->room }}</span><br><span class="text-lg text-wrap">{{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}}</span></a>
                        @endif                             
                         @endforeach
                      <div class="htcMB2">Après-Midi</div>
                      @foreach ($courseSession->lessons as $keylesson => $lesson)
                        @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($dates[$i]) && Carbon::createFromFormat('H:i:s',$lesson->starting)->gte(Carbon::createFromFormat('H:i', '12:00')))
                        <a class="event d-block htcMB pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white p-2" id="first" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br><span class="text-lg text-wrap">Salle {{ $lesson->room }}</span><br><span class="text-lg text-wrap">{{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}}</span></a>
                        @endif                                  
                       @endforeach
              </div>
            
                    @else
                    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-light text-muted">
                    </div>   
                    @endif
                    @if (($i+1)%7==0)
                    <div class="w-100"></div>
                    @endif 
                  @endfor
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
        
      </div>
         <div class="justify-center d-flex pb-1">
            <!---Bouton qui es relier au premier script---->
            <button type="button" onclick="downloadimage()" id="boo" class="inline-flex items-center mb-3 mt-3 px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-dark">
              Télécharger le planning
            </button>
             </div>
            <style>
              #boo {
  --border: 5px;    /* the border width */
  --slant: 0.7em;   /* control the slanted corners */
  --color: #3765fc6c; /* the color */
  
  font-size: 20px;
  padding: 0.4em 1.2em;
  border: 1px solid #3765fc6c !important;
  cursor: pointer;
  font-weight: 100;
  color: black;
  background: 
     linear-gradient(to bottom left,var(--color)  50%,#0000 50.1%) top right,
     linear-gradient(to top   right,var(--color)  50%,#0000 50.1%) bottom left;
  background-size: calc(var(--slant) + 1.3*var(--border)) calc(var(--slant) + 1.3*var(--border));
  background-repeat: no-repeat;
  box-shadow:
    1 0 0 200px inset var(--s,#0000),
    1 0 0 var(--border) inset var(--color);
 
  transition: color var(--t,0.3s), background-size 0.3s;
}
#boo:hover,
#boo:active{
  background-size: 100% 100%;
  color: #fff!important;
  --t: 0.2s 0.1s;
}
#boo:focus-visible {
  outline-offset: calc(-1*var(--border));
  outline: var(--border) solid #000a;
}
#boo:active {
  --s: #0005;
  transition: none;
  color: #fff!important;
}           
            
            </style>
        
         {{--Script relier a l'input de recherche. Il recupere les "a" avec l'id "first" et filtre avec la recherche.Il enelève a visibilité des résultat qui 
         ne correspond pas à la recherche--}}
      <script>
        var input = document.getElementById("myInput");

        input.addEventListener("input", myFunction); 
        
        window.addEventListener('load', function() {
          console.log('All assets are loaded');
          const e = {target: {value: "{{$contributor->firstname}} {{$contributor->lastname}}"}};
          myFunction(e);
        })

        function myFunction(e) {
          var filter = e.target.value.toUpperCase();

          var RegEx;

          if (filter == ""){
            RegEx = /(.*)/;
          }else{
            RegEx = new RegExp('\\s*' + filter + "\\s*");
          }

          var divs = $("#edt a");
          for (var i = 0; i < divs.length; i++) {
            var a = divs[i];
            if (a) {
              if (a.innerHTML.toUpperCase().match(RegEx) != null) {
                a.classList.add("d-block");
                a.classList.remove("d-none");
              } else {
                a.classList.add("d-none");
                a.classList.remove("d-block");
              }
            }       
          }

        }
      </script>
    </div>
  </div>
{{--Modal qui affiche les infos d'un cours lorsque l'on clique dessus--}}
@foreach ($courseSession->lessons as $key => $lesson)
<div class="modal fade modal_class_id" id="infoModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
  
      <div  class="modal-body">
        <dl>
          <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
            <dt class="text-sm font-medium text-gray-500">
              Matière
            </dt>
            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
              {{$lesson->subject->name}}
            </dd>
          </div>
          <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
            <dt class="text-sm font-medium text-gray-500">
              Horaire
            </dt>
            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
              {{$lesson->starting}}-{{$lesson->endding}}
            </dd>
          </div>
          <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
            <dt class="text-sm font-medium text-gray-500">
              Intervenant
            </dt>
            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
              {{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}}
            </dd>
          </div>
          <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
            <dt class="text-sm font-medium text-gray-500">
              Niveau
            </dt>
            <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
              {{$lesson->level->name}}
            </dd>
          </div>
        </dl>
      </div>
  
    </div>
  </div>
</div>
@endforeach
</x-guest-layout>

