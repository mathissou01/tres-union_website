<x-app-layout>
    <x-slot name="header">
        <x-jet-dropdown>
          <x-slot name="trigger">
            <button class="flex items-center hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
              <img src="https://img.icons8.com/external-others-phat-plus/40/000000/external-entertainment-studio-color-line-others-phat-plus-5.png"/>
                      <style>
                      img {
                        margin-right: 13px;
                          position: sticky  ;
                          margin-bottom: 5px;
                      }
                      </style>
              <div>Stages</div>
              <div class="ml-1">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                </svg>
              </div>
            </button>
          </x-slot>
          <x-slot name="content">
              <x-jet-dropdown-link href="{{ route('newsession') }}" class="bg-gray-100">
                  Ajouter un stage
              </x-jet-dropdown-link>
              <div class="border-t border-gray-100 "></div>
          </x-slot>
        </x-jet-dropdown>
    </x-slot>
    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
          <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Date Début
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Date Fin
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Nombre d'inscrit
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          État
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                      @foreach ($courseSessions as $courseSession)
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                          <div class="flex items-center">
                            <div class="ml-4">
                              <div class="text-sm font-medium text-gray-900">
                                {{date('d/m/Y', strtotime($courseSession->starting))}}
                              </div>
                            </div>
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                          <div class="text-sm text-gray-900">
                            {{date('d/m/Y', strtotime($courseSession->endding))}}  
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                        @if ($courseSession->lessons()->count()*$courseSession->places != 0 && $courseSession->students()->count() == $courseSession->lessons()->count()*$courseSession->places)
                        <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-200 text-green-800">
                          {{ $courseSession->students()->count() }} - Complet
                        </span>
                        @else
                        <span class="px-2 inline-flex text-xs leading-5 font-semibold text-gray-800">
                          {{ $courseSession->students()->count() }}
                        </span>
                        @endif
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                          @if (Carbon::createFromFormat('Y-m-d',$courseSession->endding)->gt(Carbon::createFromFormat('Y-m-d',date('Y-m-d'))))
                              @if ($courseSession->published)
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-200 text-green-800">
                                  Publié
                                </span>
                              @else
                                <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-yellow-200 text-red-800">
                                  Brouillon
                                </span>
                              @endif
                          @else
                            <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-gray-200 text-gray-800">
                              Terminé
                            </span>
                          @endif  
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-xl font-medium">
                          <a href="{{ route('showmailers',['id'=>$courseSession->id]) }}" class="text-indigo-600 hover:text-indigo-900">
                            <i class="fa fa-envelope" aria-hidden="true"></i></a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <a href="{{ route('showsession',['id'=>$courseSession->id]) }}" class="text-indigo-600 hover:text-indigo-900">Voir les informations</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <a href="{{ route('showplanning',['id'=>$courseSession->id]) }}" class="text-indigo-600 hover:text-indigo-900">Voir le planning</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <a href="{{ route('ficheStage',['id'=>$courseSession->id]) }}" class="text-indigo-600 hover:text-indigo-900" >Fiche horaire stage</a>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">            
                          <a href="{{ route('attendance',['id'=>$courseSession->id]) }}" class="text-indigo-600 hover:text-indigo-900">Appel</a>
                        </td>

                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          @if (Carbon::createFromFormat('Y-m-d',$courseSession->endding)->gt(Carbon::createFromFormat('Y-m-d',date('Y-m-d'))))
                              @if ($courseSession->published)
                                <form action="{{ route('haltsession',['id'=>$courseSession->id]) }}" method="post">
                                  @csrf
                                  <button type="submit" class="btn btn-warning">Suspendre</button></a>
                                </form>
                              @else
                                <form action="{{ route('publishsession',['id'=>$courseSession->id]) }}" method="post">
                                  @csrf
                                  <button type="submit" class="btn btn-primary">Publier</button></a>
                                </form>
                              @endif
                          @else
                            @if ($courseSession->published)
                              <button type="submit" class="btn btn-secondary" disabled>Suspendre</button></a>
                            @else
                              <button type="submit" class="btn btn-secondary" disabled>Publier</button></a>
                            @endif
                          @endif  
 
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        </div>
    </div>
</x-app-layout>
