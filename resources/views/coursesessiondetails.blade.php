<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('session') }}" class="text-blue-700">Stage </a>/ Stage du {{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}}
    </h2>
  </x-slot>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Détails du stage
          </h3>
          <span class="sm:ml-3">
            <a href="{{ route('modifysession', ['id'=>$courseSession->id])}}">
              <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 @if ($finished)
                bg-gray-400
              @else               
                bg-indigo-600 hover:bg-indigo-700
              @endif" 
              @if ($finished)
                disabled                 
              @endif>
                Modifier
              </button>
            </a>
          </span>
        </div>
        <div class="border-t border-gray-200">
          <dl>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Dates
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{date('d/m/Y', strtotime($courseSession->starting))}} - {{date('d/m/Y', strtotime($courseSession->endding))}}
              </dd>
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Établissement
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{$courseSession->school->name}},{{$courseSession->school->city}}
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Nombre d'inscrits <br>
                  <a href="{{ route('sessionlist', ['id'=>$courseSession->id])}}">
                    <button type="button" class="mt-3 inline-flex items-center px-4 py-2 bg-indigo-600 hover:bg-indigo-700 border border-transparent rounded-md shadow-sm text-sm font-medium text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" >
                      Voir la liste
                    </button>
                  </a>
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $registred }}
              </dd>
            </div>
            <div class="bg-white-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Nombre de places par groupe
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{$courseSession->places}}
              </dd>
            </div>
            
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm text-gray-900 fw-bold">
               Lien du formulaire d'inscription
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                 {{--Bouton et span relié au script de copy--}}
                <span id="tocopy"> {{ route("sessionregister",['id'=>$courseSession->id]) }} </span>  
                  <input type="button" value="Copier le lien" class="btn btn-outline-success ml-5 " id="js-copy" data-target="#tocopy">
              </dd>
            </div>
             <script type="text/javascript">
              var btncopy = document.querySelector('#js-copy');
              if(btncopy) {
                  btncopy.addEventListener('click', docopy);
              }

              function docopy() {

                  // Cible de l'élément qui doit être copié
                  var target = this.dataset.target;
                  var fromElement = document.querySelector(target);
                  if(!fromElement) return;

                  // Sélection des caractères concernés
                  var range = document.createRange();
                  var selection = window.getSelection();
                  range.selectNode(fromElement);
                  selection.removeAllRanges();
                  selection.addRange(range);

                  try {
                      // Exécution de la commande de copie
                      var result = document.execCommand('copy');
                      if (result) {
                          // La copie a réussi
                          alert('Le lien a été copié !');
                      }
                  }
                  catch(err) {
                      // Une erreur est surevnue lors de la tentative de copie
                      alert(err);
                  }

                  // Fin de l'opération
                  selection = window.getSelection();
                  if (typeof selection.removeRange === 'function') {
                      selection.removeRange(range);
                  } else if (typeof selection.removeAllRanges === 'function') {
                      selection.removeAllRanges();
                  }
              }   
            </script>
          </dl>
        </div>
      </div>
    </div>
  </div>
  <div class="modal fade" id="confirmDeleteModal" data-selected-subject="0" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header bg-red-600 ">
            <h5 class="modal-title text-light" id="confirmDeleteModalTitle">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center">
            Vous êtes sur le point d'envoyer un mail à tous les intervenants se trouvant sur le planning du stage à </br>
            {{$courseSession->school->name}}, {{$courseSession->school->city}} se déroulant le {{date('d/m/Y', strtotime($courseSession->starting))}} - {{date('d/m/Y', strtotime($courseSession->endding))}}.</br>
           Il recevront l'emplacement et les dates du stage ainsi qu'un lien leur permettant d'affichant et de télécharger leur emploi du temps
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              <a href="{{ route("emailtocontributors",['id'=>$courseSession->id ])}}"> 
            <button type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-lm font-medium rounded-md text-white bg-green-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">Valider</button>
              </a>
          </div>
        </div>
      </div>
    </div>
</x-app-layout>
