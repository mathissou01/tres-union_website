<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('session') }}" class="text-blue-700">Stage </a>/ Stage du {{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}}
    </h2>
  </x-slot>
  <div class="py-12">
      <form method="GET" action="{{ route("emailtocontributors",['id'=>$courseSession->id ])}}">
                   @csrf
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Mailer du stage 
          </h3>
        </div>
        <div class="border-t border-gray-200">
          <dl>
             <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">    
              <dt class="text-lm font-medium text-gray-500">
                Action 1
              </dt>
              {{-- Mailer intervenant --}}
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">    
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#confirmDeleteModal">Envoyer le planning aux intervenants</button>
              </dd>
                <label class="text-lm font-medium text-gray-500">Inclure un message supplémentaire</label>
                  <div class="form-outline">
           
                    <textarea class="form-control" id="message" name="message" placeholder="Votre message" type="text"></textarea>
                  </div>         
            </div>
          </dl>
        </div>
        <div class="modal fade" id="confirmDeleteModal" data-selected-subject="0" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header bg-red-600 ">
            <h5 class="modal-title text-light" id="confirmDeleteModalTitle">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        
          </div>
          <div class="modal-body text-center">
            Vous êtes sur le point d'envoyer un mail à tous les intervenants se trouvant sur le planning du stage à </br>
            <b>{{$courseSession->school->name}}, {{$courseSession->school->city}}</b> se déroulant le 
            <b>{{date('d/m/Y', strtotime($courseSession->starting))}} - {{date('d/m/Y', strtotime($courseSession->endding))}}</b>.</br>
           Ils recevront l'emplacement et les dates du stage ainsi qu'un lien leur permettant d'afficher et de télécharger leur emploi du temps
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              <a href="{{ route("emailtocontributors",['id'=>$courseSession->id ])}}"> 
            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-lm font-medium rounded-md text-white bg-green-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">Valider</button>
              </a>
          </div>
        </div>
      </div>
    </div>
      </form>
         <form method="GET" action="{{ route("emailtocontributorsperso",['id'=>$courseSession->id ])}}">
          
            @csrf
          <div class="border-t border-gray-200">
            <dl>
              <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-lm font-medium text-gray-500">
                  Action 2
                </dt>
                {{-- Mailer intervenant --}}
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">    
                  <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg">Envoyer le planning à un intervenant</button>
                  <select id="email" name="email" class="mt-1 block w-full sm:text-sm form-input rounded-md">
                                {{-- Boucler pour mettre tout les intervenants de la base de données dans le select --}}
                              @foreach ($courseSession->lessons as $key => $lesson)
                                  <option value={{$lesson->contributor->id}}><b>{{$lesson->contributor->id}}</b> - {{$lesson->contributor->lastname}},  {{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}} - {{$lesson->contributor->email}}</option>
                                @endforeach
                            </select>
                    <div class="form-outline">
                        <label class="text-lm font-medium text-gray-500">Inclure un message supplémentaire</label>
                
                    </div> 
                       <textarea class="form-control" id="message_perso" name="message_perso" placeholder="Votre message" type="text"></textarea>  
                </dd>
              </div>
            </dl>
          </div>
        
      </div>
    </div>
  </div>
  <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body text-center">
            Vous êtes sur le point d'envoyer un mail à l'intervenant que vous avez selectionné du stage à </br>
            <b>{{$courseSession->school->name}}, {{$courseSession->school->city}}</b> se déroulant le 
            <b>{{date('d/m/Y', strtotime($courseSession->starting))}} - {{date('d/m/Y', strtotime($courseSession->endding))}}</b>.</br>
           Il recevra l'emplacement et les dates du stage ainsi qu'un lien lui permettant d'afficher et de télécharger son emploi du temps
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              <a href="{{ route("emailtocontributorsperso",['id'=>$courseSession->id ])}}"> 
            <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-lm font-medium rounded-md text-white bg-green-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">Valider</button>
              </a>
          </div>
    </div>
  </div>
</div>
</form>
</x-app-layout>
