{{ Html::style('css/illustration.css') }}
{{ Html::style('https://api.blablagues.net/blablagues.min.css') }}
{{ HTML::script('js/blague.js') }}
<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-grey-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
<div class="py-12">
    <div class="mx-auto sm:px-6 lg:px-8">

    <div class="container_dashboard">
           {{-- Blague du jour sur le dashboard --}}
        <div class="letter">
            <span class="phrasedujour">Votre blague du jour :</span>
                <div id=QuoteOfDay>
                    <div class="blablagues" data-rub="blagues" data-day="10">
                    </div>
                </div>   
        </div>
            <?php
            $count=1;
            ?>
            <!-- Variable date de stage -->
            <!-- Ouverture de la div avec le contenu du text -->
        <div class="var_stage">
            <!-- Boucle pour demander toutes les variables car on demande CourSession:all -->
            @foreach ($courseSessions as $courseSession)
            @if (Carbon::now()<($courseSession->starting)) 
            <h2 class="nb_jours">Dans <b>{{ $diff = Carbon\Carbon::parse($courseSession->starting)->diffInDays(Carbon\Carbon::today()->tz('Europe/Paris')) }}</b> jours</h2>
            <h3 class="var_text">Stage {{$count}} :</h3>
            <!-- Text numero 1 qui affiche la date de début de stage -->
            <h4 class="varDébut">Date de début : {{$courseSession->starting}}</h4>
            <!-- Text numero 2 qui affiche la ville et le lycée -->
            <h5 class="varEtabl">Établissement : {{$courseSession->school->name }}, {{ $courseSession->school->city }} </h5>
            <br>
                <?php
                $count=$count+1;
                ?>
            @else
            <!-- Ne rien afficher -->
            @endif
            <!-- Fin de if -->
            @endforeach
            <!-- Fermeture de la boucle et de la /div -->
        </div>
<div class="compta_total">
        <div class="compta_etudiant">
            <h4 class="info_compta">Infos générales</h4>
            <div class="nb_student">
            <span><b>{{$minorstudent->count()}}</b> étudiants Pré-BAC</span>
             <span><b>{{$student->count()}}</b> étudiants Post-BAC</span>
             <span><b>{{$student->count() + $minorstudent->count()}}</b> étudiants au total</span>
             <hr>
            </div>
            <div class="nb_famille">
                <span><b>{{$familly->count()}}</b> familles</span>
                <hr>
            </div>
        <div class="nb_inter">
            <span><b>{{$intervenant}}</b> salariés</span>
            <span><b>{{$benevole}}</b> bénévoles</span>
            <span><b>{{$professeur}}</b> professeurs</span>
            <span><b>{{$stagiaire}}</b> stagiaires</span>
            <span><b>{{$service_civique}}</b> service civiques</span>   
            <span><b>{{$contributors->count()}}</b> intervenants au total</span>  
            <hr> 
        </div>
        </div>
        <div class="compta">
            <h4 class="info_compta">Infos financières</h4>
            <div class="compta_finance_student">
                <div class="attente">
                    <span class="attente"><b>{{$attente->count()}}</b> étudiants Post-BAC en paiement en attente</span>
                </div>
                 <div class="partiel">
                    <span><b>{{$partiel->count()}}</b> étudiants Post-BAC en paiement partiel</span>
                </div>
                <div class="payer"> 
                    <span class="payer"><b>{{$payer->count()}}</b> étudiants Post-BAC en paiement effectué</span>
                    <hr>
                </div>
                
            </div>
            <div class="compta_finance_minorstudent">
                <div class="attente">
                    <span class="attente"><b>{{$attente_min->count()}}</b> étudiants Pré-BAC paiement en attente</span>
                    
                </div>
                 <div class="partiel">
                    <span><b>{{$partiel_min->count()}}</b> étudiants Pré-BAC paiement partiel</span>
                    
                </div>
                <div class="payer"> 
                    <span class="payer"><b>{{$payer_min->count()}}</b> étudiants Pré-BAC paiement effectué</span>
                    <hr>
                </div>
                <div class="total_attente_payer">
                    <span class="attente"><b>{{$attente_min->count() + $attente->count()}}</b> étudiants au total en paiement attente</span>
                      <span class="partiel"><b>{{$partiel->count() + $partiel_min->count()}}</b> étudiants au total en paiement partiel</span>
                    <span class="payer"><b>{{$payer_min->count() + $payer->count()}}</b> étudiants au total en paiement effectué</span>
                    <hr>
                </div>
            </div>
        </div>
</div>
    </div>
    </div>
</div>
</x-app-layout> 

