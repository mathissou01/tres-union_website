@component('mail::message')
 Bonjour
   {{$firstname}} {{$lastname}},
      <br>
      Vous avez été inscrit au stage d'aide au devoirs de l'association Très Union à 
      <strong>{{ $courseSession->school->name }}, {{$courseSession->school->city}}</strong>
       <br>
      qui se déroule le <strong>{{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}}</strong> 
       <br>
       <i>{{$message}}</i>
@component('mail::panel', ['url' => '{{$url}}', 'color'=>'success'])
<a href="{{$url}}"><b>Voici votre planning pour le stage</b></a>
@endcomponent
 En vous souhaitant une bonne journée.<br>
      Cordialement,
     <strong>Très-Union</strong>
{{ config('app.name') }}
@endcomponent

