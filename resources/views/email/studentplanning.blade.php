<div class="text-center mb-3">
      Bonjour {{ $student->student_firstname }},<br>
      Voici votre planning pour le stage de soutien du {{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}},<br>
      le stage a lieu à l'Établissement : {{$courseSession->school->name}}.
      En vous souhaitant une bonne journée.<br>
      Cordialement,
     <strong>Très-Union</strong>
</div>

<a href="{{$url}}">Voir mon planning</a>
