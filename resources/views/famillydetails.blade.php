<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('familly') }}" class="text-blue-700">Famille </a>/ Information de la famille <i>({{ $familly->first_name }} {{ $familly->last_name }})</i>
    </h2>
  </x-slot>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between bg-gray-50">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Détails de la famille
          </h3>
          <span class="sm:ml-3">
            <a href="/familly/mod/{{ $familly->id }}">
              <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Modifier
              </button>
            </a>
          </span>
        </div>
        <div class="border-t border-gray-200">
          <dl>
            <div class="bg-white-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Nom, Prénom
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $familly->last_name }}   {{ $familly->first_name }}
              </dd>
            </div>        
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Email
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $familly->email_address }}
              </dd>
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Téléphone
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $familly->phone_number }}
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Adresse
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $familly->street_address }} </br> {{ $familly->postal_code }}    {{ $familly->city }}
              </dd>
            </div>           
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Règlement signé
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                @if ($familly->rules===1)
                     <span style="color:red;">Oui</span>
                @else
                    Non
                @endif
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Absence
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                @if ($familly->absence===1)
                     <span style="color:red;">Oui</span>
                @else
                    Non
                @endif
              </dd>
            </div>
          </div>
        </dl>
      </div>
    </div>
  </div>
</div>
</x-app-layout>
