<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('session') }}" class="text-blue-700">Stages </a>/ Fiche horaire stage
    </h2>
  </x-slot>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
    <label><h2>Déclaration d'heures école ouvertes</h2></label></br></br>
      <table>
        <tr>
          <td>
          <td>
            <label>Stage du : {{$courseSession->starting}} au {{$courseSession->endding}}</label></br>
            <label>Établissement : {{$courseSession->school->name}}</label>
          </td>
          <!--<td>
           <img src="/images/logoMinistere.png" style="width:150px;height:150px">
          </td>
          <td>
            @if ($courseSession->school->name == "Lycée le Castel")
              <img src="/images/logoLeCastel.png" alt="logo_Castel" style="width:150px;height:150px">
            @elseif ($courseSession->school->name == "Collège Herriot")
              <img src="/images/logoCollegeHerriot.png" alt="logo_Herriot" style="width:250px;height:150px">
            @elseif ($courseSession->school->name == "Lycée Stephen Liégeard")
              <img src="/images/logoLiegeard.png" alt="logo_Liegard" style="width:150px;height:150px">
            @else
              <img src="/images/logoTresUnion.png" alt="logo_TresUnion" style="width:150px;height:150px">
            @endif
          </td>!-->
          </td>
        </tr>
      </table>
    </div>
  </div>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
        <div class="flex flex-col">
          <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
              <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                <table class="min-w-full divide-y divide-gray-200">
                  <thead>
                    <tr>
                      <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
                      Date
                      </th>
                      <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
                      Nom, Prénom intervenant
                      </th>
                      <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
                      Matière
                      </th>
                      <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
                      Niveau d'enseignement
                      </th>
                      <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
                      Salle lieu de stage
                      </th>
                      <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
                      Nombre d'heures
                      </th>
                    </tr>
                  </thead>
                  <tbody class="bg-white divide-y divide-gray-200">
                  @foreach($courseSession->lessons as $key => $lesson)
                  <tr>
                     <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">
                {{$lesson->date}}
                </div>
              </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                    {{$lesson->contributor->firstname}}  {{$lesson->contributor->lastname}}
                      <div class="text-sm text-gray-900">
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                    {{$lesson->subject->name}}
                      <div class="text-sm text-gray-900">
                      </div>
                    </td>
                    <td class="px-6 py-4 text-center whitespace-nowrap">
                    {{$lesson->level->name}}
                      <div class="text-sm text-gray-900">
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                      <div class="text-sm text-gray-900">
                         {{$courseSession->school->name}},{{$courseSession->school->city}}
                      </div>
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                    <?php
                      $dateDebut = strtotime($lesson->starting);
                      $dateFin = strtotime($lesson->endding);
                      $totHeure = date('H:i', $dateFin - $dateDebut);
                    ?>
                    {{$totHeure}}
                      <div class="text-sm text-gray-900">
                      </div>
                    </td>
                  </tr>   
                  @endforeach
                  </tbody>
                </table>
          <table class="min-w-full divide-y divide-gray-200">
        <thead>
          <tr></tr>
          <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
              Nom, Prénom intervenant
            </th>
            <th scope="col" class="px-6 py-3 bg-gray-200 text-left text-xs font-medium text-black-500 uppercase tracking-wider">
              Nombre d'heures totale
            </th>         
          </tr>
        </thead>
        <tbody class="bg-white divide-y divide-gray-200">
          @foreach ($heuretotale as $heuretotales)             
            <tr>
            <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">
                  {{$heuretotales->contributorFullName}}
                </div>                 
              </td>               
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">  
                    <div class="text-sm font-medium text-gray-900"> 
                    {{$heuretotales->totalHeure}}
                    </div>
                </div>
              </td>           
            </tr>     
          @endforeach
        </tbody>  
      </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <div class="m-4">
     <center><a href="{{ route("pdffichestage",['id'=>$courseSession->id])}}">
  <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
  Téléchargement en PDF</button></a></center>
  </div>
  </div>
 
</x-app-layout>