<x-app-layout>
    <x-slot name="header">
        <h2 class="leading-tight text-base">
            <a href="{{ route("session") }}" class="text-blue-700">Stage </a>/ Fiche stage
        </h2>
    </x-slot>
    <!----- Combobox choix date stage existant !----->

    <!-- création nouvelle route avec id pour garder combobox !--><br><br>
      <center><td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <a href="{{ route('fichesession') }}" class="text-indigo-600 hover:text-indigo-900" >Fiche Total </a>
                  </td></center>
    @foreach ($schools as $school)
    <form name="choixStage"  method="get">
    @endforeach
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-2 lg:px-2">
              <div class="mt-5 md:mt-0 md:col-span-2">
                <div class="col-md-5">
                          <h2>Choix du stage: </h2>
                          <select id="stage" name="stage" onchange="submit();" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">    
                          <option value="none" selected>Choisir le stage</option>
                          @foreach ($courseSessions as $courseSession)
                     
                      <option value={{ $courseSession->school_id }}>{{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}} a {{$courseSession->school->name}}  </option>    
                      @endforeach
                      
                      </select>
                </div>
              </div>
            
        </div>
      </div>
      <!----- Fin de la Combobox  !----->
      <!----- Tableaux des intervenants présent a une date choisie !----->
    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
          <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
               
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Nom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Email
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Téléphone
                        </th>
                        
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    <!-- parcours du tableau des intervenants !-->
                    
 
                    @foreach ($contributors  as $contributor)
                    @if (isset($_REQUEST['stage'])) 
                    @php
                    $choix=$_REQUEST['stage'];
                    @endphp
                    @else
                    @php
                    $choix= 0;
                    @endphp
                    @endif
                    @if ($contributor->school->id == $choix)
                  
                      <tr>
                      
                        <td class="px-6 py-4 whitespace-nowrap">
                        <div class="text-sm text-gray-900">
                        {{$contributor -> firstname}} {{$contributor -> lastname}} 
                        </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                        {{$contributor -> email}}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                        {{$contributor -> phone}}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                        <a href="{{ route('fichesession') }}" class="text-indigo-600 hover:text-indigo-900" >Fiche Heure </a>
                  </td>
                      </tr> 
                      
                        @endif
                      @endforeach 
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
        </div>
    </div>
    </form>
    
    </x-app-layout>
    