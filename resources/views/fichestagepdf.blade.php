<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
<div class="py-6" style="width:18cm">
      <div class="text-center" style="font-weight: bold; font-size: 17px; margin: 3px 0 3px 0;">Intervenants au stage du {{date('d/m/Y', strtotime($session->starting))}} au {{date('d/m/Y', strtotime($session->endding))}} à  {{$courseSession->school->name}},{{$courseSession->school->city}}</div>
      <table class="table table-bordered" id="table">
        <thead>
          <tr class="divide-x divide-gray-400">
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Date présence
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Nom, Prénom intervenant
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Matière
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Niveau d'enseignement
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Heure du cours
            </th>    
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Nombre d'heures
            </th>
          </tr>
        </thead>
        @foreach ($courseSession->lessons as $key => $lesson)     
          <tbody class="bg-white divide-y divide-gray-200">                 
            <tr>
            <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">
                {{$lesson->date}}
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="flex items-center">
                  <div class="ml-4">
                    <div class="text-sm font-medium text-gray-900">
                    {{$lesson->contributor->firstname}} {{$lesson->contributor->lastname}}
                    </div>
                  </div>
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">
                {{$lesson->subject->name}}
                </div>
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
              {{$lesson->level->name}}
              </td>
              
              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                {{$lesson->starting}} - {{$lesson->endding}}
              </td>
              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
              <?php
              $dateDeb= strtotime($lesson->starting);
              $dateFin=strtotime($lesson->endding);
              $heureTot= date('H:i',$dateFin-$dateDeb);
              ?>
              {{ $heureTot}}
              </td>
            </tr>
          </tbody>
       @endforeach
      </table>
      <table class="table table-bordered" id="table">
        <thead>
          <tr class="divide-x divide-gray-400">
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Nom, Prénom intervenant
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Nombre d'heure totale
            </th>         
          </tr>
        </thead>
          @foreach ($heuretotale as $heuretotales)   
          <tbody class="bg-white divide-y divide-gray-200">                 
            <tr>
            <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">
                  {{$heuretotales->contributorFullName}}          
                </div>                 
              </td>               
               <td class="px-6 py-4 whitespace-nowrap">
                <div class="text-sm text-gray-900">  
                    <div class="text-sm font-medium text-gray-900"> 
                      {{$heuretotales->totalHeure}}        
              </td>
                 @endforeach
                  </div>
                </div>
              </td>           
            </tr>
          </tbody>  
      </table>
      
</div>
<style>
  table {
    border: 2px solid rgba(0, 42, 255, 0.452);
    background-image: url("https://i.ibb.co/3dzzsk3/logo-1.png"); 
    background-size: 350px 150px;
    background-repeat: repeat;
  opacity: 0.1; 
  }
</style>