<x-app-layout>
    <x-slot name="header">
        <x-jet-dropdown>
          <x-slot name="trigger">         
            <button class="flex items-center hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
              <img src="https://img.icons8.com/color/44/000000/book-and-pencil.png"/>
                                <style>
                              img {
                                margin-right: 10px;
                                  position: sticky  ;
                                  margin-bottom: 5px;
                              }
                                </style>
            <div>Appel</div>
              <div class="ml-1">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                </svg>
              </div>
            </button>
          </x-slot>
          <x-slot name="content">
              <x-jet-dropdown-link href="{{ route('newappel') }}" class="bg-gray-100">
                  Effectuer un appel
              </x-jet-dropdown-link>
              <div class="border-t border-gray-100 "></div>
          </x-slot>
        </x-jet-dropdown>
    </x-slot>
    <div class="py-12">
       <div class="justify-center d-flex pb-1">
        <a href="{{ route("csvappellist",['id'=>$appel->id])}}">
            <button type="button" class="inline-flex items-center mb-3 mt-3 px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Télécharger l'appel 
            </button>
        </a>
       </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
         <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
            <div class="flex flex-col">
             <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
               <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                 <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Créée par
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Date (Jour/Mois/Année/Heure)
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 d-flex justify-content-end ">
                          <span class="badge badge-pill badge-dark display-4 d-flex justify-content-end">Informations de l'appel</span>
                        </th>
                      </tr>
                    </thead>
                   <tbody class="bg-white divide-y divide-gray-200">         
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">                
                              <div class="text-sm font-medium text-gray-900">
                             {{ $appel->createur }}
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">
                              <span class="border text-blue-800">      
                              {{ $appel->created_at->format('d.m.Y H:i:s') }}
                              </span>
                            </div>
                        </td>                                                
                      </tr>
                    </tbody> 
                  </table>               
                  <table class="min-w-full divide-y divide-red-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-blue-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Nom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-blue-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Prénom
                        </th>
                        {{-- <th scope="col" class="px-6 py-3 bg-blue-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                         Motif
                        </th> --}}
                        <th scope="col" class="px-6 py-3 bg-blue-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Présent
                        </th>
                        <th scope="col" class="px-6 py-3 bg-blue-50 d-flex justify-content-end ">
                          <span class="badge badge-pill badge-dark display-4 d-flex justify-content-end">Présence des élèves</span>
                        </th>
                      </tr>
                    </thead>
                   <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($appel->minorstudents as $minorstudent)
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">                
                              <div class="text-sm font-medium text-gray-900">
                             {{ $minorstudent->last_name }}
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">                            
                              {{ $minorstudent->first_name }}                     
                            </div>
                        </td>
                        {{-- <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">                                
                              {{ $minorstudent->pivot->motif}}                    
                            </div>
                        </td> --}}                                     
                        <td class="px-6 py-4 whitespace-nowrap">                             
                            <div class="text-sm font-medium text-gray-900">                          
                               @if ($minorstudent->pivot->present == "1")
                             <span class="text-success font-weight-bold">Présent</span> 
                             @else
                              <span class="text-danger font-weight-bold">Absent</span> 
                             @endif  
                            </div>
                        </td>                                                
                      </tr>
                     @endforeach
                    </tbody> 
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
</x-app-layout>
 

