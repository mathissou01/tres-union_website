<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
   <div class="py-6" style="width:18cm">  
    <table class="table table-bordered">
      <thead>
        <tr class="divide-x divide-gray-400">
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Créée par
          </th>
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Date (Jour/Mois/Année/Heure)
          </th>
        </tr>
      </thead>
      <tbody>
        <tr class="divide-x divide-gray-400">
          <td class="px-6 py-4 whitespace-nowrap">                                                  
                {{ $appel->createur }}                      
          </td>
          <td class="px-6 py-4 whitespace-nowrap">                                                          
                {{ $appel->created_at->format('d.m.Y H:i:s') }}                            
          </td>                                                
        </tr>            
      </tbody> 
    </table>
    <table class="table table-bordered">
      <thead>
        <tr class="divide-x divide-gray-400">
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Nom
          </th>
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Prénom
          </th>                 
          <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
            Présent
          </th>                    
        </tr>
      </thead>
      <tbody>
          @foreach ($appel->minorstudents as $minorstudent)
        <tr>
          <td class="px-6 py-4 whitespace-nowrap">                                           
                {{ $minorstudent->last_name }}                        
          </td>
          <td class="px-6 py-4 whitespace-nowrap">                                                          
                {{ $minorstudent->first_name }}                    
          </td>                                                           
          <td class="px-6 py-4 whitespace-nowrap">                                                                                 
                  @if ($minorstudent->pivot->present == "1")
                <span class="text-success font-weight-bold">Présent</span> 
                @else
              <span class="text-danger font-weight-bold">Absent</span> 
                @endif                            
          </td>                                                
        </tr>
          @endforeach
      </tbody> 
    </table>
  </div>
<style>
  table {
    border: 1px solid #333;
    background-image: url("https://i.ibb.co/3dzzsk3/logo-1.png"); 
    background-size: 350px 150px;
    background-repeat: repeat;
  opacity: 0.1; 
  }
</style>
