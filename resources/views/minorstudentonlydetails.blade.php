<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('student') }}" class="text-blue-700">Enfants</a>/ Informations de <i>({{ $minorstudent->first_name }} {{ $minorstudent->last_name }})</i>
    </h2>
  </x-slot>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between bg-gray-50">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Détails de l'enfant
          </h3>
          <span class="sm:ml-3">
            <a href="/minorstudent/mod/{{ $minorstudent->id }}">
              <button type="button" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Modifier
              </button>
            </a>
          </span>
        </div>
        <div class="border-t border-gray-200">
          <dl>
            <div class="bg-white-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Nom, Prénom
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->last_name }}  {{ $minorstudent->first_name }}
              </dd>
            </div>
              <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Email
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->email_address }}
              </dd>
            </div>
               <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Téléphone
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->phone_number }}
              </dd>
            </div>
            
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Adresse
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->street_address }} </br>
                   {{ $minorstudent->postal_code }}   {{ $minorstudent->city }} 
              </dd>
            </div>
            <div class="bg-white-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Ecole
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                 <div class="text-sm font-medium text-gray-900">
                               @if ($minorstudent->school)
                              {{$minorstudent->school->name}}, {{$minorstudent->school->city}}
                              @else
                                  Aucun
                              @endif
                            </div>
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Classe
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->level }}
              </dd>
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Option
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->option }}
              </dd>
            </div>
              <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Option
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                {{ $minorstudent->choices }}, @if($minorstudent->choices === "Paiement partiel")
                valeur déjà payé :
                 {{ $minorstudent->paiement }} €
                @else
                @endif
              </dd>
            </div>
           
         
            <div class="bg-blue-50 px-4 py-5 text-center">
              {{-- <a href="{{Storage::url($minorstudent->images->path)}}">
                <button type="button" class="btn btn-primary">
                  Télécharger attestation
                  <i class="fas fa-download"></i>
                </button>
              </a> --}}
              <a href="{{ route('show_codebar_minorstudent',['id'=>$minorstudent->id]) }}" >
                <button type="button" class="btn btn-primary">
                  Afficher le code bar pour pouvoir le télécharger 
                  <i class="fas fa-download"></i>
                </button>
              </a>
            </div>
        </div>
        </dl>
      </div>
    </div>
  </div>
</div>
</x-app-layout>
