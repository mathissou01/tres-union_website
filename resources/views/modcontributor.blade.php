<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route('contributor') }}" class="text-blue-700">Intervenants </a>/ Modification intervenant - <i>({{$contributor->firstname}} {{$contributor->lastname}})</i>
    </h2>
  </x-slot>
  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Modification de l'intervenant
          </h3>
        </div>
        <form action="{{ route("applymodcontributor",['id'=>$contributor->id]) }}" method="post" class="border-t border-gray-200">
          @csrf
          <dl>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Nom, Prénom
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                <div class="grid grid-cols-6 gap-6">
                  <div class="col-span-6 sm:col-span-3">
                    <label for="lastname" class="block text-sm font-medium text-gray-700">Nom</label>
                    <input type="text" id="lastname" name="lastname" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->lastname }}">
                  </div>
                  <div class="col-span-6 sm:col-span-3">
                      <label for="firstname" class="block text-sm font-medium text-gray-700">Prénom</label>
                      <input type="text" id="firstname" name="firstname" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->firstname }}">
                  </div>
                </div>
              </dd>
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Email
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  <input type="text" id="email" name="email" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->email }}">
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Téléphone
              </dt>
              <input type="text" id="phone" name="phone" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->phone }}">
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Adresse
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                <label for="street" class="block text-sm font-medium text-gray-700">Adresse</label>
                <input type="text" id="street" name="street" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->street }}">
                </br>
                <div class="grid grid-cols-6 gap-6">
                  <div class="col-span-6 sm:col-span-3">
                    <label for="city" class="block text-sm font-medium text-gray-700">Ville</label>
                    <input type="text" id="city" name="city" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->city }}">
                </div>
                  <div class="col-span-6 sm:col-span-3">
                    <label for="postal_code" class="block text-sm font-medium text-gray-700">Code Postal</label>
                    <input type="text" id="postal_code" name="postal_code" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->postal_code }}">
                  </div>
                </div>
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Diplome
              </dt>
              <input type="text" id="diploma" name="diploma" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->diploma }}">
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Contrat
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                <input type="text" id="contract" name="contract" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control" value="{{ $contributor->contract }}">
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Établissement de rattachement
              </dt>
              <select id="school" name="school" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
              @if (!$contributor->school)
                <option value="none" selected>Aucun établissement</option>
                @foreach ($schools as $school)
                  <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                @endforeach
              @else
                <option value="none">Aucun établissement</option>
                @foreach ($schools as $school)
                  @if ( $contributor->school->name === $school->name)
                  <option value={{ $school->id }} selected>{{ $school->name }}, {{ $school->city }}</option>
                  @else
                  <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                  @endif
                @endforeach
              @endif
              </select>
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Matières enseignées
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  <ul id="subjects_list" class="list-group list-group-flush col-span-6 sm:col-span-6 lg:col-span-2" style="max-height: 200px; overflow: auto;">
                    @foreach ($subjects as $subject)
                    <li class="list-group-item">
                      <div class="custom-control custom-checkbox">
                        @if (in_array($subject->id, $teached_subjects))
                            <input type="checkbox" class="custom-control-input" id="{{ $subject->id }}" name="subjects[]" value="{{ $subject->id }}" checked>
                        @else
                            <input type="checkbox" class="custom-control-input" id="{{ $subject->id }}" name="subjects[]" value="{{ $subject->id }}">
                        @endif
                        <label class="custom-control-label" for="{{ $subject->id }}">{{ $subject->name }}</label>
                      </div>
                    </li>
                    @endforeach
                  </ul>
              </dd>
            </div>
          </dl>
            <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
              <a href="{{ URL::previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Annuler
                      </a>
              <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Valider
              </button>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>
</x-app-layout>
