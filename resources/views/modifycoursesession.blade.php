<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route("session") }}" class="text-blue-700">Stage </a>/ Modification Stage du {{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}}
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Modification du stage
          </h3>
        </div>
        <form action="{{route('modifysession',['id'=>$courseSession->id])}}" method="post" class="border-t border-gray-200">
          @csrf
          <dl>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Dates
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                <div class="grid grid-cols-6 gap-6">
                  <div class="col-span-6 sm:col-span-3">
                    <label for="start_date" class="block text-sm font-medium text-gray-700">Début du stage</label>
                    <input type="text" id="start_date" name="start_date" class="mt-1 form-input block w-full sm:text-sm rounded-md date form-control" value={{date('d/m/Y', strtotime($courseSession->starting))}}>
                  </div>
                  <div class="col-span-6 sm:col-span-3">
                    <label for="stop_date" class="block text-sm font-medium text-gray-700">Fin du stage</label>
                    <input type="text" id="stop_date" name="stop_date" class="mt-1 form-input block w-full sm:text-sm rounded-md date form-control" value={{date('d/m/Y', strtotime($courseSession->endding))}}>
                  </div>
                </div>
              </dd>
            </div>
            <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Établissement
              </dt>
              <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                <select id="school" name="school" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                @foreach ($schools as $school)
                  @if ( $courseSession->school->name === $school->name)
                    <option value={{ $school->id }} selected>{{ $school->name }}, {{ $school->city }}</option>
                  @else
                    <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                  @endif
                @endforeach
                </select>
              </dd>
            </div>
            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
              <dt class="text-sm font-medium text-gray-500">
                Nombre de places
              </dt>
              <input type="number" id="max_places" name="max_places" class="mt-1  block w-full sm:text-sm form-input rounded-md" value={{$courseSession->places}}>
            </div>
          </dl>
            <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
              <a href="{{ route('showsession',['id'=>$courseSession->id]) }}" type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Annuler
              </a>
              <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                Valider
              </button>
            </div>
        </form>
      </div>
    </div>
  </div>
  <!---Modal form---->
  <div class="modal fade" id="monmornModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Ajouter une matière</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <select id="contributor" name="contributor" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            <option value=1 selected>Martin Dupond</option>
            <option>Jean Dumont</option>
          </select>
          <select id="subjects" name="subjects" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            <option value=1 selected>Maths</option>
            <option>Français</option>
          </select>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <button type="button" class="btn btn-primary">Ajouter</button>
      </div>
    </div>
  </div>
</div>
</div>
</x-app-layout>
<script type="text/javascript">

;(function($){
  $.fn.datepicker.dates['fr'] = {
    days: ["dimanche", "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi"],
    daysShort: ["dim.", "lun.", "mar.", "mer.", "jeu.", "ven.", "sam."],
    daysMin: ["d", "l", "ma", "me", "j", "v", "s"],
    months: ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
    monthsShort: ["janv.", "févr.", "mars", "avril", "mai", "juin", "juil.", "août", "sept.", "oct.", "nov.", "déc."],
    today: "Aujourd'hui",
    monthsTitle: "Mois",
    clear: "Effacer",
    weekStart: 1,
    format: "dd/mm/yyyy"
  };
}(jQuery));

    $('.date').datepicker({
       language: 'fr'
     });

</script>
