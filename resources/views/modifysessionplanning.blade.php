<x-app-layout>
  <x-slot name="header">
    <h2 class="leading-tight text-base">
      <a href="{{ route("session") }}" class="text-blue-700">Stage </a>/ <a href="{{ route("showplanning",["id"=>$courseSession->id]) }}" class="text-blue-700">Planing du stage du {{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}}</a> / Modification 
    </h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        <div class="px-4 py-5 sm:px-6 flex object-right justify-between">
          <h3 class="text-lg leading-6 font-medium text-gray-900">
            Modification du Planning
          </h3>
            <a href="{{ route('showplanning',['id'=>$courseSession->id]) }}" class="text-indigo-600 hover:text-indigo-900">Revenir au planning</a>
        </div>
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-3">
          <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="container-fluid">
              <header>
                <div class="row d-none d-sm-flex p-1 bg-dark text-white">
                  @if (sizeof($dates)<7)
                    @foreach ($dates as $date)
                    <h5 class="col-sm p-1 text-center">{{$date->translatedFormat('l')}}</h5>
                    @endforeach  
                  @else
                    @for ($i = 0; $i < 7; $i++)
                    <h5 class="col-sm p-1 text-center">{{$dates[$i]->translatedFormat('l')}}</h5> 
                    @endfor
                  @endif
                   
                </div>
              </header>
              <div class="row border border-right-0 border-bottom-0">
                @if (sizeof($dates)<7)
                  @foreach ($dates as $key=>$date)
                  <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                    <h5 class="row align-items-center">
                      <span class="date col-1">{{$date->format('d/m/Y')}}</span>
                      <small class="col d-sm-none text-center  text-gray-900">{{$date->translatedFormat('l')}}</small>
                      <span class="col-1"></span>
                    </h5>
                    <button type="button" id="{{$date->format('d/m/Y')}}" class="addsubject inline-flex py-1 px-1 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-400 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ml-1"  data-toggle="modal" data-target="#monmornModal">
                      <i class="far fa-plus-square mr-1"></i>
                      Ajouter
                    </button>
                    <div>Matin</div>
                    @foreach ($courseSession->lessons as $keylesson => $lesson)
                      @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->lt(Carbon::createFromFormat('H:i', '12:00')))
                      <a id='lesson{{$lesson->id}}' class="event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#modifyModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span> <br> {{ $lesson->room }}</a>
                      @endif                        
                    @endforeach
                    <div>Après-Midi</div>
                    @foreach ($courseSession->lessons as $keylesson => $lesson)
                      @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->gte(Carbon::createFromFormat('H:i', '12:00')))
                        <a id='lesson{{$lesson->id}}' class="event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#modifyModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span> <br> {{ $lesson->room }}</a>                      @endif                        
                    @endforeach
                  </div>
                  @if (($key+1)%7==0)
                    <div class="w-100"></div>
                  @endif
                @endforeach
                @else
                  @for ($i = 0; $i < sizeof($dates)+(7-sizeof($dates)%7); $i++)
                    @if ($i<sizeof($dates))
                    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                      <h5 class="row align-items-center">
                        <span class="date col-1">{{$dates[$i]->format('d/m/Y')}}</span>
                        <small class="col d-sm-none text-center  text-gray-900">{{$dates[$i]->translatedFormat('l')}}</small>
                        <span class="col-1"></span>
                      </h5>
                      <button type="button" id="{{$dates[$i]->format('d/m/Y')}}" class="addsubject inline-flex py-1 px-1 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-400 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 ml-1"  data-toggle="modal" data-target="#monmornModal">
                        <i class="far fa-plus-square mr-1"></i>
                        Ajouter
                      </button>
                      <div>Matin</div>
                      @foreach ($courseSession->lessons as $keylesson => $lesson)
                        @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($dates[$i]) && Carbon::createFromFormat('H:i:s',$lesson->starting)->lt(Carbon::createFromFormat('H:i', '12:00')))
                          <a id='lesson{{$lesson->id}}' style='  word-wrap: break-word' class="event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#modifyModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span> <br> {{ $lesson->room }}</a>
                        @endif                        
                      @endforeach
                      <div>Après-Midi</div>
                      @foreach ($courseSession->lessons as $keylesson => $lesson)
                        @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($dates[$i]) && Carbon::createFromFormat('H:i:s',$lesson->starting)->gte(Carbon::createFromFormat('H:i', '12:00')))
                        <a id='lesson{{$lesson->id}}' style='  word-wrap: break-word' class="event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-secondary text-white" title="Test Event 2" data-toggle="modal" data-target="#modifyModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span> <br> {{ $lesson->room }}</a>
                        @endif                        
                      @endforeach
                    </div>
                    @else
                    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-light text-muted">
                    </div>   
                    @endif
                    @if (($i+1)%7==0)
                    <div class="w-100"></div>
                    @endif 
                  @endfor
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

@foreach ($courseSession->lessons as $key => $lesson)
  <!---Modal form---->
<div class="modal fade modal_class_id" id="modifyModal{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
 <form method="post" action="{{ route("modifylesson") }}" class="modal-content">
   <div class="modal-header">
     <h5 class="modal-title text-lg leading-6 font-medium text-gray-900" id="ModalLongTitle">Informations sur le cours</h5>
     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
       <span aria-hidden="true">&times;</span>
     </button>
   </div>
   <div class="modal-body">
     @csrf
     <dl>
       <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
         <dt class="text-sm font-medium text-gray-500">
           Matière
         </dt>
         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
           {{$lesson->subject->name}}
         </dd>
       </div>
       <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
         <dt class="text-sm font-medium text-gray-500">
           Horaire
         </dt>
         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <div class="grid grid-cols-6 gap-6">
            <div class="col-span-6 sm:col-span-3">
              <label for="starting_time" class="block text-sm font-medium text-gray-700">Début</label>
              <input type="text" id="starting" name="starting_time" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control time" value="{{$lesson->starting}}">
            </div>
            <div class="col-span-6 sm:col-span-3">
                <label for="endding_time" class="block text-sm font-medium text-gray-700">Fin</label>
                <input type="text" id="endding" name="endding_time" class="mt-1 form-input block w-full sm:text-sm rounded-md form-control time" value="{{$lesson->endding}}">
            </div>
          </div>
         </dd>
       </div>
       <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
         <dt class="text-sm font-medium text-gray-500">
           Intervenant
         </dt>
         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <select id="contributormod" name="contributor" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @foreach ($contributors as $contributor)
              {{$lesson->contributor_id}}
              @if ( $lesson->contributor_id === $contributor->id)
                <option value={{ $contributor->id }} selected>{{ $contributor->firstname }} {{ $contributor->lastname }}</option>
              @else
                <option value={{ $contributor->id }}>{{ $contributor->firstname }} {{ $contributor->lastname }}</option>
              @endif
            @endforeach
          </select>
         </dd>
       </div>
       <div class="bg-white px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
         <dt class="text-sm font-medium text-gray-500">
           Niveau
         </dt>
         <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
          <select id="level" name="level" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @foreach ($levels as $level)
              @if ( $lesson->level->id === $level->id)
                <option value={{ $level->id }} selected>{{ $level->name }}</option>
              @else
                <option value={{ $level->id }}>{{ $level->name }}</option>
              @endif
            @endforeach
          </select>
         </dd>
       </div>
       <label for="room" class="block text-sm font-medium text-gray-700">Salle</label>
       <input class="form-control" type="text" id="room" name="room" value="{{$lesson->room}}"/>
       <input type="hidden" id="lesson" name="lesson" value="{{$lesson->id}}"> 
     </dl>
    </div>
   <div class="modal-footer">
    <button type="button" id='{{$lesson->id}}' class="btn btn-danger" data-dismiss="modal">Supprimer</button>
     <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
     <button type="submit" class="btn btn-primary">Modifier</button>
   </div>
  </form>
</div>
</div>
@endforeach

<!---Modal form---->
<div class="modal fade modal_class_id" id="monmornModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <form method="POST" action="{{ route("addlesson") }}" class="modal-content">
      @csrf
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLongTitle">Ajouter une matière</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div  class="modal-body">
          <label for="starting_time" class="block text-sm font-medium text-gray-700">Heure début</label>
          <input class="form-control time" type="text" id="starting_time" name="starting_time"/>
          <label for="starting_time" class="block text-sm font-medium text-gray-700">Heure fin</label>
          <input class="form-control time" type="text" id="endding_time" name="endding_time"/>
          <label for="subject" class="block text-sm font-medium text-gray-700">Matière</label>
          <select id="subject" name="subject" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @foreach ($subjects as $subject)
              <option value={{ $subject->id }}>{{ $subject->name }}</option>
            @endforeach
          </select>
          <label for="level" class="block text-sm font-medium text-gray-700">Niveau</label>
          <select id="level" name="level" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
            @foreach ($levels as $level)
              <option value={{ $level->id }}>{{ $level->name }}</option>
            @endforeach
          </select>
          <label for="contributor" class="block text-sm font-medium text-gray-700">Intervenant</label>
          <select id="contributor" name="contributor" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
          </select>
          <label for="room" class="block text-sm font-medium text-gray-700">Salle</label>
          <input class="form-control" type="text" id="room" name="room"/>
          <input type="hidden" id="date_modal" name="date" value=""> 
          <input type="hidden" id="session" name="session" value="{{$courseSession->id}}"> 
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
        <button type="submit" class="btn btn-primary">Ajouter</button>
      </div>
    </form>
  </div>
</div>
  @csrf
<script>

  $('.time').datetimepicker({

      format: 'HH:mm'

  });

  $("#contributor").empty();
    id = $(this).data('id');
    $.ajax({
      type: 'POST',
      url: "{{ route('contributorsbysubject') }}",
      data: {
        '_token': $("input[name=_token]").val(),
        'subject': $( "#subject" ).val()
      },
      success: function(data) {
        $.each(data, function(index, element) {
          $("#contributor").append('<option value='+element.id+'>'+element.firstname+' '+element.lastname+'</option>')
        });
       //
      },
    });
  
  $(document).ready(function () {
  $('#monmornModal').on('show.bs.modal', function (event) {
    let id = $(event.relatedTarget).attr('id');
    $('#date_modal').val(id);
  });
  });

  /* $('.addsubject').click(function(event){
    alert("ok");
    $('#ModalLongTitle').append(' '+event.target.id);
    $('.modal_class_id').attr("id",event.target.id);
  }); */

  $('#subject').on('change', function(event){
    $("#contributor").empty();
    id = $(this).data('id');
    $.ajax({
      type: 'POST',
      url: "{{ route('contributorsbysubject') }}",
      data: {
        '_token': $("input[name=_token]").val(),
        'subject': $( "#subject" ).val()
      },
      success: function(data) {
        $.each(data, function(index, element) {
          $("#contributor").append('<option value='+element.id+'>'+element.firstname+' '+element.lastname+'</option>')
        });
       //
      },
    });
  });

  $('.btn-danger').click(function(event){
    var id = $(this).attr('id');
    data= {
        '_token': $("input[name=_token]").val(),
        'id': id
      }
    $.post("{{ route('deletelesson') }}",data,function(data) {
          $("#lesson"+data.id).remove()
      })
  });
</script>


</x-app-layout>
