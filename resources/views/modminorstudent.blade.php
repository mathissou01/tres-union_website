<x-app-layout>
    <x-slot name="header">
        <h2 class="leading-tight text-base">
            <a href="{{ route("student") }}" class="text-blue-700">Etudiant </a>/ Modifier l'enfant <i>({{ $last_name }} {{$first_name}})</i>
        </h2>
    </x-slot>
    <!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-indigo-600">
  <div class="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-8">
    <div class="flex items-center justify-between flex-wrap">
      <div class="w-0 flex-1 flex items-center">
        <span class="flex p-2 rounded-lg bg-indigo-800">
          <!-- Heroicon name: speakerphone -->
          <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z" />
          </svg>
        </span>
        <p class="ml-3 font-medium text-white truncate">
        </p>
      </div>
    </div>
  </div>
</div>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <div class="mt-5 md:mt-0 md:col-span-2">
                <form action="{{ route("mod_minorstudent",['id'=>$minorstudent->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                  <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                      <div class="grid grid-cols-6 gap-6">
                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Nom</label>
                          <input type="text" name="last_name" id="last_name" value="{{ $last_name }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Prénom</label>
                          <input type="text" name="first_name" id="first_name" value="{{ $first_name }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Email</label>
                          <input type="text" name="email_address" id="email_address" value="{{ $email_address }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>
                        

                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Telephone</label>
                          <input type="text" name="phone_number" id="phone_number" value="{{ $phone_number }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>
                        

                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Adresse</label>
                          <input type="text" name="street_address" id="street_address" value="{{ $street_address }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="city" class="block text-sm font-medium text-gray-700">Ville</label>
                          <input type="text" name="city" id="city" value="{{ $city }}" class="mt-1 form-input block w-full sm:text-sm rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Code postal</label>
                          <input type="text" name="postal_code" id="postal_code" value="{{ $postal_code }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Classe</label>
                            <select id="level" name="level" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">  
                            <option value="{{ $level }}" selected>{{ $level }}</option>
                              <option value="CP">CP</option>
                                <option value="CE1">CE1</option>
                                  <option value="CE2">CE2</option>
                                    <option value="CM1">CM1</option>
                                      <option value="CM2">CM2</option>
                                        <option value="6ème">6ème</option>
                                          <option value="5ème">5ème</option>
                                            <option value="4ème">4ème</option>
                                               <option value="3ème">3ème</option>
                                                <option value="2nd">2nd</option>
                                                  <option value="1ère">1ère</option>
                                                    <option value="Terminale">Terminale</option>
                          </select>
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <dt class="text-sm font-medium text-gray-500">
                            Ecole
                          </dt>
                          <select id="school" name="school" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                          @if (!$minorstudent->school)
                            <option value="none" selected>Aucun établissement</option>
                            @foreach ($schools as $school)
                              <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                            @endforeach
                          @else
                            <option value="none">Aucun établissement</option>
                            @foreach ($schools as $school)
                              @if ( $minorstudent->school->name === $school->name)
                              <option value={{ $school->id }} selected>{{ $school->name }}, {{ $school->city }}</option>
                              @else
                              <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                              @endif
                            @endforeach
                          @endif
                          </select>
                        </div>
                         <div class="col-span-6 sm:col-span-3">
                           <label for="choices" class="block text-sm font-medium text-gray-700">Etat du paiement</label>                     
                          <select id="choices" onchange="togglePaiement();" name="choices" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            @if($choices === "Paiement en attente")
                              <option value="Paiement en attente" selected>Paiement en attente</option>
                              <option value="Paiement effectué">Paiement effectué</option>
                              <option value="Paiement partiel">Paiement partiel</option>
                               @elseif($choices === "Paiement effectué")
                              <option value="Paiement en attente">Paiement en attente</option>
                              <option value="Paiement effectué" selected>Paiement effectué</option>
                              <option value="Paiement partiel">Paiement partiel</option>
                            @else($choices === "Paiement partiel")
                              <option value="Paiement en attente"> Paiement en attente</option>
                              <option value="Paiement effectué" >Paiement effectué</option>
                              <option value="Paiement partiel" selected>Paiement partiel</option> --}}
                            @endif
                          </select>
                        </div>
                           <div class="col-span-6 sm:col-span-3" id="priceget" style="display:none">
                          <label for="name" class="block text-sm font-medium text-gray-700">Valeur déjà payé en €</label>
                          <input type="number" name="paiement" id="paiement" value="{{ $paiement }}" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                          
                        </div>
                      
                        {{-- <div class="col-span-6 sm:col-span-3">
                          <label for="attestation">Attestation d'assurance : </label>
                          <input type="file" id="attestation" name="attestation" accept="application/pdf,application/vnd.ms-excel">
                      </div> --}}
                        <input type="hidden" name="id" value="{{ $id }}">
                      </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <a href="{{ URL::previous() }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Annuler
                      </a>
                      <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enregistrer
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
</x-app-layout>
<script>
  const paiement = document.getElementById('choices');
  paiement.addEventListener('click', togglePaiement);
  function togglePaiement() {  
    if(
    (document.getElementById('choices').value == "Paiement partiel")
    )
    {
    document.getElementById('priceget').style.display="block"
 
    }
    if((document.getElementById('choices').value == "Paiement effectué"))
    {
    document.getElementById('priceget').style.display="none"
    
    }
    if((document.getElementById('choices').value == "Paiement en attente"))
    {
    document.getElementById('priceget').style.display="none"
    
    }
  }
  togglePaiement()
  </script>