<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="crsf-token" content="{{ csrf_token() }}">
  <script src="{{ asset('js/app.js') }}"></script>                 
                      <script>
var FocusForced = false; //Desactive l'input createur

focusMethod = function getFocus() {
  document.getElementById("q").focus();
  $("#formToDisable :input").prop("disabled", true);
  FocusForced = true;
}

function archives(id) //Fais disparaitre le button code bar avec la scanette
{
	var archive=document.getElementById("activate").style;
  if((archive.display=='flex') | (archive.display=='')) {
		archive.display='none';
	} else {
		archive.display='block';
	};
  document.getElementById('anticlick').style.backgroundColor = 'rgba(29, 255, 0, 0.7)';
}
function remettre(id) // Fais reapparaitre le boutton avec code bar et scanette
{
	var archive=document.getElementById("activate").style;
  $("#formToDisable :input").prop("disabled", false);
  FocusForced = false;
  setTimeout(() => {
    document.getElementById("createur").focus();
  }, 200);  

  if((archive.display=='none') | (archive.display=='')) {
		archive.display='flex';
	} else {
		archive.display='none';
	};
}
function vaEtVient(){ //Fais apparaitre le button arreter 
	var archive=document.getElementById("desactivate").style;

  if((archive.display=='none') | (archive.display=='')) {
		archive.display='block';
	} else {
		archive.display='none';
	};
}
function vient(){ //Fais dispiraitre le bouton arreter
	var archive=document.getElementById("desactivate").style;
  
  if((archive.display=='block') | (archive.display=='')) {
		archive.display='none';
	} else {
		archive.display='block';
	};
  document.getElementById("createur").focus();
  setTimeout(() => {
    document.getElementById('anticlick').style.backgroundColor = '';
  }, 1000); 
  document.getElementById('anticlick').style.backgroundColor = 'rgba(255, 0, 0, 0.64)';
}
</script>                   
</html>
 {{ Html::style('css/illustration.css') }}
<x-app-layout>
    <x-slot name="header"> 
        <h2 class="leading-tight text-base">
            <a href="{{ route("appel") }}" class="text-blue-700">Appels </a>/ Créer un appel
        </h2>
        <div class="d-flex justify-content-end">
          <button type="button" class="custom-btn btn-2" data-toggle="modal" data-target="#confirmDeleteModal">Utilisation du mode code bar</button>     
        </div>  
    </x-slot>
    <div class="d-flex justify-content-center">
      <form action="{{ route('codebar.search')}}" method="post" id="search-codebar-form">
        <input name="q" id="q" type="text" class="lg-4" style = "position: absolute; left:-1000px;"/>              
      </form>  
    </div>  
    <div class="py-12" id="anticlick">      
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="buttonactivate" id="activate">
          <img src="https://i.postimg.cc/MK22FTFN/scnaette.png" class="scanette">  {{--Bouton pour activer--}}
            <button type="button" class="codebar btn-3" onclick="focusMethod();archives();vaEtVient()"><a></a></button>           
        </div> 
          <div class="buttondesactivate" id="desactivate">
            <button class="name noselect" onclick="remettre();vient();">Arrêter la lecture de code bar</button> {{--Bouton pour activer--}}
          </div>
          <div class="bakcgroundgrad">       
            <div class="overflow-hidden shadow-xl sm:rounded-lg">           
              <div class="mt-5 md:mt-0 md:col-span-2">  
                <form action="{{ route('add_appel') }}" method="POST" class="formToDisable" id="formToDisable">
                @csrf 
                  <div class="px-4 py-2">
                      <div class="grid grid-cols-6 ">
                        <div class="col-span-6">
                          <div class="pb-4">
                            <label for="createur" class="block text-sm font-medium text-gray-700">Créer par</label>
                            <select name="createur" id="createur" class="mt-1 block w-full sm:text-sm form-input rounded-md">
                              {{-- Boucler pour mettre tout les intervenants de la base de données dans le select --}}
                             @foreach ($contributors as $contributor)
                                <option value="{{$contributor->firstname}}, {{$contributor->lastname}}">{{$contributor->firstname}}, {{$contributor->lastname}}</option>
                              @endforeach
                           </select>
                          </div>
                                <style>
                                ::placeholder {
                                font-size: 1em;
                                letter-spacing: 0.5px;
                                  color: rgba(29, 0, 0, 0.384) !important;
                                              }
                                </style>
                      <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                          <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                            <table class="min-w-full divide-y divide-gray-200">
                              <thead>
                                <tr>
                                  <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Nom
                                  </th>    
                                  <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Prénom
                                  </th>
                                  <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Code Bar
                                  </th>
                                    {{-- <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                      Motif
                                    </th> --}}
                                  <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Présent
                                  </th>
                                </tr>
                              </thead>
                              {{-- <div id="etudiant">         
                          @foreach ($students as $student)
                          <tbody class="bg-white divide-y divide-gray-200">
                              <tr>
                                <td class="px-6 py-4 whitespace-nowrap">                
                                      <div class="text-sm font-medium text-gray-900">
                              {{$student->last_name}}
                                  </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm font-medium text-gray-900">
                                  {{$student->first_name}} 
                                    </div>
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm font-medium text-red-900">
                                  {{$student->uuid}}
                                    </div>
                                </td>
                              
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <div class="text-sm font-medium text-gray-900">      
                                    <input class="form-check-input" type="checkbox"  name="students[]" type="checkbox" value="{{ $student->id }}" id="student-{{ $student->id }}"/>    
                                    </div>
                                </td>
                              </tr>
                            </tbody>               
                            @endforeach 
                              </div>  --}}
                          <div id="etudiant2">         
                            @foreach ($minorstudents as $minorstudent)
                              <tbody class="bg-white divide-y divide-gray-200">
                                <tr>
                                  <td class="px-6 py-4 whitespace-nowrap">                
                                    <div class="text-sm font-medium text-gray-900">
                                        {{$minorstudent->last_name}}
                                    </div>
                                  </td>
                                  <td class="px-6 py-4 whitespace-nowrap">
                                      <div class="text-sm font-medium text-gray-900">
                                        {{$minorstudent->first_name}} 
                                      </div>
                                  </td>
                                  <td class="px-6 py-4 whitespace-nowrap">
                                      <div class="text-sm font-medium text-red-900">
                                        {{$minorstudent->uuid}}
                                      </div>
                                  </td>
                                  {{-- <td class="px-6 py-4 whitespace-nowrap">
                                      <div class="text-sm font-medium text-purple-900">
                                    <input type="text" placeholder="Entrer un motif" id="motif" name="motif-{{ $minorstudent->id }}">  
                                      </div>          
                                  </td> --}}
                                  <td class="px-6 py-4 whitespace-nowrap">
                                      <div class="form-check-inline">      
                                        <input class="form-check-input ml-3" type="checkbox"  name="minorstudents[]" type="checkbox" value="{{ $minorstudent->id }}" id="minorstudent-{{ $minorstudent->id }}"/>    
                                      </div>
                                  </td>
                                </tr>
                                  </tbody>        
                                @endforeach 
                              </div>
                            </table>
                          </div>
                        </div>
                      </div> 
                    </div>
                  </div> 
                    <div class="px-4 py-3 text-right sm:px-6">
                        <a href="{{ route('appel') }}" class="inline-flex justify-center py-2 px-4 border border-dark shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-800">
                        Annuler
                        </a>  
                        <a href=""><button type="submit" class="inline-flex justify-center py-2 px-4 border border-dark shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-green-600 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Enregistrer votre appel
                        </button></a>  
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  <div class="modal fade" id="confirmDeleteModal" data-selected-subject="0" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header bg-green-600 ">
            <h4 class="modal-title text-light" id="confirmDeleteModalTitle"><u>Consignes</u></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
          <div class="modal-body">         
            <span>Voici quelques consignes a respecté pour bien utiliser l'appel avec lecture de code bar :</span><br>
            <hr>
            <span class="h6 font-weight-bold text-red-600 text-underline"><u>Connexion de la scanette :</u></span></br>
            <span>Tout d'abord, veuillez bien brancher votre scanette au port USB de l'ordinateur.</br>
            Veuillez aussi vous assurer que le voyant de votre scanette soit allumé.</br></span>
            <hr>
            <span class="h6 font-weight-bold text-red-600 text-underline"><u>Mode lecture de code bar:</u></span></br>
            Vous pouvez ensuite appuyer sur le bouton pour passer en mode lecture de code bar.</br>
            Après cela veuillez ne plus toucher à la souris ou le clavier de l'ordinateur.</br>
            Cela vous ferait quitter du mode code bar.<br>
            <hr>
            <span class="h6 font-weight-bold text-red-600 text-underline"><u>Divers:</u></span></br>
            Un code bar abimé pourrait empêcher sa lecture.</br>
            Si un code bar ne marche pas, veuillez quitter le mode code bar et cocher l'élève à la main.
          </div>         
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">C'est compris !</button>         
          </div>
      </div>
    </div>
  </div>
    <script type="text/javascript">
        $(".delete-button").click(function(){
            $("#confirmDeleteModal").attr("data-selected-subject",this.name)
        })
        $("#confirmDelete").click(function(){
            $(location).attr("href", "/minorstudent/del/"+$("#confirmDeleteModal").attr('data-selected-subject'));
        })
        document.getElementById("q").addEventListener("focusout", function() {
          if (FocusForced == true){
            setTimeout(() => {
              document.getElementById("q").focus();
            }, 100);
          }
        });
    </script>     
</x-app-layout>
