<x-app-layout>
    <x-slot name="header">
        <h2 class="leading-tight text-base">
            <a href="{{ route("familly") }}" class="text-blue-700">Famille </a>/ Ajouter un enfant
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <div class="mt-5 md:mt-0 md:col-span-2">
                <form action="{{ route('addchild', [$familly_id]) }}" method="POST" enctype="multipart/form-data">  {{-- A corriger / relier id a famille --}}
                  @csrf           
                  <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                      <div class="grid grid-cols-6 gap-6">
                        <div class="col-span-6 sm:col-span-3">
                          <label for="first_name" class="block text-sm font-medium text-gray-700">Prénom</label>
                          
                          <input type="text" id="first_name" name="first_name" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="last_name" class="block text-sm font-medium text-gray-700">Nom</label>
                          <input type="text" id="last_name" name="last_name" class="mt-1 form-input block w-full sm:text-sm rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="email_address" class="block text-sm font-medium text-gray-700">Email</label>
                          <input type="text" id="email_address" name="email_address" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="phone_number" class="block text-sm font-medium text-gray-700">Téléphone</label>
                          <input name="phone_number" type="text" id="phone_number" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="street_address" class="block text-sm font-medium text-gray-700">Adresse</label>
                          <input name="street_address" type="text" id="street_address" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="postal_code" class="block text-sm font-medium text-gray-700">Code postal</label>
                          <input name="postal_code" type="text" id="postal_code" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="city" class="block text-sm font-medium text-gray-700">Ville</label>
                          <input name="city" type="text" id="city" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="school" class="block text-sm font-medium text-gray-700 mt-2">Établissement</label>
                          <select id="school" name="school" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="none" selected>Aucun établissement</option>
                          @foreach ($schools as $school)
                            <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                          @endforeach
                          </select>
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                          <label for="option" class="block text-sm font-medium text-gray-700">Option</label>
                          <select id="option" name="option" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="Aucune option">Aucune option</option>
                            <option value="Histoire Géographique">Histoire Géographique</option>
                             <option value="Maths">Maths</option>
                              <option value="Littérature, Langues et Cultures de Antiquité">Littérature, Langues et Cultures de l'Antiquité</option>
                               <option value="Humanités, Littérature et Philosophie">Humanités, Littérature et Philosophie</option>
                                <option value="Numérique et Sciences Informatiques">Numérique et Sciences Informatiques</option>
                                 <option value="SVT">SVT</option>
                                  <option value="Sciences de ingénieur">Sciences de l'ingénieur</option>
                                   <option value="Sciences Économiques et Sociales">Sciences Économiques et Sociales</option>
                                    <option value="Physique Chimie">Physique Chimie</option>
                                     <option value="Arts:Histoire des arts...">Arts : Histoire des arts...</option>
                                      <option value="Biologie Écologie">Biologie Écologie</option>
                                       <option value="Éducation Physique">Éducation Physique</option>
                          </select>
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                          <label for="level" class="block text-sm font-medium text-gray-700">Classe</label>
                          <select id="level" name="level" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">  
                            <option value="" selected>Choisir une classe</option>
                              <option value="CP">CP</option>
                                <option value="CE1">CE1</option>
                                  <option value="CE2">CE2</option>
                                    <option value="CM1">CM1</option>
                                      <option value="CM2">CM2</option>
                                        <option value="6ème">6ème</option>
                                          <option value="5ème">5ème</option>
                                            <option value="4ème">4ème</option>
                                               <option value="3ème">3ème</option>
                                                <option value="2nd">2nd</option>
                                                  <option value="1ère">1ère</option>
                                                    <option value="Terminale">Terminale</option>
                          </select>
                        </div>
                        {{-- <div class="col-span-6 sm:col-span-3">
                          <label for="attestation">Attestation d'assurance : </label>
                          <input type="file" id="attestation" name="attestation" accept="application/pdf,application/vnd.ms-excel">
                      </div> --}}
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                    <a href="/familly"><button type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Annuler
                      </button></a>
                      <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enregistrer
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
</x-app-layout>
