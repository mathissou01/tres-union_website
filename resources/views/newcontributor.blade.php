<x-app-layout>
    <x-slot name="header">
        <h2 class="leading-tight text-base">
            <a href="{{ route("contributor") }}" class="text-blue-700">Intervenants </a>/ Ajouter un nouvel intervenant
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <div class="mt-5 md:mt-0 md:col-span-2">
                <form action="{{ route('addcontributor') }}" method="POST">
                @csrf
                  <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                      <div class="grid grid-cols-6 gap-6">
                        <div class="col-span-6 sm:col-span-3">
                          <label for="first_name" class="block text-sm font-medium text-gray-700">Prénom</label>
                          <input type="text" id="first_name" name="firstname" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="last_name" class="block text-sm font-medium text-gray-700">Nom</label>
                          <input type="text" id="last_name" name="lastname" class="mt-1 form-input block w-full sm:text-sm rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="email_address" class="block text-sm font-medium text-gray-700">Email</label>
                          <input type="text" id="email_address" name="email" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="phone_number" class="block text-sm font-medium text-gray-700">Téléphone</label>
                          <input name="phone" type="text" id="phone_number" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6">
                          <label for="street_address" class="block text-sm font-medium text-gray-700">Adresse</label>
                          <input type="text" name="street" id="street_address" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                          <label for="city" class="block text-sm font-medium text-gray-700">Ville</label>
                          <input type="text" name="city" id="city" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                          <label for="postal_code" class="block text-sm font-medium text-gray-700">Code Postal</label>
                          <input type="text" name="postal_code" id="postal_code" class="mt-1 block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="flex items-start mt-6 col-span-6 sm:col-span-6 lg:col-span-2" id="boxes">
                          <div class="flex items-center h-5">
                            <input name="rules" id="rules" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                          </div>
                          <div class="ml-3 text-sm">
                            <label for="rules" class="font-medium text-gray-700">Réglement</label>
                            <p class="text-gray-500">Réglement lu et signé</p>
                          </div>
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                          <label for="certificate" class="block text-sm font-medium text-gray-700">Diplôme (aquis ou en cours)</label>
                          <input type="text" id="certificate" name="diploma" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                          <label for="contract" class="block text-sm font-medium text-gray-700 mt-2">Type de Contrat</label>            
                            <select name="contract" id="contract" class="mt-1 block w-full sm:text-sm form-input rounded-md">>
                              <option value="">Choisissez un contract</option>
                                <option value="Bénévole">Bénévole</option>
                                  <option value="Intervenant">Salarié</option>
                                    <option value="Professeur">Professeur</option>   
                                      <option value="Stagiaire">Stagiaire</option> 
                                        <option value="Service civique">Service civique</option>                                 
                            </select>
                          <label for="school" class="block text-sm font-medium text-gray-700 mt-2">Établissement</label>
                          <select id="school" name="school" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                            <option value="none" selected>Aucun établissement</option>
                          @foreach ($schools as $school)
                            <option value={{ $school->id }}>{{ $school->name }}, {{ $school->city }}</option>
                          @endforeach
                          </select>
                        </div>
                        <div class="col-span-6 sm:col-span-3">
                            <label for="subject_list" class="block text-sm font-medium text-gray-700">Matières enseignées</label>
                            <ul id="subjects_list" class="list-group list-group-flush col-span-6 sm:col-span-6 lg:col-span-2" style="max-height: 200px; overflow: auto;">
                              @foreach ($subjects as $subject)
                              <li class="list-group-item">
                                <!-- Default checked -->
                                <div class="custom-control custom-checkbox">
                                  <input type="checkbox" class="custom-control-input" id="{{ $subject->id }}" name="subjects[]" value="{{ $subject->id }}">
                                  <label class="custom-control-label" for="{{ $subject->id }}">{{ $subject->name }}</label>
                                </div>
                              </li>
                              @endforeach
                            </ul>
                        </div>
                      </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <a href="{{ route('contributor') }}">
                        <button type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Annuler
                        </button>
                      </a>
                      <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enregistrer
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
</x-app-layout>
