<x-app-layout>
    <x-slot name="header">
      <h2 class="leading-tight text-base">
          <a href="{{ route("familly") }}" class="text-blue-700">Familles </a>/ Ajouter une nouvelle famille
      </h2>
    </x-slot>
    <div class="py-12" x-data="load()">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <div class="mt-5 md:mt-0 md:col-span-2">
                <form action="{{ route('addfamilly') }}" method="POST" id="add_familly_form">
                  @csrf          
                  <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                      <div class="grid grid-cols-6 gap-6">
                        <div class="col-span-6 sm:col-span-3">
                          <label for="last_name" class="block text-sm font-medium text-gray-700">Nom du représentant légal</label>
                          <input name="last_name" type="text" id="last_name" class="mt-1 form-input block w-full sm:text-sm rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="first_name" class="block text-sm font-medium text-gray-700">Prénom du représentant légal</label>
                          <input name="first_name" type="text" id="first_name" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="email_address" class="block text-sm font-medium text-gray-700">Email</label>
                          <input name="email_address" type="text" id="email_address" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="phone_number" class="block text-sm font-medium text-gray-700">Téléphone</label>
                          <input name="phone_number" type="text" id="phone_number" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6">
                          <label for="street_address" class="block text-sm font-medium text-gray-700">Adresse</label>
                          <input name="street_address" type="text" id="street_address" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                          <label for="city" class="block text-sm font-medium text-gray-700">Ville</label>
                          <input name="city" type="text" id="city" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                          <label for="postal_code" class="block text-sm font-medium text-gray-700">Code Postal</label>
                          <input name="postal_code" type="text" id="postal_code" class="mt-1 block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                          <div class=" flex items-start">
                            <div class="flex items-center h-5">
                              <input name="absence" id="absence" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                            </div>
                            <div class="ml-3 text-sm">
                              <label for="absence" class="font-medium text-gray-700">Absence</label>
                              <p class="text-gray-500">Étre alerté en cas d'absence des enfants</p>
                            </div>
                          </div>

                          <div class="flex items-start" id="boxes">
                            <div class="flex items-center h-5">
                              <input name="rules" id="rules" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                            </div>
                            <div class="ml-3 text-sm">
                              <label for="rules" class="font-medium text-gray-700">Réglement</label>
                              <p class="text-gray-500">Réglement lu et signé</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <a href="{{route ('student')}}" type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Annuler
                      </a>
                      <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enregistrer
                      </button>
                      <button type="button" x-on:click="submitform_and_addstudent()" id="submit_and_new_student" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enregistrer et ajouter un élève
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
          <script type="text/javascript">
          function load(){
            return {
              submitform_and_addstudent()
              {
                document.getElementById('boxes').insertAdjacentHTML('afterend','<input type="hidden" name="add_student_next" value="1">')
                document.getElementById('add_familly_form').submit()
              }
            }
          }
          </script>
            @if ($errors->any())
              @foreach ($errors->all() as $error)
                <jet-action-message>
                  {{ $error }}
                </jet-action-message>
              @endforeach
              </div>
            @endif
</x-app-layout>
