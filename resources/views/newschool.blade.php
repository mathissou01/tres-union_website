<x-app-layout>
    <x-slot name="header">
        <h2 class="leading-tight text-base">
            <a href="{{ route("school") }}" class="text-blue-700">Établissements </a>/ Ajouter un nouvel établissement
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
              <div class="mt-5 md:mt-0 md:col-span-2">
                <form action="{{ route('addschool') }}" method="POST">
                @csrf
                  <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                      <div class="grid grid-cols-6 gap-6">
                        <div class="col-span-6 sm:col-span-3">
                          <label for="name" class="block text-sm font-medium text-gray-700">Nom</label>
                          <input type="text" name="name" id="name" class="mt-1  block w-full sm:text-sm form-input rounded-md">
                        </div>

                        <div class="col-span-6 sm:col-span-3">
                          <label for="city" class="block text-sm font-medium text-gray-700">Commune</label>
                          <input type="text" name="city" id="city" class="mt-1 form-input block w-full sm:text-sm rounded-md">
                        </div>

                      </div>
                    </div>
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                      <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Annuler
                      </button>
                      <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                        Enregistrer
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
</x-app-layout>
