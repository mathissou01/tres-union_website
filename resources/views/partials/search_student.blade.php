@if(Route::is('student*', 'famill*'))
<form action="{{ Route::is('student*') ? route('students.search') : route('famillies.search') }}" class="d-flex mr-3 justify-content-end">
    <div class="form-group col-3 d-flex ">
        <input type="text" name="q" class="form-control" value="{{ request()->q ?? ''  }}" placeholder="Entrer Nom, Prénom ou Tél">
        <button type="submit" class="btn btn-info ml-1"><i class="fas fa-search"></i></button>
    </div>
</form>
@else
    <div class="form-group col-3 d-flex pb-4">
        <input name="q" type="hidden">
    </div>
@endif
<style> 
    input:placeholder-shown { 
  font-style: italic;
  font-weight: 200;
}
</style>

           