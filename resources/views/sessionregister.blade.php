<x-guest-layout>
  <x-slot name="header">
  </x-slot>
  <!-- This example requires Tailwind CSS v2.0+ -->
<div class="bg-indigo-600">
  <div class="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-8">
    <div class="flex items-center justify-between flex-wrap">
      <div class="w-0 flex-1 flex items-center">
        <span class="flex p-2 rounded-lg bg-indigo-800">
          <!-- Heroicon name: outline/speakerphone -->
          <svg class="h-6 w-6 text-white" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z" />
          </svg>
        </span>
        <p class="ml-3 mt-3 font-medium text-white text-xl truncate pb-1">
            Inscription au stage du <u>{{date('d/m/Y', strtotime($courseSession->starting))}} - {{date('d/m/Y', strtotime($courseSession->endding))}}</u> à <u>{{$courseSession->school->name}}, {{$courseSession->school->city}}</u>
        </p>
      </div>
    </div>
  </div>
</div>

<form action="{{ route('applyregister') }}" method="post">
  @csrf
  <input type="hidden" id="id" name="sessionid" value="{{ $id }}"> 
<div class="mt-3" id="info">
  <div class="md:grid md:grid-cols-3 md:gap-6">
    <div class="md:col-span-1">
      <div id="formdesc" class="px-4 sm:px-0">
        <div class="buble">
        <h3 class="text-lg font-medium leading-6 text-gray-900"><u>Informations personelles :</u></h3>
        <p class="mt-1 text-sm text-gray-600 pb-3">
          Ces informations permettent votre inscription aux différents modules proposés lors du stage d'aide au devoir, merci de compléter le formulaire avec des informations véridiques afin de pourvoir contacter les bonnes personnes en cas de problèmes.
        </p>
        </div>
       
      </div>

      <div class="spring" id="spring" role="img" aria-label="Animated cartoon showing a sunny scene with a field with flowers and a flying bee.">
        
      </div>
      <style>
        .buble{
          border: thick double #2b28dbc9;
          padding: 8px;
        }
        .spring  {
  --flower1: #dad;
  --flower2: #8af;
  --flower3: #fbd;
  width: 500px;
  aspect-ratio: 1;
  position: absolute;
  top: 60%;
  left: 16%;
  transform: translate(-50%, -50%);
  background:
    /* flower 1 */
    radial-gradient(circle at 20% 60%, gold 1%, #0000 0),
    radial-gradient(200% 50% at 18% 60%, var(--flower1) 1%, #0000 0),
    radial-gradient(100% 150% at 20% 58%, var(--flower1) 1%, #0000 0),
    radial-gradient(circle at 18.5% 59%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 18.5% 61.5%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 21% 61.5%, var(--flower1) 1.75%, #0000 0),
    radial-gradient(circle at 21.5% 59%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(farthest-side at 100% 60%, #0000 90%, green 0 99.9%, #0000 0) 17.5% 80% / 5% 25% no-repeat,
    radial-gradient(farthest-side at 50% 100%, green 99.9%, #0000 0) 18.5% 70% / 8% 1.5% no-repeat,
    radial-gradient(farthest-side at 50% 20%, green 99.9%, #0000 0) 10% 75% / 8% 1.5% no-repeat,
    /* flower 2 */
    radial-gradient(circle at 25% 70%, gold 1%, #0000 0),
    radial-gradient(340% 150% at 25% 70.5%, var(--flower1) 1%, #0000 0),
    radial-gradient(100% 150% at 25% 68%, var(--flower1) 1%, #0000 0),
    radial-gradient(circle at 23.5% 69%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 23.5% 71.5%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 26% 71.5%, var(--flower1) 1.75%, #0000 0),
    radial-gradient(circle at 26.5% 69%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(farthest-side at 0% 75%, #0000 80%, green 0 99.9%, #0000 0) 25.5% 82% / 2% 15% no-repeat,
    /* flower 3 */
    radial-gradient(circle at 30% 50%, gold 1%, #0000 0),
    radial-gradient(340% 150% at 30% 50.5%, var(--flower3) 1%, #0000 0),
    radial-gradient(100% 150% at 30% 48%, var(--flower3) 1%, #0000 0),
    radial-gradient(circle at 28.5% 49%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(circle at 28.5% 51.5%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(circle at 31% 51.5%, var(--flower3) 1.75%, #0000 0),
    radial-gradient(circle at 31.5% 49%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(farthest-side at 0% 55%, #0000 88%, green 0 99.9%, #0000 0) 30.5% 77% / 4% 35% no-repeat,
    radial-gradient(farthest-side at 50% 0%, green 99.9%, #0000 0) 28.5% 66% / 6% 1.25% no-repeat,
    /* flower 4 */
    radial-gradient(circle at 38% 58%, gold 1%, #0000 0),
    radial-gradient(350% 180% at 38% 57.75%, var(--flower2) 1%, #0000 0),
    radial-gradient(circle at 37.5% 55.5%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(circle at 37% 60%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(circle at 39.25% 60%, var(--flower2) 1.75%, #0000 0),
    radial-gradient(circle at 39.75% 55.75%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(farthest-side at 100% 70%, #0000 75%, green 0 99.9%, #0000 0) 37.75% 78.5% / 2% 30% no-repeat,
    /* flower 5 */
    radial-gradient(circle at 45% 65%, gold 1%, #0000 0),
    radial-gradient(350% 200% at 45% 65.5%, var(--flower1) 1%, #0000 0),
    radial-gradient(100% 150% at 45% 63%, var(--flower1) 1%, #0000 0),
    radial-gradient(circle at 43.25% 63.5%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 44% 67%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 46.25% 66.25%, var(--flower1) 1.75%, #0000 0),
    radial-gradient(circle at 46.75% 63.75%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(farthest-side at 100% 70%, #0000 75%, green 0 99.9%, #0000 0) 44.5% 80% / 2% 20% no-repeat,
    /* flower 6 */
    radial-gradient(circle at 50% 71%, gold 1%, #0000 0),
    radial-gradient(350% 130% at 50% 71.5%, var(--flower2) 1%, #0000 0),
    radial-gradient(100% 150% at 50% 69%, var(--flower2) 1%, #0000 0),
    radial-gradient(circle at 48.25% 69.5%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(circle at 49% 73%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(circle at 51.25% 72.75%, var(--flower2) 1.75%, #0000 0),
    radial-gradient(circle at 51.75% 69.75%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(farthest-side at 20% 68%, #0000 72%, green 74% 98%, #0000 100%) 50.5% 83% / 2% 14% no-repeat,
    /* flower 7 */
    radial-gradient(circle at 55% 63%, gold 1%, #0000 0),
    radial-gradient(350% 130% at 55% 63.5%, var(--flower1) 1%, #0000 0),
    radial-gradient(circle at 54.25% 61.5%, var(--flower1) 2%, #0000 0),
    radial-gradient(circle at 54% 65%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 56.25% 64.75%, var(--flower1) 1.75%, #0000 0),
    radial-gradient(circle at 56.75% 61.75%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(farthest-side at 20% 70%, #0000 72%, green 74% 98%, #0000 100%) 55.5% 80% / 2% 24% no-repeat,
    /* flower 8 */
    radial-gradient(circle at 62% 68%, gold 1%, #0000 0),
    radial-gradient(330% 170% at 62% 68.5%, var(--flower3) 1%, #0000 0),
    radial-gradient(circle at 60.5% 66.5%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(circle at 63% 66.75%, var(--flower3) 2%, #0000 0),
    radial-gradient(circle at 61% 70%, var(--flower3) 1.75%, #0000 0),
    radial-gradient(circle at 63% 69.75%, var(--flower3) 1.75%, #0000 0),
    radial-gradient(farthest-side at 0% 60%, #0000 75%, green 76% 97%, #0000 0) 63% 81% / 2% 18% no-repeat,
    radial-gradient(farthest-side at 50% 100%, #7c8 99.9%, #0000 0) 59.5% 74.5% / 6% 0.75% no-repeat,
    radial-gradient(farthest-side at 50% 100%, green 99.9%, #0000 0) 59.5% 74% / 6% 1.25% no-repeat,
    radial-gradient(farthest-side at 50% 20%, green 99.9%, #0000 0) 48.5% 80% / 5% 1% no-repeat,
    /* flower 9 */
    radial-gradient(circle at 68% 53%, gold 1%, #0000 0),
    radial-gradient(330% 170% at 68% 53.5%, var(--flower1) 1%, #0000 0),
    radial-gradient(110% 140% at 68% 51%, var(--flower1) 1%, #0000 0),
    radial-gradient(circle at 66.25% 51.5%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 67% 55%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(circle at 69% 54.75%, var(--flower1) 1.75%, #0000 0),
    radial-gradient(circle at 69.25% 51.75%, var(--flower1) 1.5%, #0000 0),
    radial-gradient(farthest-side at 10% 60%, #0000 75%, green 76% 97%, #0000 0) 69% 78% / 3% 35.5% no-repeat,
    
    /* flower 10 */
    radial-gradient(circle at 75% 73%, gold 1%, #0000 0),
    radial-gradient(330% 130% at 75% 73.5%, var(--flower3) 1%, #0000 0),
    radial-gradient(110% 140% at 75% 71%, var(--flower3) 1%, #0000 0),
    radial-gradient(circle at 73.25% 71.5%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(circle at 74% 75%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(circle at 76% 74.75%, var(--flower3) 1.75%, #0000 0),
    radial-gradient(circle at 76.25% 71.75%, var(--flower3) 1.5%, #0000 0),
    radial-gradient(farthest-side at 10% 60%, #0000 75%, green 76% 97%, #0000 0) 76% 84% / 2% 12% no-repeat,
    radial-gradient(farthest-side at 50% 100%, green 99.9%, #0000 0) 74.75% 64% / 7% 1.25% no-repeat,
    /* flower 11 */
    radial-gradient(circle at 83% 62%, gold 1%, #0000 0),
    radial-gradient(330% 170% at 83% 62.5%, var(--flower2) 1%, #0000 0),
    radial-gradient(circle at 81.5% 60.5%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(circle at 82% 64%, var(--flower2) 1.5%, #0000 0),
    radial-gradient(circle at 84% 63.75%, var(--flower2) 1.75%, #0000 0),
    radial-gradient(circle at 84% 60.5%, var(--flower2) 1.65%, #0000 0),
    radial-gradient(farthest-side at 10% 60%, #0000 75%, green 76% 97%, #0000 0) 84% 81.5% / 3% 25.5% no-repeat,
    /* ground */
    radial-gradient(100% 20% at 50% 90%, #7c8 45%, #0000 45.25%),
    radial-gradient(100% 20% at 50% 83%, #7c8 49%, #0000 49.25%),
    /* sun */
    radial-gradient(circle at 50% 0%, #0000 20%, #d90c 0 33%, #0000 0) 34.33% 30% / 5% 5% no-repeat,
    radial-gradient(circle at 32% 26%, #d90c 1%, #0000 0),
    radial-gradient(circle at 38% 26%, #d90c 1%, #0000 0),
    radial-gradient(circle at 35% 25%, #fe0 12%, #fd0c 0, #0000 25%),
    /* cloud */
    radial-gradient(circle at 70% 68%, #fff 10%, #0000 10.25%),
    radial-gradient(circle at 66% 70%, #fff 13%, #0000 13.25%),
    radial-gradient(circle at 76% 75%, #fff 8%, #0000 8.25%),
    radial-gradient(circle at 26% 78%, #fff 8%, #0000 8.25%),
    radial-gradient(circle at 37% 70%, #fff 9%, #0000 9.25%),
    radial-gradient(circle at 50% 60%, #f0f8ff 40%, #0000 20.25%),
    radial-gradient(circle at 30% 70%, #f0f8ff 12%, #0000 12.25%),
    radial-gradient(circle at 33% 53%, #f0f8ff 7%, #0000 6.25%),
    radial-gradient(circle at 16% 73%, #f0f8ff 8%, #0000 8.25%),
    radial-gradient(circle at 76% 73%, #f0f8ff 14%, #0000 14.25%),
    radial-gradient(circle at 68% 58%, #f0f8ff 8%, #0000 8.25%),
    radial-gradient(circle at 66% 80%, #f0f8ff 17%, #0000 17.25%),
    /* bg */
    linear-gradient(rgba(221, 238, 255, 0) 0 0)
    ;
  
}

@keyframes fly {
  0%  { left: 60%; transform: rotateY(0); }
  50% { left: 30%; transform: rotateY(0); }
  51% { left: 30%; transform: rotateY(180deg); }
  99% { left: 60%; transform: rotateY(180deg); }
  100% { left: 60%; transform: rotateY(0); }
}

.spring::before {
  content: "";
  animation: fly 5s infinite;
  position: absolute;
  top: 48%;
  left: 60%;
  width: 8%;
  height: 6%;
  z-index: 10;
  border-radius: 50%;
  background: 
    radial-gradient(circle at 26% 50%, #000 7%, #0000 0),
    radial-gradient(circle at 10% 50%, #000 5%, #0000 0),
    radial-gradient(circle at 50% 0%, #0000 20%, #000 0 60%, #0000 0) 15% 70% / 10% 15% no-repeat,
    radial-gradient(100% 200% at 0 50%, #0000 40%, #000 0 53%, #0000 0 66%, #000 0 80%, #0000 0 90%, #000 0);
  background-color: gold;
  box-shadow: 0 -20px 0 -5px #fffa, 10px -20px 0 -5px #fff, 17px 0 0 -13.5px;
}

@media only screen and (max-width: 600px) {
  #spring{
    display: none;
  }
}
        </style>
    </div>
    <div class="mt-5 md:mt-0 md:col-span-2">
        <div class="shadow overflow-hidden sm:rounded-md">
          <div class="px-4 py-5 bg-white sm:p-6">
            <div class="grid grid-cols-6 gap-6">
              <div class="col-span-6 sm:col-span-3">
                <label for="student_lastname" class="block text-sm font-medium text-gray-700">Nom de l'étudiant</label>
                <input type="text" name="student_lastname" id="student_lastname" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="student_firstname" class="block text-sm font-medium text-gray-700">Prénom de l'étudiant</label>
                <input type="text" name="student_firstname" id="student_firstname" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="student_email" class="block text-sm font-medium text-gray-700">Adresse Email de l'étudiant</label>
                <input type="text" name="student_email" id="student_email" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="student_phone" class="block text-sm font-medium text-gray-700">Téléphone de l'étudiant</label>
                <input type="text" name="student_phone" id="student_phone" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="student_school" class="block text-sm font-medium text-gray-700">Établissement scolaire de l'étudiant</label>
                <input type="text" name="student_school" id="student_school" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="level" class="block text-sm font-medium text-gray-700">Niveau scolaire</label>
                <select id="level" name="level" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                  @foreach ($levels as $level)
                    <option value={{ $level->id }}>{{ $level->name }}</option>
                  @endforeach
                </select>
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="lastname" class="block text-sm font-medium text-gray-700">Nom du responsable légal</label>
                <input type="text" name="lastname" id="lastname" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="firstname" class="block text-sm font-medium text-gray-700">Prénom du responsable légal</label>
                <input type="text" name="firstname" id="firstname" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="email" class="block text-sm font-medium text-gray-700">Adresse Email du responsable légal</label>
                <input type="text" name="email" id="email" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3">
                <label for="phone" class="block text-sm font-medium text-gray-700">Téléphone du responsable légal</label>
                <input type="text" name="phone" id="phone" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6">
                <label for="street" class="block text-sm font-medium text-gray-700">Rue</label>
                <input type="text" name="street" id="street" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                <label for="postal_code" class="block text-sm font-medium text-gray-700">Code Postal</label>
                <input type="text" name="postal_code" id="postal_code" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>

              <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                <label for="city" class="block text-sm font-medium text-gray-700">Ville</label>
                <input type="text" name="city" id="city" class="mt-1  block w-full sm:text-sm form-input rounded-md">
              </div>
              
            </div>
          </div>
          <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
            <div class="relative">
           <a href="https://tresunion.fr/" target="_blank"><img src="{{ asset('storage/images/logotext.svg') }}" class="position-absolute translate-middle" height="80" width="80">
            </div>
      
            <button id="next" type="button" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"></a>
              Suivant
            </button>
          </div>
        </div>
    </div>
  </div>
</div>

<div class="mt-3" id="planning" style="display: none">
  <div id="coursedesc" class="text-center text-base">Choisissez les cours auxquels vous souhaitez vous inscrire</div>
  <div class="md:grid md:grid-cols-3 md:gap-6">
    <div class="mt-5 md:mt-0 md:col-span-3">
        <div class="shadow overflow-hidden sm:rounded-md">
          <div class="px-4 py-5 bg-white sm:p-6">
              <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                <div class="container-fluid">
                  <header>
                    <div class="row d-none d-sm-flex p-1 bg-dark text-white">
                      @if (sizeof($dates)<7)
                        @foreach ($dates as $date)
                        <h5 class="col-sm p-1 text-center">{{$date->translatedFormat('l')}}</h5>
                        @endforeach  
                      @else
                        @for ($i = 0; $i < 7; $i++)
                        <h5 class="col-sm p-1 text-center">{{$dates[$i]->translatedFormat('l')}}</h5> 
                        @endfor
                      @endif
                       
                    </div>
                  </header>
                  <div id='agenda' class="row border border-right-0 border-bottom-0">
                    @if (sizeof($dates)<7)
                      @foreach ($dates as $key=>$date)
                      <div id="{{$date->format('d-m')}}" class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                        <h5 class="row align-items-center">
                          <span class="date col-1">{{$date->format('d/m')}}</span>
                          <small class="col d-sm-none text-center  text-gray-900">{{$date->translatedFormat('l')}}</small>
                          <span class="col-1"></span>
                        </h5>
                        <div>Matin</div>
                        <div class="afternoon">Après-Midi</div>
                      </div>
                      @if (($key+1)%7==0)
                        <div class="w-100"></div>
                      @endif
                      @endforeach
                    @else
                      @for ($i = 0; $i < sizeof($dates)+(7-sizeof($dates)%7); $i++)
                        @if ($i<sizeof($dates))
                        <div id="{{$dates[$i]->format('d-m')}}" class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                          <h5 class="row align-items-center">
                            <span class="date col-1">{{$dates[$i]->format('d/m')}}</span>
                            <small class="col d-sm-none text-center  text-gray-900">{{$dates[$i]->translatedFormat('l')}}</small>
                            <span class="col-1"></span>
                          </h5>
                          <div>Matin</div>
                          <div class="afternoon">Après-Midi</div>
                        </div>
                        @else
                        <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-light text-muted">
                        </div>   
                        @endif
                        @if (($i+1)%7==0)
                        <div class="w-100"></div>
                        @endif 
                      @endfor
                    @endif
                  </div>
                </div>
              </div>
          </div>
          <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
            <button type="button" id="previous" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Précédent
            </button>
            <button type="submit" id="validregister" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Enregistrer et finaliser mon inscription
            </button>
          </div>
        </div>
    </div>
  </div>
</div>
</form>
@csrf
<script>
  $("#next").click(function(event){
    $('#planning').show();
    $('#info').hide();
  })

  $("#previous").click(function(event){
    $('#planning').hide();
    $('#info').show();
  })

  $('#level').on('change', function(event){
    id = $(this).data('id');
    let children = $('#agenda').children()
    for (let i = 0; i < children.length; i++) {
      $noRemove = children.eq(i).children("h5")
      children.eq(i).empty()
      children.eq(i).append($noRemove)
      children.eq(i).append('<div>Matin</div>')
      children.eq(i).append('<div class="afternoon">Après-Midi</div>')
    }
    
    $.ajax({
      type: 'POST',
      url: "{{ route('lessonbylevel') }}",
      data: {
        '_token': $("input[name=_token]").val(),
        'level': $( "#level" ).val(),
        'session': "{{ $id }}"
      },
      success: function(data) {
        if(data.length != 0){
          if ($('#noclass').length) {
            $('#noclass').remove()
            $('#noclass1').remove()
            $('#validregister').prop('disabled',false)
            $('#validregister').addClass('bg-indigo-600')
            $('#validregister').addClass('hover:bg-indigo-700')
            $('#validregister').removeClass('bg-gray-400')
          }
          $.each(data, function(index, element) {
            let subject = {
              @foreach ($subjects as $subject)
                {{ $subject->id }}: "{{ $subject->name }}",
              @endforeach
              }
            let level = {
            @foreach ($levels as $level)
              {{ $level->id }}: "{{ $level->name }}",
            @endforeach
            }
            let date = element.date.substring(element.date.length-2,element.date.length)+'-'+element.date.substring(element.date.length-5,element.date.length-3)
            let time = element.starting.substring(0,2)
            let children = $('#agenda').children()
            for (let i = 0; i < children.length; i++) {
                let currentChild = children.eq(i);
                if (currentChild.attr('id') === date) {
                  console.log(date)
                  console.log(time);
                  console.log(Number(time));
                  if (Number(time) < 12) {
                    currentChild.children().eq(1).after('<div class="event d-block p-1 pl-2 pr-2 mb-1 rounded small text-sm bg-primary text-white" title="&#128540;" data-toggle="modal" data-target="#infoModal'+element.id+'">'+element.starting.substring(0,5)+'-'+element.endding.substring(0,5)+'<br><input type="checkbox" name="lessons[]" class="mr-3" value="'+element.id+'"> <label class="h6 text-wrap"> '+subject[element.subject_id]+' - '+level[element.level_id]+'</label><br>'+element.room+'</div>')
                  }
                  else {
                    currentChild.children('.afternoon').after('<div class="event d-block p-1 pl-2 pr-2 mb-1 rounded small bg-secondary text-white" title="&#128540;" data-toggle="modal" data-target="#infoModal'+element.id+'">'+element.starting.substring(0,5)+'-'+element.endding.substring(0,5)+'<br><input type="checkbox"  class="mr-3" name="lessons[]" value="'+element.id+'"> <label class="h6 text-wrap"> '+subject[element.subject_id]+' - '+level[element.level_id]+'</label><br>'+element.room+'</div>')
                  }
                }
            }
          }); 
        }
        else{
          if (!$('#noclass').length) {
            $('#coursedesc').after('<div id="noclass" class="alert alert-danger" role="alert">\
                                  Il n\'y a plus de place disponible dans les cours correspondants au niveau scolaire séléctionné, attendez le prochain stage !\
                                </div>')
            $('#formdesc').after('<div id="noclass1" class="alert alert-danger" role="alert">\
                                  Il n\'y a plus de place disponible dans les cours correspondants au niveau scolaire séléctionné, attendez le prochain stage !\
                                </div>')
            $('#validregister').prop('disabled',true)
            $('#validregister').removeClass('bg-indigo-600')
            $('#validregister').removeClass('hover:bg-indigo-700')
            $('#validregister').addClass('bg-gray-400')
          }
          
        }
      },
    });
  });

  id = $(this).data('id');
    let children = $('#agenda').children()
    for (let i = 0; i < children.length; i++) {
      $noRemove = children.eq(i).children("h5")
      children.eq(i).empty()
      children.eq(i).append($noRemove)
      children.eq(i).append('<div>Matin</div>')
      children.eq(i).append('<div class="afternoon">Après-Midi</div>')
    }
    
    $.ajax({
      type: 'POST',
      url: "{{ route('lessonbylevel') }}",
      data: {
        '_token': $("input[name=_token]").val(),
        'level': $( "#level" ).val(),
        'session': "{{ $id }}"
      },
      success: function(data) {
        if(data.length != 0){
          $.each(data, function(index, element) {
            let subject = {
              @foreach ($subjects as $subject)
                {{ $subject->id }}: "{{ $subject->name }}",
              @endforeach
              }
            let level = {
            @foreach ($levels as $level)
              {{ $level->id }}: "{{ $level->name }}",
            @endforeach
            }
            let date = element.date.substring(element.date.length-2,element.date.length)+'-'+element.date.substring(element.date.length-5,element.date.length-3)
            let time = element.starting.substring(0,2)
            let children = $('#agenda').children()
            for (let i = 0; i < children.length; i++) {
                let currentChild = children.eq(i);
                if (currentChild.attr('id') === date) {
                  if (Number(time) < 12) {
                    currentChild.children().eq(1).after('<div class="event d-block p-1 pl-2 pr-2 mb-1 rounded small text-sm bg-primary text-white" title="&#128540;" data-toggle="modal" data-target="#infoModal'+element.id+'">'+element.starting.substring(0,5)+'-'+element.endding.substring(0,5)+'<br><input type="checkbox" name="lessons[]" class="mr-3" value="'+element.id+'"> <label class="h6 text-wrap"> '+subject[element.subject_id]+' - '+level[element.level_id]+'</label><br>'+element.room+'</div>')
                  }
                  else {
                    currentChild.children('.afternoon').after('<div class="event d-block p-1 pl-2 pr-2 mb-1 rounded small bg-secondary text-white" title="&#128540;" data-toggle="modal" data-target="#infoModal'+element.id+'">'+element.starting.substring(0,5)+'-'+element.endding.substring(0,5)+'<br><input type="checkbox"  class="mr-3" name="lessons[]" value="'+element.id+'"> <label class="h6 text-wrap"> '+subject[element.subject_id]+' - '+level[element.level_id]+'</label><br>'+element.room+'</div>')
                  }
                }
            }
          }); 
        }
        else{
          $('#coursedesc').after('<div id="noclass" class="alert alert-danger" role="alert">\
                                  Il n\'y a plus de place disponible dans les cours correspondants au niveau scolaire séléctionné, attendez le prochain stage !\
                                </div>')
          $('#formdesc').after('<div id="noclass1" class="alert alert-danger" role="alert">\
                                  Il n\'y a plus de place disponible dans les cours correspondants au niveau scolaire séléctionné, attendez le prochain stage !\
                                </div>')
          $('#validregister').prop('disabled',true)
          $('#validregister').removeClass('bg-indigo-600')
          $('#validregister').removeClass('hover:bg-indigo-700')
          $('#validregister').addClass('bg-gray-400')
        }
      },
    });
</script>

</x-guest-layout>
