<x-app-layout>
    <x-slot name="header">
      <h2 class="leading-tight text-base">
        <a href="{{ route("session") }}" class="text-blue-700">Stage </a>/<a href="{{ route("showsession",['id'=>$session->id]) }}" class="text-blue-700"> Stage du {{date('d/m/Y', strtotime($session->starting))}} au {{date('d/m/Y', strtotime($session->endding))}} </a> / Liste des inscrits
      </h2>
    </x-slot>
    
    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <span>
          <a href="{{ route("pdfsessionlist",['id'=>$session->id])}}">
            <button type="button" class="inline-flex items-center mb-3 mt-3 px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Télécharger la liste en PDF
            </button>
            <a href="{{ route("csvsessionlist",['id'=>$session->id])}}">
            <button type="button" class="inline-flex items-center mb-3 mt-3 px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
              Télécharger la liste en Excel
            </button>
          </a>
        </span>
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
          <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Nom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Classe
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Établissement d'origine
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Téléphone Responsable Légal
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                      </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                      @foreach ($session->students as $student)
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">
                          <div class="flex items-center">
                            <div class="ml-4">
                              <div class="text-sm font-medium text-gray-900">
                                {{ $student->student_firstname }} {{ $student->student_lastname }}
                              </div>
                              <div class="text-sm text-gray-500">
                                {{ $student->student_email}}
                              </div>
                            </div>
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                          <div class="text-sm text-gray-900">{{ $student->level->name }}</div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                          {{ $student->school }}
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          <div class="text-sm font-medium text-gray-900">
                            {{ $student->legal_phone }}
                          </div>
                          <div class="text-sm text-gray-500">
                            {{ $student->legal_email}}
                          </div>
                        </td>
                        
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                          <a href="{{ route("sessionstudent",['id'=>$student->id]) }}" class="text-indigo-600 hover:text-indigo-900">Voir</a>
                        </td>
                      </tr>
                      @endforeach

                      <!-- More rows... -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>

        </div>
        </div>
    </div>
</x-app-layout>
