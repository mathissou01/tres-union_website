<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<style>
  table {
  table-layout: fixed;
  }
  #page{
    page-break-inside: avoid !important;
  }
  table th:nth-child(1) { width:3cm; word-wrap:break-word;}
  table th:nth-child(2) { width:3cm; word-wrap:break-word;}
  table th:nth-child(3) { width:3cm; word-wrap:break-word;}
  table th:nth-child(4) { width:4cm; word-wrap:break-word;}
  table th:nth-child(5) { width:4cm; word-wrap:break-word;}
  table th:nth-child(5) { width:1cm; word-wrap:break-word;}
  table td:nth-child(1) { width:3cm; word-wrap:break-word;}
  table td:nth-child(2) { width:3cm; word-wrap:break-word;}
  table td:nth-child(3) { width:3cm; word-wrap:break-word;}
  table td:nth-child(4) { width:4cm; word-wrap:break-word;}
  table td:nth-child(5) { width:4cm; word-wrap:break-word;}
  table td:nth-child(5) { width:1cm; word-wrap:break-word;}
</style>
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <div id="page" class="py-6" style="width:18cm">
      <div class="text-center" style="font-weight: bold; font-size: 17px; margin: 3px 0 3px 0;">Élèves inscrit au stage du {{date('d/m/Y', strtotime($session->starting))}} au {{date('d/m/Y', strtotime($session->endding))}}</div>
      <table class="table table-bordered">
        <thead>
          <tr class="divide-x divide-gray-400">
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Nom
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Prénom
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Classe
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Établissement d'origine
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Responsable Légal
            </th>
            <th scope="col" class="px-2 py-1 bg-gray-200 text-left text-sm font-bold text-gray-900 uppercase">
              Contacts Responsable Légal
            </th>
          </tr>
        </thead>
        <tbody class="divide-y divide-gray-400">
          @foreach ($session->students as $student)
          <tr class="divide-x divide-gray-400">
            <td class="px-2 py-1 ">
                    {{ $student->student_lastname }}
            </td>
            <td class="px-2 py-1 ">
                    {{ $student->student_firstname }} 
            </td>
            
            <td class="px-2 py-1 ">
              {{ $student->level->name }}
            </td>
            <td class="px-2 py-1 ">
              {{ $student->school }}
            </td>
            <td class="px-2 py-1 ">
                    {{ $student->legal_lastname }} {{ $student->legal_firstname }} 
            </td>
            <td class="px-2 py-1 text-sm text-gray-500">
                {{ $student->legal_phone }}
            </td>
          </tr>
          @endforeach

          <!-- More rows... -->
        </tbody>
      </table>
    </div>
<style>
  table {
    border: 2px solid rgba(0, 42, 255, 0.452);
    background-image: url("https://i.ibb.co/3dzzsk3/logo-1.png"); 
    background-size: 350px 150px;
    background-repeat: repeat;
  opacity: 0.1; 
  }
</style>