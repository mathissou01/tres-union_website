<x-app-layout>
  <x-slot name="header">
        <x-jet-dropdown>   
          <x-slot name="trigger">  
            <button class="flex items-center hover:text-gray-700 hover:border-gray-300 focus:outline-none focus:text-gray-700 focus:border-gray-300 transition duration-150 ease-in-out">
              <img src="https://img.icons8.com/external-kiranshastry-lineal-color-kiranshastry/40/000000/external-student-online-learning-kiranshastry-lineal-color-kiranshastry-1.png"/>
                                          <style>
                                        img {
                                          margin-right: 10px;
                                            position: sticky  ;
                                            margin-bottom: 5px;
                                        }
                                      </style>
              <div>Étudiants</div>
              <div class="ml-1">
                <svg class="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
                  <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                </svg>
              </div>
            </button>     
          </x-slot>
          <x-slot name="content">
              <x-jet-dropdown-link href="{{ route('newstudent') }}" class="bg-white-200">
                  Ajouter un étudiant Post-BAC
              </x-jet-dropdown-link>
              <x-jet-dropdown-link href="{{ route('csvstudentlist')}}" class="bg-blue-200">
                  Télécharger la liste des étudiants Post-BAC
              </x-jet-dropdown-link>
              <x-jet-dropdown-link href="{{ route('csvminorstudentlist')}}" class="bg-blue-200">
                  Télécharger la liste des étudiants Pré-BAC et leur familles
              </x-jet-dropdown-link>
              <div class="border-t border-gray-100 "></div>
          </x-slot>
        </x-jet-dropdown>
  </x-slot>
    <div class="py-12">
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">  
          <div class="flex flex-col">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">   
              <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                  <table class="min-w-full divide-y divide-gray-200" id="table"> 
                        <!-- Premier tableau des étudiant majeurs. Affichage de chaque collumns -->                                  
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" data-sortable="true">
                          Nom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" data-sortable="true">
                          Prénom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" data-sortable="true">
                          Téléphone
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" data-sortable="true">
                          Étude
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider" data-sortable="true">
                          État 
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 d-flex justify-content-end ">
                          <span class="badge badge-secondary display-4 d-flex justify-content-end">Etudiant Post-Bac</span>
                        </th>    
                      </tr>
                    </thead>
                   <tbody class="bg-white divide-y divide-gray-200">
                     <!-- Premier tableau des étudiant majeurs. Affichage des valeurs -->  
                    @foreach ($students as $student) 
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">                
                              <div class="text-sm font-medium text-gray-900">
                                {{ $student->last_name }} 
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">
                              {{ $student->first_name }} 
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-500">
                              {{ $student->phone_number }}
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">
                              {{ $student->level }}
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                           <!-- Boucle pour changé lacouleur en fonction du type de paiement en cours -->
                            <div class="text-sm font-medium text-gray-900">
                               @if($student->choices === "Paiement en attente")
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-200 text-red-900">
                              {{ $student->choices }}
                               </span>                      
                               @elseif($student->choices === "Paiement effectué")
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-200 text-dark-800">
                              {{ $student->choices }}
                               </span>                                                         
                               @else($student->choices === "Paiement partiel")
                               <span title="{{$student->paiement}} €" class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-orange-200 text-orange-800" id="partiel">
                              {{ $student->choices }}
                               </span> 
                               @endif

                            </div>
                        </td>                       
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium bg-white-50">
                          <a href="/student/mod/{{ $student->id }}" class="text-indigo-600 hover:text-indigo-900 mr-2">Modifier</a>
                            <button name="{{ $student->id }}" class="text-red-600 hover:text-red-900 delete-button" data-toggle="modal" data-target="#confirmDeleteModal1">Supprimer</button>
                              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium bg-blue-50">
                                <a href="{{ route('show_codebar_student',['id'=>$student->id]) }}" type="button" onclick="downloadimage()" class="text-indigo-600 hover:text-indigo-900 mr-5"><i class="fas fa-download"></i></a>  
                                <a href="{{ route('showstudent', ['id'=>$student->id])}}" class="text-indigo-600 hover:text-indigo-900">Voir</a>
                              </td>
                        </td>
                      </tr>
                    @endforeach
                       <!-- Deuxieme tableau = étudiant mineurs. Affichage des collumns -->  
                    </tbody> 
                  </table>
                  <table class="min-w-full divide-y divide-gray-200">
                    <thead>
                      <tr>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Nom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Prénom
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Téléphone
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          Étude
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                          État 
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50">
                          <span class="sr-only">Edit</span>
                        </th>
                        <th scope="col" class="px-6 py-3 bg-gray-50 d-flex justify-content-end ">
                          <span class="badge badge-secondary display-4 d-flex justify-content-end">Etudiant Pré-BAC</span>
                        </th>
                      </tr>
                    </thead>
                           <!-- Deuxieme tableau = étudiant mineurs. Affichage des valeurs -->  
                    <tbody class="bg-white divide-y divide-gray-200">
                    @foreach ($minorstudents as $minorstudent) 
                      <tr>
                        <td class="px-6 py-4 whitespace-nowrap">                
                              <div class="text-sm font-medium text-gray-900">
                                {{ $minorstudent->last_name }} 
                          </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">
                              {{ $minorstudent->first_name }} 
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-500">
                              {{ $minorstudent->phone_number }}
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">
                              {{ $minorstudent->level }}
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm font-medium text-gray-900">
                           <!-- Boucle pour changé lacouleur en fonction du type de paiement en cours -->
                              @if($minorstudent->choices === "Paiement en attente")
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-red-200 text-red-900">
                                  {{ $minorstudent->choices }}
                               </span>                          
                               @elseif($minorstudent->choices === "Paiement effectué")
                               <span class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-200 text-dark-800">
                                  {{ $minorstudent->choices }}
                               </span>                                                           
                               @else($minorstudent->choices === "Paiement partiel")
                               <span title="{{$minorstudent->paiement}} €" class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-orange-200 text-orange-800">
                                  {{ $minorstudent->choices }}
                               </span> 
                               @endif
                            </div>
                        </td>                       
                        <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium bg-white-50">
                          <a href="/minorstudent/mod/{{ $minorstudent->id }}" class="text-indigo-600 hover:text-indigo-900 mr-2">Modifier</a>
                            <button name="{{ $minorstudent->id }}" class="text-red-600 hover:text-red-900 delete-button" data-toggle="modal" data-target="#confirmDeleteModal">Supprimer</button>
                              <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium bg-blue-50">
                                <a href="{{ route('show_codebar_minorstudent',['id'=>$minorstudent->id]) }}" type="button" onclick="downloadimage()" class="text-indigo-600 hover:text-indigo-900 mr-5"><i class="fas fa-download"></i></a> 
                                <a href="{{ route('showonlyminorstudent', ['id'=>$minorstudent->id])}}" class="text-indigo-600 hover:text-indigo-900">Voir</a>                    
                              </td>
                        </td>
                      </tr>
                    @endforeach            
                    </tbody> 
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal qui s'affichage pour confirmer une supression d'étudiant mineur -->
    <div class="modal fade" id="confirmDeleteModal" data-selected-subject="0" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header bg-red-600 ">
            <h5 class="modal-title text-light" id="confirmDeleteModalTitle">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Êtes-vous sur de vouloir supprimer cette étudiant Pré-BAC ? </br>
            Vous supprimerez toutes ses informations !
          </div> 
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="confirmDelete" type="button" class="inline-flex justify-center py-2
             px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700
              focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">Supprimer</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        $(".delete-button").click(function(){
            $("#confirmDeleteModal").attr("data-selected-subject",this.name)
        })
        $("#confirmDelete").click(function(){
            $(location).attr("href", "/minorstudent/del/"+$("#confirmDeleteModal").attr('data-selected-subject'));
        })    
    </script>  
    <!-- Modal qui s'affichage pour confirmer une supression d'étudiant majeur -->
    <div class="modal fade" id="confirmDeleteModal1" data-selected-subject="0" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header bg-red-600 ">
            <h5 class="modal-title text-light" id="confirmDeleteModalTitle">Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            Êtes-vous sur de vouloir supprimer cette étudiant Post-BAC ? </br>
            Vous supprimerez toutes ses informations !
          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="confirmDelete1" type="button" class="inline-flex justify-center py-2
             px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700
              focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500">Supprimer</button>
          </div>
        </div>
      </div>
    </div>
    <script type="text/javascript">
        $(".delete-button").click(function(){
            $("#confirmDeleteModal1").attr("data-selected-subject",this.name)
        })
        $("#confirmDelete1").click(function(){
            $(location).attr("href", "/student/del/"+$("#confirmDeleteModal").attr('data-selected-subject'));
        })    
    </script>      
</x-app-layout>
