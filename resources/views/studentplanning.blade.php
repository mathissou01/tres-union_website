<x-guest-layout>
  <script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    {{-- Script qui screen le code bar --}}
         <script type="text/javascript">
            function downloadimage() {
                /*var container = document.getElementById("image-wrap");*/ /*specific element on page*/
                var documentPadding = document.querySelectorAll(".htcMB");
                var documentMatinPadding = document.querySelectorAll(".htcMB2");
                for (let element = 0; element < documentPadding.length; element++) {
                  var simple = documentPadding[element];
                  simple.classList.toggle("pb-4");
                }
                for (let element = 0; element < documentMatinPadding.length; element++) {
                  var loc = documentMatinPadding[element];
                  loc.classList.toggle("pb-2");
                }

                var container = document.getElementById("cardToDownload");; /* full page */
                html2canvas(container, { scale:2, allowTaint: true, backgroundColor: '#fff' }).then(function (canvas) {

                    var link = document.createElement("a");
                    document.body.appendChild(link);
                    link.download = "Planning_de_{{ $student->student_firstname }}_{{ $student->student_lastname }}.png";
                    link.href = canvas.toDataURL();
                    link.target = '_blank';
                    link.click();
                });

                for (let element = 0; element < documentPadding.length; element++) {
                  var simple = documentPadding[element];
                  simple.classList.toggle("pb-4");
                }
                for (let element = 0; element < documentMatinPadding.length; element++) {
                  var loc = documentMatinPadding[element];
                  loc.classList.toggle("pb-2");
                }
            }
          </script>
  <div class="py-12">
    <div id="cardToDownload">
    <div class="text-center mb-3">
      Bonjour {{ $student->student_firstname }},<br>
      Voici votre planning pour le stage de soutient du {{date('d/m/Y', strtotime($courseSession->starting))}} au {{date('d/m/Y', strtotime($courseSession->endding))}},<br>
      le stage à lieu à l'Établissement : {{$courseSession->school->name}}
    </div>
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 mb-3">
          <div class="bg-white shadow overflow-hidden sm:rounded-lg">
            <div class="container-fluid">
              <header>
                <div class="row d-none d-sm-flex p-1 bg-dark text-white">
                  @if (sizeof($dates)<7)
                    @foreach ($dates as $date)
                    <h5 class="col-sm p-1 text-center">{{$date->translatedFormat('l')}}</h5>
                    @endforeach  
                  @else
                    @for ($i = 0; $i < 7; $i++)
                    <h5 class="col-sm p-1 text-center">{{$dates[$i]->translatedFormat('l')}}</h5> 
                    @endfor
                  @endif
                   
                </div>
              </header>
              <div class="row border border-right-0 border-bottom-0">
                @if (sizeof($dates)<7)
                  @foreach ($dates as $key=>$date)
                  <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                    <h5 class="row align-items-center">
                      <span class="date col-1">{{$date->format('d/m')}}</span>
                      <small class="col d-sm-none text-center  text-gray-900">{{$date->translatedFormat('l')}}</small>
                      <span class="col-1"></span>
                    </h5>
                    <div class="htcMB2">Matin</div>
                    @foreach ($student->lessons as $keylesson => $lesson)
                      @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->lt(Carbon::createFromFormat('H:i', '12:00')))
                      <a class="htcMB event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br> {{ $lesson->room }}</a>
                      @endif                        
                    @endforeach
                    <div class="htcMB2">Après-Midi</div>
                    @foreach ($student->lessons as $keylesson => $lesson)
                      @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->gte(Carbon::createFromFormat('H:i', '12:00')))
                      <a class="htcMB event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br> {{ $lesson->room }}</a>
                      @endif                        
                    @endforeach
                  </div>
                  @if (($key+1)%7==0)
                    <div class="w-100"></div>
                  @endif
                  @endforeach
                @else
                  @for ($i = 0; $i < sizeof($dates)+(7-sizeof($dates)%7); $i++)
                    @if ($i<sizeof($dates))
                    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate ">
                      <h5 class="row align-items-center">
                        <span class="date col-1">{{$dates[$i]->format('d/m')}}</span>
                        <small class="col d-sm-none text-center  text-gray-900">{{$dates[$i]->translatedFormat('l')}}</small>
                        <span class="col-1"></span>
                      </h5>
                      <div class="htcMB2">Matin</div>
                      @foreach ($courseSession->lessons as $keylesson => $lesson)
                        @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->lt(Carbon::createFromFormat('H:i', '12:00')))
                        <a class="htcMB event d-block p-1 pl-2 pr-2 mb-1 rounded text-sm bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br> {{ $lesson->room }}</a>
                        @endif                        
                      @endforeach
                      <div class="htcMB2">Après-Midi</div>
                      @foreach ($courseSession->lessons as $keylesson => $lesson)
                        @if (Carbon::createFromFormat('Y-m-d',$lesson->date)->eq($date) && Carbon::createFromFormat('H:i:s',$lesson->starting)->gte(Carbon::createFromFormat('H:i', '12:00')))
                        <a class="htcMB event d-block p-1 pl-2 pr-2 mb-1 rounded small bg-primary text-white" title="Test Event 2" data-toggle="modal" data-target="#infoModal{{$keylesson}}">{{date('H:i', strtotime($lesson->starting))}} - {{date('H:i', strtotime($lesson->endding))}} <br> <span class="text-lg text-wrap">{{$lesson->subject->name}} - {{$lesson->level->name}}</span><br> {{ $lesson->room }}</a>
                        @endif                        
                      @endforeach
                    </div>
                    @else
                    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-light text-muted">
                    </div>   
                    @endif
                    @if (($i+1)%7==0)
                    <div class="w-100"></div>
                    @endif 
                  @endfor
                @endif
              </div>
            </div>
          </div>
        </div>
             <button onclick="downloadimage()" class="clickbtn">Télécharger votre planning &nbsp<i class="fas fa-download"></i></button>
      </div>
    </div>
  </div>
</x-guest-layout>
<style>
   .clickbtn{
      position: relative;
      left: 50%;
      transform: translateX(calc(-50% - 30px));
      box-sizing: border-box;
      margin: 60px 30px;
      align-items: center;
      appearance: none;
      background-color: #fff;
      border-radius: 24px;
      border-style: none;
      box-shadow: rgba(0, 0, 0, .2) 0 3px 5px -1px,rgba(0, 0, 0, .14) 0 6px 10px 0,rgba(0, 0, 0, .12) 0 1px 18px 0;
      box-sizing: border-box;
      color: #3c4043;
      cursor: pointer;
      display: inline-flex;
      fill: currentcolor;
      font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
      font-weight: 500;
      height: 48px;
      justify-content: center;
      letter-spacing: .25px;
      line-height: normal;
      max-width: 100%;
      overflow: visible;
      padding: 2px 24px;
      position: relative;
      text-align: center;
      text-transform: none;
      font-size: 25px;
      color: rgb(0, 81, 255);
      transition: box-shadow 280ms cubic-bezier(.4, 0, .2, 1),opacity 15ms linear 30ms,transform 270ms cubic-bezier(0, 0, .2, 1) 0ms;
      user-select: none;
      -webkit-user-select: none;
      touch-action: manipulation;
      width: auto;
      will-change: transform,opacity;
      z-index: 0;
    }

.clickbtn:hover {
  background: #00363d18;
  color: #174ea6;
}

.clickbtn:active {
  box-shadow: 0 4px 4px 0 rgb(60 64 67 / 30%), 0 8px 12px 6px rgb(60 64 67 / 15%);
  outline: none;
}

.clickbtn:focus {#4f92ff
  outline: none;
  border: 2px solid #4285f4;
}

.clickbtn:not(:disabled) {
  box-shadow: rgba(51, 66, 78, 0.3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
}

.clickbtn:not(:disabled):hover {
  box-shadow: rgba(60, 64, 67, .3) 0 2px 3px 0, rgba(60, 64, 67, .15) 0 6px 10px 4px;
}

.clickbtn:not(:disabled):focus {
  box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
}

.clickbtn:not(:disabled):active {
  box-shadow: rgba(60, 64, 67, .3) 0 4px 4px 0, rgba(60, 64, 67, .15) 0 8px 12px 6px;
}

.clickbtn:disabled {
  box-shadow: rgba(60, 64, 67, .3) 0 1px 3px 0, rgba(60, 64, 67, .15) 0 4px 8px 3px;
}    
    }
</style>
