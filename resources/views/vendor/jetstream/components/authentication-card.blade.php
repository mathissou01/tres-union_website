{{ Html::style('css/illustration.css') }}
<div class="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100 z-index: 10" id="overflow">
    <div>
        {{ $logo }}
    </div>

    <div class="w-full sm:max-w-md mt-6 px-6 py-4 bg-white shadow-md overflow-hidden sm:rounded-lg" >
        {{ $slot }}
    </div>

        {{--Lama animé sur la page de connexion--}}
<div class="clouds">
  <div></div>
  <div></div>
</div>
<div class="alpaca">
  
    <div class="shadowcircle">
          <svg height="150" width="500">
  <ellipse cx="240" cy="100" rx="220" ry="30" style="fill:#0f0f0f85" />
  <ellipse cx="220" cy="70" rx="190" ry="20" style="fill:#3636368f" />
  <ellipse cx="210" cy="45" rx="170" ry="15" style="fill:#6666667e" />
</svg> 

    </div>
  <div class="alpaca__top flex">
    <div class="head flex">
      <span class="kevin">Kevin</span>
      <div class="head__ears flex">
        <div></div>
        <div></div>
      </div>
      <div class="head__face-neck">
        <div class="face flex">
          <div class="eyes flex">
            <div class="flex"></div>
            <div class="flex"></div>
          </div>
          <div class="cheeks flex">
            <div></div>
            <div></div>
          </div>
          <div class="snout flex">
            <div class="nose"></div>
            <div class="mouth"></div>
          </div>
        </div>
      </div>
    </div>
    <div class="tail"></div>
  </div>
  <div class="alpaca__btm">
    <div class="legs flex">
      <div class="legs__front">
        <div></div>
        <div></div>
      </div>
      <div class="legs__back">
        <div></div>
        <div></div>
      </div>
    </div>
    <div class="body flex">
      <div class="spots"> </div>
    </div>
  </div>
</div>
</div>
  {{--Footer page de connexion--}}

