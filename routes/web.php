<?php

use App\Models\Familly;
use App\Models\Student;
use App\Models\Contributor;
use App\Models\Minorstudent;
use App\Models\CourseSession;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppelController;
use App\Http\Controllers\SchoolController;
use App\Http\Controllers\FamillyController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\ContributorController;
use App\Http\Controllers\CourseSessionController;
use App\Http\Controllers\SessionStudentController;
use App\Http\Controllers\SessionRegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified', 'role'])->get('/register', [RegisterController::class, 'showRegister'])->name('showregister');

Route::middleware(['auth:sanctum', 'verified', 'role'])->post('/applyregister', [RegisterController::class, 'register'])->name('register');


Route::get('/', function () {
    return redirect(route('dashboard'));
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [Controller::class, 'home'])->name('dashboard');

Route::middleware(['auth:sanctum', 'verified', 'role'])->get('/student', [StudentController::class, 'show'])->name('student');

Route::middleware(['auth:sanctum', 'verified'])->get('/student/new', [StudentController::class, 'new_major'])->name('newstudent');

Route::middleware(['auth:sanctum', 'verified'])->post('/student/add', [StudentController::class, 'add_major'])->name('addstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/student/del/{id}', [StudentController::class, 'del'])->name('delstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/student/csv', [StudentController::class, 'csvliststudent'])->name('csvstudentlist');

Route::middleware(['auth:sanctum', 'verified'])->get('/minorstudent/csv', [FamillyController::class, 'csvlistminorstudent'])->name('csvminorstudentlist');

Route::middleware(['auth:sanctum', 'verified'])->post('/student/mod', [StudentController::class, 'mod'])->name('modstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/student/mod/{id}', [StudentController::class, 'showMod'])->name('showmodstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/student/{id}/show', [StudentController::class, 'show_details'])->name('showstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/familly', [FamillyController::class, 'show'])->name('familly');

Route::middleware(['auth:sanctum', 'verified'])->get('/familly/new', [FamillyController::class, 'new'])->name('newfamilly');

Route::middleware(['auth:sanctum', 'verified'])->post('/familly/add', [FamillyController::class, 'add'])->name('addfamilly');

Route::middleware(['auth:sanctum', 'verified'])->get('/familly/del/{id}', [FamillyController::class, 'del'])->name('delfamilly');

Route::middleware(['auth:sanctum', 'verified'])->post('/familly/mod', [FamillyController::class, 'mod'])->name('modfamilly');

Route::middleware(['auth:sanctum', 'verified'])->get('/familly/mod/{id}', [FamillyController::class, 'showMod'])->name('showmodfamilly');

Route::middleware(['auth:sanctum', 'verified'])->get('/familly/{id}/show', [FamillyController::class, 'show_details'])->name('showfamilly');

Route::middleware(['auth:sanctum', 'verified'])->get('/familly/{id}/student/new', [FamillyController::class, 'new_minor'])->name('newchild');

Route::middleware(['auth:sanctum', 'verified'])->post('/familly/{id}/student/add', [FamillyController::class, 'add_minor'])->name('addchild');

Route::middleware(['auth:sanctum', 'verified'])->get('/minorstudent/attestation_download/', [FamillyController::class, 'download_attestation'])->name('download_attestation');

Route::middleware(['auth:sanctum', 'verified'])->get('/minorstudent/{id}/show', [FamillyController::class, 'show_details_minor_student'])->name('showminorstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/minorstudent/{id}/showonly', [FamillyController::class, 'show_details_only_minor_student'])->name('showonlyminorstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/minorstudent/del/{id}', [FamillyController::class, 'del_minor'])->name('del_minorstudent');

Route::middleware(['auth:sanctum', 'verified'])->post('/minorstudent/mod', [FamillyController::class, 'mod_minorstudent'])->name('mod_minorstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/minorstudent/mod/{id}', [FamillyController::class, 'showmod_minorstudent'])->name('showmod_minorstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/session', [CourseSessionController::class, 'show'])->name('session');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/new', [CourseSessionController::class, 'new'])->name('newsession');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/fiche', [CourseSessionController::class, 'voir'])->name('fichesession');

Route::middleware(['auth:sanctum', 'verified'])->post('/session/{id}/voir}', [CourseSessionController::class, 'show_ecole'])->name('showInter');

Route::middleware(['auth:sanctum', 'verified'])->post('/session/add', [CourseSessionController::class, 'add'])->name('addsession');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/show', [CourseSessionController::class, 'show_details'])->name('showsession');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/mailer', [CourseSessionController::class, 'show_mailers'])->name('showmailers');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/ficheDeStage', [CourseSessionController::class, 'ficheDeStage'])->name('ficheStage');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/list', [SessionStudentController::class, 'list'])->name('sessionlist');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/planning', [CourseSessionController::class, 'show_planning'])->name('showplanning');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/planning/modify', [CourseSessionController::class, 'modify_planning'])->name('modifyplanning');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/modify', [CourseSessionController::class, 'modify'])->name('modifysession');

Route::middleware(['auth:sanctum', 'verified'])->post('/session/{id}/modify', [CourseSessionController::class, 'applyModify'])->name('modifysession');

Route::middleware(['auth:sanctum', 'verified'])->post('/session/{id}/publish', [CourseSessionController::class, 'publish'])->name('publishsession');

Route::middleware(['auth:sanctum', 'verified'])->post('/session/{id}/halt', [CourseSessionController::class, 'halt'])->name('haltsession');

Route::middleware(['auth:sanctum', 'verified'])->post('/api/deletelesson', [CourseSessionController::class, 'deleteLesson'])->name('deletelesson');

Route::middleware(['auth:sanctum', 'verified'])->get('/sessionstudent/{id}', [SessionStudentController::class, 'detail'])->name('sessionstudent');

Route::middleware(['auth:sanctum', 'verified'])->get('/contributor', [ContributorController::class, 'show'])->name('contributor');

Route::middleware(['auth:sanctum', 'verified'])->get('/contributor/new', [ContributorController::class, 'new'])->name('newcontributor');

Route::middleware(['auth:sanctum', 'verified'])->post('/contributor/add', [ContributorController::class, 'add'])->name('addcontributor');

Route::middleware(['auth:sanctum', 'verified'])->get('/contributor/del/{id}', [ContributorController::class, 'del'])->name('delcontributor');

Route::middleware(['auth:sanctum', 'verified'])->get('/contributor/{id}/show', [ContributorController::class, 'show_details'])->name('showcontributor');

Route::middleware(['auth:sanctum', 'verified'])->get('/contributor/{id}/show_horaire', [ContributorController::class, 'show_horaire'])->name('ficheHoraire');

Route::middleware(['auth:sanctum', 'verified'])->get('/contributor/{id}/mod', [ContributorController::class, 'showMod'])->name('modcontributor');

Route::middleware(['auth:sanctum', 'verified'])->post('/contributor/{id}/mod', [ContributorController::class, 'mod'])->name('applymodcontributor');

Route::middleware(['auth:sanctum', 'verified'])->get('/school', [SchoolController::class, 'show'])->name('school');

Route::middleware(['auth:sanctum', 'verified'])->get('/school/new', [SchoolController::class, 'new'])->name('newschool');

Route::middleware(['auth:sanctum', 'verified'])->post('/subject/school', [SchoolController::class, 'add'])->name('addschool');

Route::middleware(['auth:sanctum', 'verified'])->get('/school/del/{id}', [SchoolController::class, 'del'])->name('delschool');

Route::middleware(['auth:sanctum', 'verified'])->get('/school/mod/{id}', [SchoolController::class, 'showMod'])->name('showmodschool');

Route::middleware(['auth:sanctum', 'verified'])->post('/school/mod', [SchoolController::class, 'mod'])->name('modschool');

Route::middleware(['auth:sanctum', 'verified'])->get('/subject', [SubjectController::class, 'show'])->name('subject');

Route::middleware(['auth:sanctum', 'verified'])->get('/subject/new', [SubjectController::class, 'new'])->name('newsubject');

Route::middleware(['auth:sanctum', 'verified'])->post('/subject/add', [SubjectController::class, 'add'])->name('addsubject');

Route::middleware(['auth:sanctum', 'verified'])->get('/subject/del/{id}', [SubjectController::class, 'del'])->name('delsubject');

Route::middleware(['auth:sanctum', 'verified'])->post('/api/contributors-by-subject', [ContributorController::class, 'contributorsBySubject'])->name('contributorsbysubject');

Route::middleware(['auth:sanctum', 'verified'])->post('/lesson/add', [CourseSessionController::class, 'addLesson'])->name('addlesson');

Route::middleware(['auth:sanctum', 'verified'])->post('/lesson/mofidy', [CourseSessionController::class, 'modifyLesson'])->name('modifylesson');

Route::post('/register/apply', [SessionRegisterController::class, 'register'])->name('applyregister');

Route::middleware(['auth:sanctum', 'verified', 'role'])->get('/sendplanningcontributor/{id}', [CourseSessionController::class, 'email_contributors'])->name('emailtocontributors');

Route::middleware(['auth:sanctum', 'verified', 'role'])->get('/sendplanningcontributorperso/{id}', [CourseSessionController::class, 'email_contributors_perso'])->name('emailtocontributorsperso');

Route::get('/registertosession/{id}', [SessionRegisterController::class, 'show'])->name('sessionregister');

Route::post('/api/lesson-by-level', [CourseSessionController::class, 'lessonsByLevel'])->name('lessonbylevel');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/pdf', [SessionStudentController::class, 'pdflist'])->name('pdfsessionlist');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/csv', [SessionStudentController::class, 'csvlist'])->name('csvsessionlist');

Route::middleware(['auth:sanctum', 'verified'])->get('/email/{id}', [CourseSessionController::class, 'email'])->name('email');

Route::get('/seeplanning/{id}', [SessionStudentController::class, 'planning'])->name('sessionstudentplanning')->middleware('signed');

Route::get('/seeplanning_contributor/{id_contributor}/{id_course_session}', [CourseSessionController::class, 'planning_contributor'])->name('sessioncontributorplanning')->middleware('signed');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/pdfstage', [CourseSessionController ::class, 'pdfliststage'])->name('pdffichestage');

Route::middleware(['auth:sanctum', 'verified'])->get('/session/{id}/attendance', [CourseSessionController::class, 'attendance'])->name('attendance');

Route::middleware(['auth:sanctum', 'verified'])->get('/search_student', [StudentController::class, 'search'])->name('students.search');

Route::middleware(['auth:sanctum', 'verified'])->get('/search_familly', [FamillyController::class, 'search'])->name('famillies.search');

Route::middleware(['auth:sanctum', 'verified'])->get('/appel', [AppelController::class, 'show'])->name('appel');

Route::middleware(['auth:sanctum', 'verified'])->get('/appel/new', [AppelController::class, 'new'])->name('newappel'); 

Route::middleware(['auth:sanctum', 'verified'])->get('/appel/del/{id}', [AppelController::class, 'del'])->name('delappel');

Route::middleware(['auth:sanctum', 'verified'])->post('/codebarsearch', [AppelController::class, 'search_codebar'])->name('codebar.search');

Route::middleware(['auth:sanctum', 'verified'])->post('/appel/add', [AppelController::class, 'add_appel'])->name('add_appel');

Route::middleware(['auth:sanctum', 'verified'])->get('/appel/{id}/list', [AppelController::class, 'list_appel'])->name('list_appel');

Route::middleware(['auth:sanctum', 'verified'])->get('/appel/{id}/csv', [AppelController::class, 'csvappel'])->name('csvappellist');

Route::middleware(['auth:sanctum', 'verified'])->get('student/{id}/codebar', [StudentController::class, 'show_codebar_student'])->name('show_codebar_student');

Route::middleware(['auth:sanctum', 'verified'])->get('minorstudent/{id}/codebar', [StudentController::class, 'show_codebar_minorstudent'])->name('show_codebar_minorstudent');




